# Amnesty International (Denmark)

Login til backoffice: Admin//AoAk2014

### Environment
 Miljø | Url | Server 
 --- | --- | --- 
 Test | https://amnesty-test.dis-play.dk (tidligere *http://amnesty.test.dis-play.dk*)| 192.168.0.150 
 Live | https://amnesty.dk | 195.69.128.251
 Midlertidig URL til test af LIVE miljø | http://umbraco.amnesty.dk | 195.69.128.251

Brugernavn: 1079824.Admin

Kodeord: z4E+pvHMY

Adgang til FTP, der kører på webserver:
login: Amnesty  
pw: epwmtG8R  

Der bliver brugt Hangfire på amnesty, der administrer tasks  
Lige nu er der to tasks, fjern cpr nr fra dabasen, og export af forms  
Hangfire rapport og "kør tasks igen" muligheder kan findes via hangfire UI, vær opmærksom på at man skal være logget ind i Umbraco før man kan tilgå UI'et
/umbraco/backoffice/hangfire

  

Du får adgang til Mit ScanNet ved at logge ind på www.scannet.dk/mit-scannet. 

Loginoplysninger
Brugernavn: 	239570  
Adgangskode: 	BpAFFzIoA  


## Contributors

Team members: Nicolaj Lund Nielsen (Frontend lead), Anders Lynge (Backend lead), Louise Birk Arnbjerg (Project lead), Henrik Dreijer-Pedersen(Design).

Date of creation: 04/12 - 2014


## FRONTEND DOKUMENTATION

All frontend files are located in /static folder.
Frontend is build with Nodejs module Gulp and configuration is placed in `gulpfile.js` and `package.json`.

### Install frontend buildtools

#### Install NodeJS
To use Gulp you need Node JS (Windows Installer (.msi) 32-bit version)
[Install Node.js](http://nodejs.org/download/ "Node JS website")

### Install site specific Gulp modules
This installs Gulp and all Gulp modules used for this site. Listed in package.json.

```
c:\path\to\project\root\folder> npm install
```

And wait for Nodejs package manager (npm) to install all dependent Gulp modules.

### Run Gulp
Write this in CMD to start Gulp:

```
c:\path\to\project\root\folder\ > npm run gulp
```

> Gulp 'watches' file changes and builds Sass and JS.

### CSS

Sass is located in `/static/sass/` and Gulp builds it to `/static/dist/`. CSS is Minified.

> Remember to describe any Sass specific structures and wizardry that is not obvious.

### Javascript
(jQuery / TweenMax / Other).

JS is located in `/static/js/` and Gulp builds it to `/static/dist/js/`.
JS is Uglify'ed.

jQuery, TweenMax and other plugins is is located in vendor/plugins.

### Livereload
Livereload is activated in Gulpfile.js.

To use it you need to install this Chrome Browser extension:

```
http://bit.ly/IKI2MY
```

Or Firefox:

```
http://mzl.la/1uAab9V
```

Or if you need to Livereload another browser (eg. on a mobile phone/tablet) then place this script before `</body>` tag:

`<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>`

> Remember to remove it before you deploy to production!


## BACKEND DOKUMENTATION

[Projectroom](https://url/to/projectroom/dokumentation/)

> Paste the content of DeveloperGuide.docx here


## Troubleshoot'n
...