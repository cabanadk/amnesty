﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using DisPlay.Amnesty.ViewModels;
using RJP.MultiUrlPicker.Models;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.Models
{
    public class SettingsNodeModel : PublishedContentModel
    {
        public SettingsNodeModel(IPublishedContent content) : base(content)
        {
            
        }

        public string SupportHeadline
        {
            get { return this.GetPropertyValue<string>("supportHeadline"); }
        }

        public string SupportChooseAmount { get { return this.GetPropertyValue<string>("supportChooseAmount"); } }
        public string SupportChooseCustomAmount { get { return this.GetPropertyValue<string>("supportChooseCustomAmount"); } }
        public string SupportTypeAmount { get { return this.GetPropertyValue<string>("supportTypeAmount"); } }
        public string SupportPlaceholderName { get { return this.GetPropertyValue<string>("supportPlaceholderName"); } }
        public string SupportPlaceholderSurname { get { return this.GetPropertyValue<string>("supportPlaceholderSurname"); } }
        public string SupportContactInfo { get { return this.GetPropertyValue<string>("supportContactInfo"); } }
        public string SupportPlaceholderEmail { get { return this.GetPropertyValue<string>("supportPlaceholderEmail"); } }
        public string SupportPlaceholderPhone { get { return this.GetPropertyValue<string>("supportPlaceholderPhone"); } }
        public string SupportPlaceholderZip { get { return this.GetPropertyValue<string>("supportPlaceholderZip"); } }
        public string SupportPlaceholderCity { get { return this.GetPropertyValue<string>("supportPlaceholderCity"); } }
        public string SupportTaxDeduction { get { return this.GetPropertyValue<string>("supportTaxDeduction"); } }
        public string SupportPlaceholderCPR { get { return this.GetPropertyValue<string>("supportPlaceholderCPR"); } }
        public string SupportStudentCheckbox { get { return this.GetPropertyValue<string>("supportStudentCheckbox"); } }
        public string SupportButtonSend { get { return this.GetPropertyValue<string>("supportButtonSend"); } }
        public string SupportErrorAmount { get { return this.GetPropertyValue<string>("supportErrorAmount"); } }
        public string SupportErrorName { get { return this.GetPropertyValue<string>("supportErrorName"); } }
        public string SupportErrorSurname { get { return this.GetPropertyValue<string>("supportErrorSurname"); } }
        public string SupportErrorEmail { get { return this.GetPropertyValue<string>("supportErrorEmail"); } }
        public string SupportErrorPhone { get { return this.GetPropertyValue<string>("supportErrorPhone"); } }
        public string SupportErrorZip { get { return this.GetPropertyValue<string>("supportErrorZip"); } }
        public string SupportErrorCity { get { return this.GetPropertyValue<string>("supportErrorCity"); } }
        public string SupportErrorCPR { get { return this.GetPropertyValue<string>("supportErrorCPR"); } }
        public string MemberChooseAmount { get { return this.GetPropertyValue<string>("memberChooseAmount"); } }
        public string MemberHeadline { get { return this.GetPropertyValue<string>("memberHeadline"); } }
        public string MemberBetalingsService { get { return this.GetPropertyValue<string>("memberBetalingsService"); } }
        public string MemberRegnr { get { return this.GetPropertyValue<string>("memberRegnr"); } }
        public string MemberKontonr { get { return this.GetPropertyValue<string>("memberKontonr"); } }
        public string MemberErrorRegnr { get { return this.GetPropertyValue<string>("memberErrorRegnr"); } }
        public string MemberErrorKontonr { get { return this.GetPropertyValue<string>("memberErrorKontonr"); } }
        public string FooterPayoff { get { return this.GetPropertyValue<string>("FooterPayoff"); } }
        public string FooterEmail { get { return this.GetPropertyValue<string>("FooterEmail"); } }
        public string FooterAddress { get { return this.GetPropertyValue<string>("FooterAddress"); } }

        public string CookieHeadline { get { return this.GetPropertyValue<string>("cookieHeadline"); } }

        public IHtmlString CookieDescription
        {
            get
            {
                var cookieDescription = this.GetPropertyValue<string>("cookieDescription");
                if (cookieDescription == null)
                {
                    return null;
                }
                return new HtmlString(cookieDescription.Replace("\n", "<br>"));
            }
        }

        public string CookieLinkText { get { return this.GetPropertyValue<string>("cookieLinkText"); } }

        public BaseViewModel CookieLinkPage
        {
            get
            {
                var cookiePage = this.GetPropertyValue<BaseViewModel>("cookiePage");
                return cookiePage;
            }
        }

        public string VisualAppearance { get { return this.GetPropertyValue<string>("visualAppearance"); } }

        public bool HidePageHeadingAndTags { get { return this.GetPropertyValue<bool>("hidePageHeadingAndTags"); } }

        public bool DibsTestMode { get { return ConfigurationManager.AppSettings["dibsTestMode"] == "True"; } }





    }
}