using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Ctl.Data;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace DisPlay.Amnesty.Models
{
    [PrimaryKey("OrderId", autoIncrement = true)]
    [TableName("LandsstaevneOrders")]
    public class LandsstaevneOrderModel
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        [PositionAttribute(30)]
        public int OrderId { get; set; }
        [PositionAttribute(6)]
        public string Email { get; set; }
        [PositionAttribute(3)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Navn")]
        public string Name { get; set; }
        [PositionAttribute(4)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Efternavn")]
        public string SurName { get; set; }
        [Position(2)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Amnesty ID")]
        public string MemberOrCPR { get; set; }
        [PositionAttribute(8)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Postnr")]
        public string Zip { get; set; }
        [PositionAttribute(9)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("By")]
        public string City { get; set; }
        [PositionAttribute(5)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Telefon")]
        public string Phone { get; set; }
        [NotMapped]
        public bool Student { get; set; }
        [PositionAttribute(13)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Beløb")]
        public string Amount { get; set; }
        [PositionAttribute(20)]
        public int NodeId { get; set; }
        [PositionAttribute(7)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Adresse")]
        public string Address { get; set; }
        [PositionAttribute(10)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Alder")]
        public string Age { get; set; }
        [PositionAttribute(11)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Vegetar")]
        public string Vegetar { get; set; }
        [PositionAttribute(12)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Løsning")]
        public string Solution { get; set; }
        [PositionAttribute(14)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Kommentar")]
        public string Comment { get; set; }
        [PositionAttribute(16)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Betaling")]
        public string PaymentOptions { get; set; }
        [PositionAttribute(15)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Relevant information")]
        public bool RelevantInfo { get; set;  }
        [PositionAttribute(1)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Dato")]
        public DateTime Date { get; set; }
        [PositionAttribute(21)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Betalt")]
        public bool Payed { get; set; }
    }
}