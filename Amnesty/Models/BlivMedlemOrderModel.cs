﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Ctl.Data;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace DisPlay.Amnesty.Models
{
    [PrimaryKey("OrderId", autoIncrement = true)]
    [TableName("BlivMedlemOrders")]
    public class BlivMedlemOrderModel
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        [PositionAttribute(15)]
        public int OrderId { get; set; }
        [PositionAttribute(6)]
        public string Email { get; set; }
        [PositionAttribute(3)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Navn")]
        public string Name { get; set; }
        [PositionAttribute(4)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Efternavn")]
        public string SurName { get; set; }
        [PositionAttribute(7)]
        [LengthAttribute(8)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Nyt CPR")]
        public string Cpr { get; set; }
        [PositionAttribute(9)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Postnr")]
        public string Zip { get; set; }
        [PositionAttribute(10)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("By")]
        public string City { get; set; }
        [PositionAttribute(5)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Telefon")]
        public string Phone { get; set; }
        [PositionAttribute(13)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Type")]
        public bool Student { get; set; }
        [PositionAttribute(2)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Nyt Beløb")]
        public string Amount { get; set; }
        [PositionAttribute(14)]
        public int NodeId { get; set; }
        [PositionAttribute(11)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Nyt reg nr")]
        public string Reg { get; set; }
        [PositionAttribute(12)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Nyt Konto nr")]
        public string Konto { get; set; }
        [PositionAttribute(8)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Adresse")]
        public string Address { get; set; }
        [PositionAttribute(1)]
        [System.ComponentModel.DataAnnotations.Schema.ColumnAttribute("Opkaldsdato")]
        public DateTime Date { get; set; }
    }
}