﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web;

namespace DisPlay.Amnesty.ValueConverters
{
    public class MultiNodeTreePickerPropertyValueConverter : PropertyValueConverterBase, IPropertyValueConverterMeta
    {
        public override object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            return
                source.ToString()
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();
        }

        public override object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
        {
            var ids = source as int[];
            if (ids == null || ids.Length == 0 || UmbracoContext.Current == null)
            {
                return Enumerable.Empty<IPublishedContent>();
            }

            PreValueCollection preValues =
                ApplicationContext.Current.Services.DataTypeService.GetPreValuesCollectionByDataTypeId(
                    propertyType.DataTypeId);

            PreValue prevalue = preValues.PreValuesAsDictionary["startNode"];
            var startNode = JsonConvert.DeserializeObject<ContentPickerStartValue>(prevalue.Value);
            PreValue maxNumberPrevalue;
            bool multiPicker = true;
            if (preValues.PreValuesAsDictionary.TryGetValue("maxNumber", out maxNumberPrevalue))
            {
                multiPicker = maxNumberPrevalue.Value != "1";
            }

            IEnumerable<IPublishedContent> result = Enumerable.Empty<IPublishedContent>();

            switch (startNode.Type)
            {
                case "content":
                    result = ids.Select(UmbracoContext.Current.ContentCache.GetById);
                    break;
                case "media":
                    result = ids.Select(UmbracoContext.Current.MediaCache.GetById);
                    break;
                case "member":
                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    result = ids.Select(helper.TypedMember);
                    break;
            }

            if (multiPicker)
            {
                return result.WhereNotNull();
            }

            return result.FirstOrDefault();
        }

        public PropertyCacheLevel GetPropertyCacheLevel(PublishedPropertyType propertyType,
                                                        PropertyCacheValue cacheValue)
        {
            PropertyCacheLevel returnLevel;
            switch (cacheValue)
            {
                case PropertyCacheValue.Object:
                    returnLevel = PropertyCacheLevel.ContentCache;
                    break;
                case PropertyCacheValue.Source:
                    returnLevel = PropertyCacheLevel.Content;
                    break;
                case PropertyCacheValue.XPath:
                    returnLevel = PropertyCacheLevel.Content;
                    break;
                default:
                    returnLevel = PropertyCacheLevel.None;
                    break;
            }

            return returnLevel;
        }

        public Type GetPropertyValueType(PublishedPropertyType propertyType)
        {
            return typeof(IEnumerable<IPublishedContent>);
        }

        public override bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals(Constants.PropertyEditors.MultiNodeTreePickerAlias);
        }

        private class ContentPickerStartValue
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("query")]
            public string Query { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }
        }
    }
}