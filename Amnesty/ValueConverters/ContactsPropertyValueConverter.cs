﻿using System.Collections.Generic;
using System.Linq;
using Archetype.Models;
using Archetype.PropertyConverters;
using DisPlay.Amnesty.ViewModels.ContactBoxes;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace DisPlay.Amnesty.ValueConverters
{
    public class ContactBoxPropertyValueConverter : PropertyValueConverterBase
    {
        public override object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {

            var boxes = new ArchetypeValueConverter().ConvertDataToSource(propertyType, source, preview) as ArchetypeModel;

            if (boxes == null)
            {
                return Enumerable.Empty<ContactBox>();
            }

            var result = new List<ContactBox>();
            foreach (var box in boxes)
            {
                if (box.Alias == "contactPerson")
                    result.Add(new ContactPersonBox(box));
            }
            return result;
        }

        public override bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("Imulus.Archetype") &&
                   ApplicationContext.Current.Services.DataTypeService.GetDataTypeDefinitionById(propertyType.DataTypeId)
                       .Name.Equals("ArcheType - Kontakter");
        }
    }
}