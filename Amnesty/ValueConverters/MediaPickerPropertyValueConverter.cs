﻿using System;
using DisPlay.Amnesty.ViewModels.Models;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web;

namespace DisPlay.Amnesty.ValueConverters
{
    public class MediaPickerPropertyConverter : PropertyValueConverterBase, IPropertyValueConverterMeta
    {
        public override object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            Attempt<int> attempt = source.TryConvertTo<int>();
            if (attempt.Success)
            {
                return attempt.Result;
            }
            return null;
        }

        public override object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source == null || UmbracoContext.Current == null)
            {
                return null;
            }
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            IPublishedContent content = umbracoHelper.TypedMedia(source);
            if (content != null)
            {
                if (content.DocumentTypeAlias == "Image")
                {
                    return new ImageModel(content);
                }
                if (content.DocumentTypeAlias == "File")
                {
                    return new FileModel(content);
                }
                if (content.DocumentTypeAlias == "Folder")
                {
                    return new FolderModel(content);
                }
                return new MediaModel(content);
            }
            return null;
        }

        public PropertyCacheLevel GetPropertyCacheLevel(PublishedPropertyType propertyType,
                                                        PropertyCacheValue cacheValue)
        {
            PropertyCacheLevel propertyCacheLevel;
            switch (cacheValue)
            {
                case PropertyCacheValue.Source:
                    propertyCacheLevel = PropertyCacheLevel.Content;
                    break;
                case PropertyCacheValue.Object:
                    propertyCacheLevel = PropertyCacheLevel.ContentCache;
                    break;
                case PropertyCacheValue.XPath:
                    propertyCacheLevel = PropertyCacheLevel.Content;
                    break;
                default:
                    propertyCacheLevel = PropertyCacheLevel.None;
                    break;
            }
            return propertyCacheLevel;
        }

        public Type GetPropertyValueType(PublishedPropertyType propertyType)
        {
            if (propertyType.ContentType.Alias == "Image")
            {
                return typeof(ImageModel);
            }
            if (propertyType.ContentType.Alias == "File")
            {
                return typeof(FileModel);
            }
            if (propertyType.ContentType.Alias == "Folder")
            {
                return typeof(FolderModel);
            }
            return typeof(MediaModel);
        }

        public override bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("Umbraco.MediaPicker");
        }
    }
}
