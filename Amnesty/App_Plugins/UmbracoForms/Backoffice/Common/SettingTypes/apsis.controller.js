﻿function apsisResource($http) {

    var apiRoot = "backoffice/UmbracoForms/Apsis/";

    return {
        GetAllProjects: function () {
            return $http.get(apiRoot + "GetAllProjects");
        },
        GetAllDemographicFields: function () {
            return $http.get(apiRoot + "GetAllDemographicFields");
        }
    };
}
angular.module('umbraco.resources').factory('apsisResource', apsisResource);

angular.module("umbraco").controller("UmbracoForms.SettingTypes.ApsisController",
	function ($scope, $routeParams, apsisResource, pickerResource) {

	    if (!$scope.setting.value) {

	    } else {
	        var value = JSON.parse($scope.setting.value);
	        $scope.project = value.project;
	        $scope.email = value.email;
	        $scope.properties = value.properties;
	    }

	    apsisResource.GetAllProjects().then(function (response) {
	        $scope.projects = response.data;
	    });
	 

	    pickerResource.getAllFields($routeParams.id).then(function (response) {
	        $scope.fields = response.data;
	    });

	    $scope.setProject = function () {
	        apsisResource.GetAllDemographicFields().then(function (response) {
	            $scope.properties = response.data;
	        });
	    };

	    $scope.setValue = function () {

	        var val = {};
	        val.project = $scope.project;

	        val.properties = $scope.properties;
	        val.email = $scope.email;
	        $scope.setting.value = JSON.stringify(val);
	    };
	});