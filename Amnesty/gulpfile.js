/**
 * SETUP
 *
 * require module dependencies
 */
var gulp = require("gulp"),
	// JS
    concat = require("gulp-concat"),
	uglify = require("gulp-uglifyjs"),
	jshint = require("gulp-jshint"),
	// CSS
	sass = require("gulp-sass"),
    prefix = require("gulp-autoprefixer"),
	minifyCss = require("gulp-minify-css"),
    // IMAGES
    imageop = require("gulp-image-optimization"),
	// MISC
	rename = require("gulp-rename"),
	plumber = require("gulp-plumber"),
	livereload = require("gulp-livereload"),
	watcherCallback = function (event) {
	    console.log("=================================================================");
	    console.log("File " + event.path + " was " + event.type + ", running tasks...");
	};

/**
 * TASKS
 *  default: all tasks.
 *  images: run only the "images" task.
 *  resourcecopy: copy resources to DIST folder
 *  Then add watches on folders.
 *
 *  The array of tasks (after task: default) is run on start
 *  NB: Add and remove less/sass here
 */
gulp.task("default", [
		"sass",
		"js",
		"lint",
        "resourcecopy"
], function () {
    livereload.listen();

    // WATCH events

    // Watch DIST folder and refresh browser
    gulp
        .watch(["static/dist/**"])
        .on("change", livereload.changed);

    // Watch JS-folder(s) for changes
    gulp
        .watch("static/src/js/**/*.js", ["js", "lint"])
        .on("change", watcherCallback);

    // Watch SASS-folder(s) for changes
    gulp
        .watch("static/src/sass/**/*.*", ["sass"])
        .on("change", watcherCallback);
});

// Compress and do stuff to images
gulp.task("images", ["images"]);
gulp.task("resourcecopy", ["resourcecopy"]);

/**
 * JAVASCRIPT
 *
 * Set up task for JS
 */
gulp.task("js", function () {
    gulp.src([
            "static/src/js/vendor/modernizr.custom.js",
            "node_modules/slick-carousel/slick/slick.js",
            "static/src/js/vendor/gsap/TweenLite.js",
            "static/src/js/vendor/gsap/TimelineLite.js",
            "static/src/js/vendor/gsap/easing/EasePack.js",
            "static/src/js/vendor/gsap/plugins/CSSPlugin.js",
            "static/src/js/vendor/gsap/plugins/ScrollToPlugin.js",
            "static/src/js/vendor/gsap/plugins/DrawSVGPlugin.js",
            "static/src/js/vendor/bootstrap-affix.js",
            "static/src/js/vendor/jquery.lazy-load-google-maps.js",
            "static/src/js/vendor/ScrollMagic.min.js",
            "static/src/js/vendor/plugins/animation.gsap.js",
            "static/src/js/vendor/fastclick.js",
            "static/src/js/main.js",
            "static/src/js/custom/DIS.scroll.js",
            "static/src/js/custom/DIS.ui.js",
            "static/src/js/custom/DIS.menu.js",
            "static/src/js/custom/DIS.infograph.js",
            "static/src/js/custom/DIS.share.js",
            "static/src/js/custom/DIS.forms.js",
            "static/src/js/custom/DIS.progressbar.js",
            "static/src/js/custom/DIS.fixes.js",
            "static/src/js/custom/DIS.slider.js"
        ])
        .pipe(plumber())
        //.pipe(concat("total.js"))
        // Uglify the javascript files into "main.min.js"
        // and create a sourcemap file
        .pipe(uglify("total.min.js", {
                outSourceMap: true,
                mangle: false,
                sourceRoot: "../../../"
            })
        )
        .pipe(plumber.stop())
        // Render the output into the DIST folder
        .pipe(gulp.dest("static/dist/js"));
    // ...and copy to BUILD (avoid a PUBLISH in VS)
    //.pipe(gulp.dest("../BUILDFOLDER/static/dist/js"));

    // Create a file for legacy-browsers, such as IE8 and below, to get rudimentary support for HTML5 and similar.
    gulp.src([
        "static/src/js/vendor/consolelogfix.min.js",
        "static/src/js/vendor/html5shiv-printshiv.js",
        "static/src/js/vendor/respond.min.js"
    ])
        .pipe(plumber())
        // Uglify the javascript files into "legacy.min.js"
        // and create a sourcemap file
        .pipe(  // Uglify the javascript files into "legacy.min.js", but without
                // a sourcemap, since IE8 doesn"t understand that anyway.
                uglify("legacy.min.js", {
                    outSourceMap: false,
                    sourceRoot: "../../",
                    mangle: false
                }))
        .pipe(plumber.stop())
        // Render the output into the DIST folder
        .pipe(gulp.dest("static/dist/js"));
});

/**
 * JS Linter
 *
 * Set up task for JSHint
 */
gulp.task("lint", function () {
    gulp.src("static/src/js/*.js")
        // Run file through JSHint
        .pipe(jshint())
        // Pipe the result of that through to the JSHint reporter
        .pipe(jshint.reporter("default"));
});

/**
 * SASS
 *
 * Set up task for SASS
 */
gulp.task("sass", function () {
    gulp.src("static/src/sass/main.scss")
        .pipe(plumber())
        // Run file through SASS-compiler
        .pipe(sass())
        // Minify the CSS
        .pipe(prefix({
            browsers: ["> 1%", "last 2 versions"],
            cascade: false
        }))
        .pipe(minifyCss())
        // Rename CSS file
        .pipe(rename("main.min.css"))
        .pipe(plumber.stop())
        // Render the output into the DIST folder
        .pipe(gulp.dest("static/dist/css"));
    // ...and copy to BUILD (avoid a PUBLISH in VS)
    //.pipe(gulp.dest("../BUILDFOLDER/static/dist"));
});

/**
 * RESOURCECOPY
 *
 * Set up tasks for copying folders and files to /dist/
 * and compress images.
 *
 * TODO: needs some SVG optimation.
 */
gulp.task("resourcecopy", function () {
    gulp.src([
        "static/src/fonts/*.*",
        "static/src/js/vendor/*.*",
        "static/src/media/*.*"
    ], { base: "static/src" })
        // Render the output into the DIST folder
        .pipe(gulp.dest("static/dist"));
/*
    gulp.src([
        "static/src/img/*.{png,jpg,jpeg,gif,svg}"
    ], { base: "static/src" })
        .pipe(plumber())
        .pipe(imageop({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true,
            quality: "65-80"
        }))
        .pipe(plumber.stop())
        // Render the output into the DIST folder
        .pipe(gulp.dest("static/dist"));
*/
});

/**
 * IMAGES
 *
 * Set up task for IMAGES
 * Run this task: "npm run gulpimg"
 *
 * TODO: needs some SVG optimation.
 */
gulp.task("images", function () {
    gulp.src([
		"static/src/img/*.{png,jpg,jpeg,gif,svg}"
    ], { base: "static/src" })
        .pipe(plumber())
		.pipe(imageop({
		    optimizationLevel: 5,
		    progressive: true,
		    interlaced: true,
		    quality: "65-80"
		}))
        .pipe(plumber.stop())
    	// Render the output into the DIST folder
    	.pipe(gulp.dest("static/dist"));
});
