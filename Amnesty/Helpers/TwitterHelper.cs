﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using DisPlay.Amnesty.ViewModels.Models;
using LinqToTwitter;
using Umbraco.Core.Configuration;

namespace DisPlay.Amnesty.Helpers
{
    public class TwitterHelper
    {
        private readonly string _consumerKey = ConfigurationManager.AppSettings["twitterConsumerKey"]; //"iC5ulV5MabYfzyX9tKSfvl2Aq";
        private readonly string _consumerSecret = ConfigurationManager.AppSettings["twitterConsumerSecret"];//"XcfOwbd30yZiuyRNXkbTkLLbkjfGeLz5B9oFF6CEoeLUpuLvaC";
        private readonly string _accessToken = ConfigurationManager.AppSettings["twitterAccessToken"];//"167322189-WzLUaiH6l6Os9n0uArOHpZHligl4DVyLFq1SDP04";
        private readonly string _accessTokenSecret = ConfigurationManager.AppSettings["twitterAccessTokenSecret"];//"JSTfd5HRmBuu8hGkehooQbPMSzYn7W4eFvpruy7wRQhB9";

        public TwitterHelper()
        {
               
        }

        private const string GET_TWEET_FROMU_HASHTAG = "TwitterHelper.GetTweetsFromHash:hashtag:{0}:count:{1}";
        public IEnumerable<TweetModel> GetTweetsFromHash(string hashtag, int count)
        {
            string cacheKey = string.Format(GET_TWEET_FROMU_HASHTAG, hashtag, count);
            IEnumerable<TweetModel> tweets = HttpRuntime.Cache[cacheKey] as IEnumerable<TweetModel>;
            if (tweets == null)
            {
                tweets = GetUncachedTweetsFromHash(hashtag, count);
                HttpRuntime.Cache.Add(cacheKey, tweets, null, Cache.NoAbsoluteExpiration,
                                      new TimeSpan(0, 1, 0), CacheItemPriority.Normal, null);
            }
            return tweets;
        }

        private IEnumerable<TweetModel> GetUncachedTweetsFromHash(string hashtag, int count)
        {
            var twitterCtx = GetTwitterContext();
            var searchResponse =
                (from search in twitterCtx.Search
                 where search.Type == SearchType.Search &&
                       search.Query == hashtag && search.Count == count
                 select search).SingleOrDefault();

            if (searchResponse != null && searchResponse.Statuses != null)
            {
                return
                    searchResponse.Statuses.Select(
                        tweet => new TweetModel(tweet));
            }
            return new List<TweetModel>();
        }

        private const string GET_TWEET_FROM_USERNAME = "TwitterHelper.GetTweetsFromUsername:screenName:{0}:count:{1}";
        public IEnumerable<TweetModel> GetTweetsFromUsername(string screenName = "Amnesty",int count=10)
        {
            string cacheKey = string.Format(GET_TWEET_FROM_USERNAME, screenName, count);
            IEnumerable<TweetModel> tweets = HttpRuntime.Cache[cacheKey] as IEnumerable<TweetModel>;
            if (tweets == null)
            {
                tweets = GetUncachedTweetsFromUsername(screenName,count);
                HttpRuntime.Cache.Add(cacheKey, tweets, null, Cache.NoAbsoluteExpiration,
                                      new TimeSpan(0, 10, 0), CacheItemPriority.Normal, null);
            }
            return tweets;
        }


        private IEnumerable<TweetModel> GetUncachedTweetsFromUsername(string screenName = "Amnesty", int count = 10)
        {
            // In v1.1, all API calls require authentication
            var twitterCtx = GetTwitterContext();

            var tweets = (from status in twitterCtx.Status
                          where status.ScreenName == screenName && status.Type == StatusType.User && status.Count == count
                          select status).ToList();

            return tweets.Select(tweet => new TweetModel(tweet));
        }

        private TwitterContext GetTwitterContext()
        {
            return new TwitterContext(new SingleUserAuthorizer
            {
                CredentialStore = new SingleUserInMemoryCredentialStore
                {
                    AccessToken = _accessToken,
                    AccessTokenSecret = _accessTokenSecret,
                    ConsumerKey = _consumerKey,
                    ConsumerSecret = _consumerSecret
                }
            });
        }
    }
}