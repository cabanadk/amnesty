﻿using System.Collections.Generic;
using System.Linq;
using Archetype.Models;
using DisPlay.Amnesty.ViewModels;
using DisPlay.Amnesty.ViewModels.Modules;

namespace DisPlay.Amnesty.Helpers
{
    public static class ModuleHelper
    {
        public static IEnumerable<Module> GetModules(this ArchetypeFieldsetModel model)
        {
            var modules = model.GetValue<ArchetypeModel>("modules");
            foreach (ArchetypeFieldsetModel fieldset in modules.Where(x => !x.Disabled))
            {
                switch (fieldset.Alias)
                {
                    case "text":
                        yield return new Text(fieldset);
                        break;
                    case "googlemaps":
                        yield return new GoogleMaps(fieldset);
                        break;
                    case "googleMapsWithPoints":
                        yield return new GoogleMapsWithPoints(fieldset);
                        break;
                    case "text2cols":
                        yield return new Text2Cols(fieldset);
                        break;
                    case "video":
                        yield return new Video(fieldset);
                        break;
                    case "infographic":
                        yield return new Infographic(fieldset);
                        break;
                    case "downloadmaterial":
                        yield return new DownloadMaterial(fieldset);
                        break;
                    case "boxSection":
                        yield return new BoxSection(fieldset);
                        break;
                    case "statement":
                        yield return new Statement(fieldset);
                        break;
                    case "topic":
                        yield return new Topic(fieldset);
                        break;
                    case "contacts":
                        yield return new Contacts(fieldset);
                        break;
                    case "sign":
                        yield return new Sign(fieldset);
                        break;
                    case "lifeline":
                        yield return new Lifeline(fieldset);
                        break;
                    case "writeletter":
                        yield return new WriteLetter(fieldset);
                        break;
                    case "formular": 
                        yield return new Formular(fieldset);
                        break;
                    case "skrivunderFormular":
                        yield return new SkrivunderFormular(fieldset);
                        break;
                    case "pressAndConsulation":
                        yield return new PressAndConsulation(fieldset);
                        break;
                    case "news": 
                        yield return new News(fieldset);
                        break;
                    case "hrSkyen":
                        yield return new HrSkyen(fieldset);
                        break;
                    case "externalContent":
                        yield return new ExternalContent(fieldset);
                        break;
                    case "personalThankYouNote":
                        yield return new PersonalThankYouNote(fieldset);
                        break;
                    case "imageSliderWithLink":
                        yield return new ImageSliderWithLink(fieldset);
                        break;
                    case "accordion":
                        yield return new Accordion(fieldset);
                        break;
                }
            }
        }
    }
}