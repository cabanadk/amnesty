﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DisPlay.Amnesty.Models;
using DisPlay.Amnesty.ViewModels.Models;
using RJP.MultiUrlPicker.Models;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace DisPlay.Amnesty.Helpers
{
    public class ContentHelper
    {
        public static IEnumerable<MenuLinkModel> GetLinkModelsFromLinkField(IPublishedContent content, string propertyName)
        {
            var multiUrlPicker = content.GetPropertyValue<MultiUrls>(propertyName);
            if (multiUrlPicker == null)
            {
                return Enumerable.Empty<MenuLinkModel>();
            }
            return multiUrlPicker.Select(GetLinkModelFromLink).ToList();
        }

        public static MenuLinkModel GetLinkModelFromLink(Link link)
        {
            return new MenuLinkModel(link);
        }

        public static IPublishedContent FindWebsite(IPublishedContent content)
        {
            if (content == null)
            {
                //throw new Exception("Site settings not found")
                return null;
            }
            var websiteNode = content.AncestorOrSelf("Website");

            return websiteNode;
        }

        public static SettingsNodeModel FindSiteSettings(IPublishedContent content)
        {
            var websiteNode = FindWebsite(content);
            if (websiteNode == null)
            {
                return null;
            }
            return  new SettingsNodeModel(websiteNode.Children.FirstOrDefault(child => child.DocumentTypeAlias == "WebsiteSettings"));
        }

        public static IPublishedContent FindSharedContent(IPublishedContent content)
        {
            var websiteNode = FindWebsite(content);
            if (websiteNode == null)
            {
                return null;
            }
            return websiteNode.Children.FirstOrDefault(child => child.DocumentTypeAlias == "GlobalShared");
        }

        public static IPublishedContent FindFrontpage(IPublishedContent content)
        {
            var websiteNode = FindWebsite(content);
            if (websiteNode == null)
            {
                return null;
            }
            return websiteNode.Children.FirstOrDefault(child => child.DocumentTypeAlias == "frontpage" || child.DocumentTypeAlias == "subsiteForside");
        }
    }
}