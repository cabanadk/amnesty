﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using Ctl.Data;
using Ctl.Data.Infrastructure;
using DisPlay.Amnesty.Controllers;
using DisPlay.Amnesty.Models;
using Umbraco.Core;
using Umbraco.Forms.Core;
using Umbraco.Forms.Data.Storage;
using Umbraco.Forms.Web.Models.Backoffice;
using Umbraco.Web;

namespace DisPlay.Amnesty.Helpers
{
    public static class ExportHelper
    {
        private static readonly string ExportPath = $"~\\App_Data\\forms";

        public static string ExportToFile(RecordExportFilter filter, string filepath)
        {

            EntrySearchResultCollection resultCollection = FormRecordSearcher2.QueryDataBase((RecordFilter)filter, ApplicationContext.Current.DatabaseContext);

            if (!resultCollection.Results.Any())
            {
                return "no results";
            }

            using (StreamWriter writer = new StreamWriter(filepath, false, Encoding.Default))
            {

                var csvWriter = new CsvWriter(writer, ';');


                csvWriter.WriteRow(resultCollection.schema.Select(_ => _.Name));

                foreach (var result in resultCollection.Results)
                {
                    csvWriter.WriteRow(result.Fields.Select(_ => _?.ToString()));
                }

                csvWriter.Flush();
            }

            return "no excel";

        }

        public static int ExportForms()
        {
            int i = 0;
            using (FormStorage formStorage = new FormStorage())
            {
                var forms = formStorage.GetForms();
                foreach (var form in forms)
                {
                    var temp = new RecordExportFilter() { StartDate = DateTime.Today.AddDays(-1), Form = form.Id, StartIndex = 1, };

                    var formName = System.IO.Path.GetInvalidFileNameChars().Aggregate(form.Name, (current, c) => current.Replace(c, '_'));

                    var fileName = HostingEnvironment.MapPath(ExportPath + $"\\{ formName+" "} {DateTime.Now.AddDays(-1).ToString("yyyyMMdd")}.csv");


                    var result = ExportToFile(temp, fileName);
                    if (result != "no results")
                    {
                        i++;
                    }
                }
            }

            ExportDibs();
            ExportLandsStaevne();
            ExportBlivMedlem();

            return i;
        }

        private static void ExportDibs()
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            var dibsOrders = db.Fetch<DibsOrderModel>("SELECT * FROM DibsOrders where convert(varchar(10), Date, 102 )= convert(varchar(10), DateAdd(dd, -1, GetDate()), 102)");
            var dibsFileName = HostingEnvironment.MapPath(ExportPath + "\\Engangsdonation "+DateTime.Now.AddDays(-1).ToString("yyyyMMdd") +".csv");

            if (!dibsOrders.Any())
            {
                return;
            }

            foreach (var order in dibsOrders)
            {
                order.Amount = order.Amount.Remove(order.Amount.Length - 2, 2);
            }

            using (StreamWriter writer = new StreamWriter(dibsFileName, false, Encoding.Default))
            {

                var csvWriter = new CsvWriter<DibsOrderModel>(writer, separator: ';', writeHeaders: true);
                csvWriter.WriteObjectsCompact(dibsOrders);


                csvWriter.Flush();
            }
        }

        private static void ExportLandsStaevne()
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            var dibsOrders = db.Fetch<LandsstaevneOrderModel>("SELECT * FROM LandsstaevneOrders where convert(varchar(10), Date, 102 )= convert(varchar(10), DateAdd(dd, -1, GetDate()), 102)");
            var dibsFileName = HostingEnvironment.MapPath(ExportPath + "\\Landstaevne " + DateTime.Now.AddDays(-1).ToString("yyyyMMdd") + ".csv");


            foreach (var order in dibsOrders)
            {
                order.Amount = order.Amount.Remove(order.Amount.Length - 2, 2);
            }

            if (!dibsOrders.Any())
            {
                return;
            }

            using (StreamWriter writer = new StreamWriter(dibsFileName, false, Encoding.Default))
            {

                var csvWriter = new CsvWriter<LandsstaevneOrderModel>(writer, separator: ';', writeHeaders: true);
                csvWriter.WriteObjectsCompact(dibsOrders);


                csvWriter.Flush();
            }
        }

        private static void ExportBlivMedlem()
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            var dibsOrders = db.Fetch<BlivMedlemOrderModel>("SELECT * FROM blivMedlemOrders where convert(varchar(10), Date, 102 )= convert(varchar(10), DateAdd(dd, -1, GetDate()), 102)");
            var dibsFileName = HostingEnvironment.MapPath(ExportPath + "\\BlivMedlem " + DateTime.Now.AddDays(-1).ToString("yyyyMMdd") + ".csv");

            if (!dibsOrders.Any())
            {
                return;
            }

            foreach (var order in dibsOrders)
            {
                order.Amount = order.Amount.Remove(order.Amount.Length - 2, 2);
            }

            using (StreamWriter writer = new StreamWriter(dibsFileName, false, Encoding.Default))
            {
                var csvWriter = new CsvWriter<BlivMedlemOrderModel>(writer, separator: ';', writeHeaders: true);

                csvWriter.WriteObjectsCompact(dibsOrders);

                
                csvWriter.Flush();
            }
        }
    }
}