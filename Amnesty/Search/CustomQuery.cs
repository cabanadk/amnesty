﻿using DisPlay.Umbraco.FullTextSearch;
using Examine.LuceneEngine.Providers;
using Examine.Providers;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Util;

namespace DisPlay.Amnesty.Search
{
    public class CustomQuery : FullTextQuery
    {
        private readonly string _searchTerm;
        private readonly BaseSearchProvider _provider;

        public CustomQuery(string searchTerm, SearchType searchType, BaseSearchProvider provider) : base(searchTerm, searchType, provider)
        {
            _searchTerm = searchTerm;
            _provider = provider;
        }

        protected override BooleanQuery GetCustomQuery()
        {
            if (string.IsNullOrWhiteSpace(_searchTerm))
            {
                return null;
            }

            var booleanQuery = new BooleanQuery();

            var termQuery = new QueryParser(Version.LUCENE_29, "tags", ((BaseLuceneSearcher)_provider).IndexingAnalyzer).Parse(_searchTerm);
            termQuery.SetBoost(10);

            booleanQuery.Add(termQuery, BooleanClause.Occur.MUST);
            return booleanQuery;
        }
    }
}