﻿using System.IO;
using DisPlay.Umbraco.FullTextSearch.Analyzers;
using Lucene.Net.Analysis;

namespace DisPlay.Amnesty.Search
{
    public sealed class CustomAnalyzer : DanishFullTextAnalyzer
    {
        public CustomAnalyzer()
        {
            AddAnalyzer("tags", new LowercaseKeywordAnalyzer());
        }

        private class LowercaseKeywordAnalyzer : Analyzer
        {
            public override TokenStream TokenStream(string fieldName, TextReader reader)
            {
                var tokenizer = new KeywordTokenizer(reader);
                return new LowerCaseFilter(tokenizer);
            }
        }
    }
}