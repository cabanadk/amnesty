﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using DisPlay.Umbraco.FullTextSearch;
using Examine;
using Examine.LuceneEngine;
using Examine.LuceneEngine.Config;
using Lucene.Net.Documents;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace DisPlay.Amnesty.Search
{
    public class CustomIndexer : FullTextUmbracoContentIndexer
    {
        protected override void OnDocumentWriting(DocumentWritingEventArgs docArgs)
        {
            if (docArgs.Fields.ContainsKey("tags"))
            {
                using (var writer = new StringWriter())
                {
                    try
                    {
                        HttpContext.Current = new HttpContext(new HttpRequest("default.aspx", "http://localhost", ""),
                            new HttpResponse(writer));
                        UmbracoContext context =
                            UmbracoContext.EnsureContext(new HttpContextWrapper(HttpContext.Current),
                                ApplicationContext.Current, false);
                        var content = context.ContentCache.GetById(docArgs.NodeId);

                        foreach (
                            IPublishedContent tag in content.GetPropertyValue<IEnumerable<IPublishedContent>>("tags"))
                        {
                            docArgs.Document.Add(new Field("tags", tag.Name, Field.Store.YES, Field.Index.ANALYZED));
                        }
                    }
                    finally
                    {
                        HttpContext.Current = null;
                    }
                }
            }
        }
    }
}