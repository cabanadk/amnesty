﻿using DisPlay.Amnesty.Models;
using Umbraco.Web.Mvc;

namespace DisPlay.Amnesty.Controllers
{
    public class UserInformationController : SurfaceController
    {

        [System.Web.Mvc.HttpPost]
        public void Store(UserInformation info)
        {
            Session["UserInformation"] = info;
        }

    }
}