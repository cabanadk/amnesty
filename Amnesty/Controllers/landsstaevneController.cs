using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using DisPlay.Amnesty.Helpers;
using DisPlay.Amnesty.Models;
using DisPlay.Amnesty.ViewModels;
using log4net;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace DisPlay.Amnesty.Controllers
{
    public class LandsstaevneController : UmbracoApiController
    {
        [System.Web.Http.HttpPost]
        public void HandleCallback()
        {
            var orderId = HttpContext.Current.Request.Form["orderId"];
            processOrder(orderId);
        }

        private void processOrder(string orderId)
        {
            if (string.IsNullOrWhiteSpace(orderId))
            {
                LogHelper.Info(typeof(LandsstaevneController), "orderId is null or empty");
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return;
            }

            var db = DatabaseContext.Database;
            var result = db.SingleOrDefault<LandsstaevneOrderModel>("SELECT * FROM LandsstaevneOrders WHERE OrderId=@0", orderId);
            if (result == null)
            {
                LogHelper.Info(typeof(LandsstaevneController), "No result in database width id " + orderId);
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return;
            }
            result.Payed = true;
            db.Update(result);

            double amount = 0;
            double.TryParse(result.Amount, out amount);
            amount = amount / 100;
            var formNode = Umbraco.TypedContent(result.NodeId);
            var siteSettings = ContentHelper.FindSiteSettings(formNode);

            var emailSender = siteSettings.GetPropertyValue<string>("emailSender");
            var emailLandsstaevneReciver = siteSettings.GetPropertyValue<string>("emailLandsstaevneReciver");
            var landsstaevneTilmeldingEmailBody = siteSettings.GetPropertyValue<string>("landsstaevneTilmeldingEmailBody");
            var landsstaevneEmailTitle = siteSettings.GetPropertyValue<string>("landsstaevneEmailTitle");

            try
            {
                MailMessage mailToCustomer = new MailMessage(emailSender, result.Email)
                {
                    Subject = landsstaevneEmailTitle,
                    Body = landsstaevneTilmeldingEmailBody?.Replace("\n", "<br>").
                    Replace("{name}", result.Name)?.Replace("{surname}", result.SurName.ToString())?.Replace("{relevantinfo}", result.RelevantInfo.ToString())?.Replace("{amount}", amount.ToString())?.Replace("{amount}", amount.ToString())?.Replace("{address}", result.Address)?.Replace("{ZipCity}", result.Zip + ", " + result.City)?.Replace("{phone}", result.Phone)?.Replace("{email}", result.Email)?.Replace("{age}", result.Age)?.Replace("{vegetar}", result.Vegetar)?.Replace("{solution}", result.Solution)?.Replace("{comment}", result.Comment)?.Replace("{paymentoptions}", result.PaymentOptions),
                    IsBodyHtml = true
                };
                SmtpClient client = new SmtpClient();
                client.Send(mailToCustomer);
            }
            catch (Exception e)
            {
                LogHelper.Info(typeof(LandsstaevneController), "Custiomermail could not send: " + e.Message + ". " + e.StackTrace);
            }

            try
            {

                MailMessage mailToAmnesty = new MailMessage(emailSender, emailLandsstaevneReciver)
                {
                    Subject = "Tilmelding til landsmøde - " + formNode.Url,
                    Body = "Meldlemsnummer eller CPR: " + result.MemberOrCPR + "<br>" +
                    "Fornavn: " + result.Name + "<br>" +
                    "Efternavn: " + result.SurName + "<br>" +
                    "Email: " + result.Email + "<br>" +
                    "Telefon: " + result.Phone + "<br>" +
                    "Postnummer: " + result.Zip + "<br>" +
                    "By: " + result.City + "<br>" +
                    "Adresse: " + result.Address + "<br>" +
                    "Alder: " + result.Age + "<br>" +
                    "Vegetar: " + result.Vegetar + "<br>" +
                    "Løsning: " + result.Solution + "<br>" +
                    "Kommentar: " + result.Comment + "<br>" +
                    "Relevant information via e-mail: " + result.RelevantInfo + "<br>" +
                    "Betalingsform: " + result.PaymentOptions + "<br>" +
                    "Beløb: " + amount + "  kr.<br>",
                    IsBodyHtml = true,
                    SubjectEncoding = System.Text.Encoding.UTF8,
                    BodyEncoding = System.Text.Encoding.UTF8
                };

                SmtpClient client = new SmtpClient();
                client.Send(mailToAmnesty);
            }
            catch (Exception e)
            {
                LogHelper.Info(typeof(LandsstaevneController), "Error sending email to " + emailSender + ", error:" + e.StackTrace);
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return;
            }
        }

        [System.Web.Http.HttpGet]
        public int GetOrderId(string email, string name, string surname, string address, string city, string MemberOrCPR, string phone, bool student, string zip, string amount, string age, string vegetar, string solution, string comment, string paymentOptions, int nodeId, bool relevantInfo)
        {
            var db = DatabaseContext.Database;

            if (string.IsNullOrWhiteSpace(amount))
            {
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return 0;
            }
            var temp = new LandsstaevneOrderModel { Email = CheckInput(email), Name = CheckInput(name), SurName = CheckInput(surname), City = CheckInput(city), MemberOrCPR = CheckInput(MemberOrCPR), Phone = CheckInput(phone), Student = false, Zip = zip, Amount = amount, NodeId = nodeId, Address = address, Age = age, Vegetar = vegetar, Comment = comment, Solution = solution, PaymentOptions = paymentOptions, RelevantInfo = relevantInfo,Date = DateTime.Now, Payed = false};
            db.Insert(temp);
            if (!paymentOptions.Equals("Betalingskort"))
            {
                processOrder(temp.OrderId.ToString());
            }
            else
            {
                var formNode = Umbraco.TypedContent(nodeId);
                var siteSettings = ContentHelper.FindSiteSettings(formNode);
                var emailSender = siteSettings.GetPropertyValue<string>("emailSender");
                var emailLandsstaevneReciver = siteSettings.GetPropertyValue<string>("emailLandsstaevneReciver");

                try
                {
                    MailMessage mailToAmnesty = new MailMessage(emailSender, emailLandsstaevneReciver)
                    {
                        Subject = "Gå til betaling Landsmøde - " + formNode.Url,
                        Body = "Følgende er gået til betaling<br>" +
                               "Meldlemsnummer eller CPR: " + MemberOrCPR + "<br>" +
                               "Fornavn: " + temp.Name + "<br>" +
                               "Efternavn: " + temp.SurName + "<br>" +
                               "Email: " + temp.Email + "<br>" +
                               "Telefon: " + temp.Phone + "<br>" +
                               "Postnummer: " + temp.Zip + "<br>" +
                               "By: " + temp.City + "<br>" +
                               "Adresse: " + temp.Address + "<br>" +
                               "Alder: " + temp.Age + "<br>" +
                               "Vegetar: " + temp.Vegetar + "<br>" +
                               "Løsning: " + temp.Solution + "<br>" +
                               "Kommentar: " + temp.Comment + "<br>" +
                               "Relevant information via e-mail: " + temp.RelevantInfo + "<br>" +
                               "Betalingsform: " + temp.PaymentOptions + "<br>" +
                               "Beløb: " + amount + "  kr.<br>",
                        IsBodyHtml = true,
                        SubjectEncoding = System.Text.Encoding.UTF8,
                        BodyEncoding = System.Text.Encoding.UTF8
                    };

                    SmtpClient client = new SmtpClient();
                    client.Send(mailToAmnesty);
                }
                catch (Exception e)
                {
                    LogHelper.Info(typeof(LandsstaevneController), "Error sending email to " + emailSender + ", error:" + e.StackTrace);
                    HttpContext.Current.Response.StatusCode = 400;
                    HttpContext.Current.Response.End();
                }
            }
            return temp.OrderId;
        }

        private string CheckInput(string input)
        {
            if (input == null)
            {
                return "";
            }
            return input;
        }
    }
}