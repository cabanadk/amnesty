﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Examine;
using Examine.LuceneEngine.SearchCriteria;
using Examine.Providers;
using Examine.SearchCriteria;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Core.Services;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Enums;
using Umbraco.Forms.Data.Storage;
using Umbraco.Forms.Web.BusinessLogic;
using Umbraco.Forms.Web.Models.Backoffice;
using Umbraco.Web;

namespace DisPlay.Amnesty.Controllers
{
    public class FormRecordSearcher2
    {
        public static BaseSearchProvider Current()
        {
            return ExamineManager.Instance.SearchProviderCollection["FormsSearcher"];
        }

        internal static EntrySearchResultCollection QueryDataBase(RecordFilter model, DatabaseContext databaseContext)
        {
            IMemberService memberService = ApplicationContext.Current.Services.MemberService;
            using (FormStorage formStorage = new FormStorage())
            {
                Form form = formStorage.GetForm(model.Form);
                Sql sql = FormRecordSearcher2.BuildSqlQuery(model, "");
                Page<Record> page = databaseContext.Database.Page<Record>((long)model.StartIndex, (long)model.Length, sql);
                EntrySearchResultCollection resultCollection = new EntrySearchResultCollection();
                resultCollection.TotalNumberOfResults = page.TotalItems;
                resultCollection.TotalNumberOfPages = page.TotalPages;
                bool flag = Configuration.IsTrial && !global::Umbraco.Forms.Core.Licensing.IsLocalBrowserRequest();
                List<EntrySearchResult> list1 = new List<EntrySearchResult>();
                if (form != null)
                {
                    List<EntrySearchResultSchema> list2 = new List<EntrySearchResultSchema>();
                    foreach (Field field in form.AllFields)
                    {
                        if (field.FieldType.StoresData)
                            list2.Add(new EntrySearchResultSchema()
                            {
                                Name = field.Caption,
                                Id = field.Id.ToString(),
                                View = field.FieldType.RenderView
                            });
                    }
                    list2.Add(new EntrySearchResultSchema()
                    {
                        Id = "member",
                        View = "member",
                        Name = "Member"
                    });
                    list2.Add(new EntrySearchResultSchema()
                    {
                        Id = "state",
                        View = "text",
                        Name = "State"
                    });
                    list2.Add(new EntrySearchResultSchema()
                    {
                        Id = "created",
                        View = "date",
                        Name = "Created"
                    });
                    list2.Add(new EntrySearchResultSchema()
                    {
                        Id = "updated",
                        View = "date",
                        Name = "Updated"
                    });
                    resultCollection.schema = (IEnumerable<EntrySearchResultSchema>)list2;
                }
                foreach (Record record in page.Items)
                {
                    EntrySearchResult entrySearchResult = new EntrySearchResult();
                    entrySearchResult.Id = record.Id;
                    entrySearchResult.Form = record.Form.ToString();
                    entrySearchResult.State = record.StateAsString;
                    entrySearchResult.Created = record.Created;
                    entrySearchResult.Updated = record.Updated;
                    entrySearchResult.UniqueId = record.UniqueId;
                    if (!string.IsNullOrEmpty(record.MemberKey))
                    {
                        IMember byId = memberService.GetById(Convert.ToInt32(record.MemberKey));
                        if (byId != null)
                            entrySearchResult.Member = byId;
                    }
                    if (form != null && !string.IsNullOrEmpty(record.RecordData))
                    {
                        Dictionary<string, string> dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(record.RecordData);
                        List<object> list2 = new List<object>();
                        foreach (EntrySearchResultSchema searchResultSchema in Enumerable.Take<EntrySearchResultSchema>(resultCollection.schema, Enumerable.Count<EntrySearchResultSchema>(resultCollection.schema) - 4))
                        {
                            if (dictionary.ContainsKey(searchResultSchema.Id))
                                list2.Add(flag ? (object)Configuration.TrialSaveMessage : (object)dictionary[searchResultSchema.Id]);
                            else
                                list2.Add((object)string.Empty);
                        }
                        list2.Add((object)entrySearchResult.Member);
                        list2.Add((object)entrySearchResult.State);
                        list2.Add((object)entrySearchResult.Created);
                        list2.Add((object)entrySearchResult.Updated);
                        entrySearchResult.Fields = (IEnumerable<object>)list2;
                    }
                    list1.Add(entrySearchResult);
                }
                resultCollection.Results = (IEnumerable<EntrySearchResult>)list1;
                return resultCollection;
            }
        }

        internal static EntrySearchResultCollection QueryLucene(RecordFilter model)
        {
            FormStorage formStorage = new FormStorage();
            Form form = formStorage.GetForm(model.Form);
            formStorage.Dispose();
            BaseSearchProvider searcher = FormRecordSearcher2.Current();
            ISearchCriteria searchParams = FormRecordSearcher2.BuildLuceneQuery(model, searcher);
            ISearchResults searchResults = searcher.Search(searchParams);
            EntrySearchResultCollection resultCollection = new EntrySearchResultCollection();
            resultCollection.TotalNumberOfResults = (long)searchResults.TotalItemCount;
            List<EntrySearchResult> list1 = new List<EntrySearchResult>();
            if (form != null)
            {
                List<EntrySearchResultSchema> list2 = new List<EntrySearchResultSchema>();
                list2.Add(new EntrySearchResultSchema()
                {
                    Id = "state",
                    View = "text",
                    Name = "State"
                });
                list2.Add(new EntrySearchResultSchema()
                {
                    Id = "created",
                    View = "date",
                    Name = "Created"
                });
                list2.Add(new EntrySearchResultSchema()
                {
                    Id = "updated",
                    View = "date",
                    Name = "Updated"
                });
                foreach (Field field in form.AllFields)
                {
                    if (field.FieldType.StoresData)
                        list2.Add(new EntrySearchResultSchema()
                        {
                            Name = field.Caption,
                            Id = field.Id.ToString(),
                            View = field.FieldType.RenderView
                        });
                }
                resultCollection.schema = (IEnumerable<EntrySearchResultSchema>)list2;
            }
            foreach (SearchResult searchResult in Enumerable.Take<SearchResult>(searchResults.Skip(model.StartIndex), model.Length))
            {
                EntrySearchResult entrySearchResult = new EntrySearchResult();
                entrySearchResult.Score = searchResult.Score;
                entrySearchResult.Id = searchResult.Id;
                IDictionary<string, string> fields = searchResult.Fields;
                entrySearchResult.Form = fields["Form"];
                entrySearchResult.State = fields["State"];
                DateTime result1;
                DateTime.TryParseExact(fields["Created"], "yyyyMMddHHmmssFFF", (IFormatProvider)CultureInfo.InvariantCulture, DateTimeStyles.None, out result1);
                DateTime result2;
                DateTime.TryParseExact(fields["Updated"], "yyyyMMddHHmmssFFF", (IFormatProvider)CultureInfo.InvariantCulture, DateTimeStyles.None, out result2);
                entrySearchResult.Created = result1;
                entrySearchResult.Updated = result2;
                if (form != null)
                {
                    Dictionary<string, string> dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(fields["Fields"]);
                    List<object> list2 = new List<object>();
                    list2.Add((object)entrySearchResult.State);
                    list2.Add((object)entrySearchResult.Created);
                    list2.Add((object)entrySearchResult.Updated);
                    foreach (EntrySearchResultSchema searchResultSchema in Enumerable.Skip<EntrySearchResultSchema>(resultCollection.schema, 3))
                    {
                        if (dictionary.ContainsKey(searchResultSchema.Id))
                            list2.Add((object)dictionary[searchResultSchema.Id]);
                        else
                            list2.Add((object)"");
                    }
                    entrySearchResult.Fields = (IEnumerable<object>)list2;
                }
                list1.Add(entrySearchResult);
            }
            resultCollection.Results = (IEnumerable<EntrySearchResult>)list1;
            return resultCollection;
        }

        internal static Sql BuildSqlQuery(RecordFilter model, string select = "")
        {
            if (model.StartIndex == 0)
                model.StartIndex = 0;
            if (model.Length == 0)
                model.Length = 50;
            if (model.StartDate == new DateTime())
                model.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(2000.0));
            if (model.EndDate == new DateTime())
                model.EndDate = DateTime.Now;
            if (model.States == null)
                model.States = new List<FormState>()
        {
          FormState.Approved,
          FormState.Submitted
        };
            Sql sql1 = new Sql();
            if (!string.IsNullOrEmpty(select))
                sql1 = sql1.Select((object)select);
            Sql sql2 = PetaPocoSqlExtensions.From<Record>(sql1);
            sql2.Where("Created >= @0 AND Created <= @1", (object)model.StartDate, (object)model.EndDate.AddDays(1.0));
            if (model.Form != Guid.Empty)
                sql2 = sql2.Where("Form = @0", (object)model.Form.ToString());
            if (!string.IsNullOrEmpty(model.Filter))
                sql2 = sql2.Where("RecordData LIKE @0", (object)("%" + model.Filter + "%"));
            if (Enumerable.Any<FormState>((IEnumerable<FormState>)model.States))
                sql2 = sql2.Where("State in (@states)", (object)new
                {
                    states = Enumerable.ToList<string>(Enumerable.Select<FormState, string>((IEnumerable<FormState>)model.States, (Func<FormState, string>)(state => state.ToString())))
                });
            if (!string.IsNullOrEmpty(model.SortBy))
            {
                if (model.SortOrder == Sorting.descending)
                    sql2 = PetaPocoSqlExtensions.OrderByDescending(sql2, new object[1]
                    {
            (object) model.SortBy
                    });
                else
                    sql2 = sql2.OrderBy((object)model.SortBy);
            }
            return sql2;
        }

        internal static ISearchCriteria BuildLuceneQuery(RecordFilter model, BaseSearchProvider searcher)
        {
            if (model.StartIndex == 0)
                model.StartIndex = 0;
            if (model.Length == 0)
                model.Length = 50;
            if (model.StartDate == new DateTime())
                model.StartDate = DateTime.Now.Subtract(TimeSpan.FromDays(2.0));
            if (model.EndDate == new DateTime())
                model.EndDate = DateTime.Now;
            if (model.States == null)
                model.States = new List<FormState>()
        {
          FormState.Approved,
          FormState.Submitted
        };
            IBooleanOperation booleanOperation = searcher.CreateSearchCriteria().Field("__IndexType", "formrecord").And().Range("Created", model.StartDate, model.EndDate);
            if (model.Form != Guid.Empty)
                booleanOperation = booleanOperation.And().Field("Form", model.Form.ToString());
            if (!string.IsNullOrEmpty(model.Filter))
                booleanOperation = booleanOperation.And().Field("Blob", LuceneSearchExtensions.MultipleCharacterWildcard(model.Filter));
            return booleanOperation.Compile();
        }
    }
}
