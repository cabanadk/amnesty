﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DisPlay.Amnesty.Helpers;
using DisPlay.Amnesty.Search;
using DisPlay.Amnesty.ViewModels;
using DisPlay.Amnesty.ViewModels.Models;
using DisPlay.Umbraco.FullTextSearch;
using Examine;
using Examine.LuceneEngine.Providers;
using Examine.Providers;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace DisPlay.Amnesty.Controllers
{
    public class SearchController : RenderMvcController
    {
        public ActionResult Index()
        {
            var searchModel = (SearchViewModel)CurrentPage;
            var searchResults = new List<SearchResultModel>();
            var tags = new List<string>();

            var query = Request.QueryString["q"];

            var frontpage = CurrentPage.AncestorOrSelf("frontpage")??CurrentPage.AncestorOrSelf("subsiteForside");
            if (frontpage != null)
            {
                BaseSearchProvider provider = ExamineManager.Instance.SearchProviderCollection[Config.Instance.DefaultIndex + "Searcher"];


                var fulltextQuery = new CustomQuery(query, SearchType.And, provider).StartFrom(frontpage).CreateQuery();              
                
                searchResults = Umbraco.FullTextSearch(query, fulltextQuery).Where(result=>!(result==null || result.Content==null)).Select(result => new SearchResultModel
                {
                    Content = ((BaseViewModel)result.Content),
                    Title = result.Title(),
                    Snippet = result.Summary(),
                    Url = result.Url,
                    Description = ((BaseViewModel) result.Content).SeoMetaData.SEODescription
                }).ToList();
            }

            var sharedContent = ContentHelper.FindSharedContent(searchModel);
            if (sharedContent != null)
            {
                var searchFolder = sharedContent.Children.FirstOrDefault(_ => _.DocumentTypeAlias == "SearchTagsFolder");
                if (searchFolder != null)
                {
                    tags = searchFolder.Children.Select(_ => _.Name).ToList();
                }
            }

            searchModel.SearchResults = searchResults;
            searchModel.SearchTags = tags;
            searchModel.SearchQuery = query;
            
            return CurrentTemplate(searchModel);
        }
    }
}