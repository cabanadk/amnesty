﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using DisPlay.Amnesty.Helpers;
using Umbraco.Core;
using Umbraco.Forms.Mvc.DynamicObjects;
using Umbraco.Web;
using Umbraco.Forms;
using Umbraco.Forms.Core;
using Umbraco.Forms.Data.Storage;
using Umbraco.Web.WebApi;

namespace DisPlay.Amnesty.Controllers
{
    public class SamletAntalUnderskrifterController: UmbracoApiController
    {
        //Return the manually added signatures from the backend. 
        [System.Web.Http.HttpGet]
        public int Get(Guid formGuid)
        {
            var memoryCache = MemoryCache.Default;
            if (!memoryCache.Contains("Amnesty.Forms.TotalNumberOfSignatures" + formGuid))
            {
                FormStorage fs = new FormStorage();
                Form form = fs.GetForm(formGuid);

                //Get manually entered signatures
                int manualAddedSignatures = 0;
                foreach (Field field in form.AllFields)
                {
                    if (field.Caption.Equals("ekstra-underskrifter"))
                    {
                        int.TryParse(field.ToolTip, out manualAddedSignatures);
                    }
                }


                //get number of records, equal to number of signatures from the form 
                RecordStorage rs = new RecordStorage();
                int signaturesFromForm = rs.GetAllRecords(form, false).Count; //Must use false, otherwise this is really slow! 

                var experation = DateTimeOffset.UtcNow.AddSeconds(30);
                int totalNumberOfSignatures = manualAddedSignatures + signaturesFromForm;

                memoryCache.Add("Amnesty.Forms.TotalNumberOfSignatures" + formGuid, totalNumberOfSignatures, experation);
            }

            return (int)memoryCache.Get("Amnesty.Forms.TotalNumberOfSignatures" + formGuid);
        }
    }
}