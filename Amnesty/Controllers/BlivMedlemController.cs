﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ApsisCSharpApi.Helpers;
using ApsisCSharpApi.Models.General;
using ApsisCSharpApi.Models.Transactional;
using ApsisCSharpApi.Service.Requests.Subscriber;
using ApsisCSharpApi.Service.Requests.Transactional;
using DisPlay.Amnesty.Helpers;
using DisPlay.Amnesty.Models;
using DisPlay.Amnesty.ViewModels;
using log4net;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace DisPlay.Amnesty.Controllers
{
    public class BlivMedlemController : UmbracoApiController
    {

        [System.Web.Http.HttpGet]
        public bool SendEmail(string email, string name, string surname, string address, string city, string cpr, bool student, string zip,string amount,int nodeId,string reg,string konto, string phone="")
        {

            if (string.IsNullOrWhiteSpace(amount))
            {
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return false;
            }

            try
            {
                var db = DatabaseContext.Database;

                var temp = new BlivMedlemOrderModel
                {
                    Email = CheckInput(email),
                    Name = CheckInput(name),
                    SurName = CheckInput(surname),
                    City = CheckInput(city),
                    Cpr = CheckInput(cpr),
                    Phone = CheckInput(phone),
                    Student = student,
                    Zip = zip,
                    Amount = amount,
                    NodeId = nodeId,
                    Reg = reg,
                    Konto = konto,
                    Address = address,
                    Date = DateTime.Now
                };
                db.Insert(temp);
            }
            catch (Exception e)
            {
                LogHelper.Info(typeof(BlivMedlemController), "Error in BlivMedlemController, not saved in DB: " + e.StackTrace);
            }

            double doubleAmount = 0;
            double.TryParse(amount, out doubleAmount);
            doubleAmount = doubleAmount / 100;
            var formNode = Umbraco.TypedContent(nodeId);
            var siteSettings = ContentHelper.FindSiteSettings(formNode);
            var emailSender = siteSettings.GetPropertyValue<string>("emailSender");
            var emailMemberReciver = siteSettings.GetPropertyValue<string>("emailMemberReciver");
            var emailMemberReciver2 = siteSettings.GetPropertyValue<string>("emailMemberReciver2");
            var memberEmail = siteSettings.GetPropertyValue<string>("memberEmail");
            var memberEmailTitle = siteSettings.GetPropertyValue<string>("memberEmailTitle");
            var BlivMedlemTransactionalProjectID = siteSettings.GetPropertyValue<string>("BlivMedlemTransactionalProjectID");

            try
            {

                var subscriber = new GetSubscriberIdByEmail().Get(email);
                List<DemDataField> demographic = new List<DemDataField>();
                if (subscriber.Code == 1)
                    demographic = new GetSubscriberDetailsById().Get(subscriber.Result).Result.DemDataFields;
                else
                    demographic.AddOrUpdate("Ukendt", "Y");
                demographic.AddOrUpdate("Fornavn", name);
                demographic.AddOrUpdate("Efternavn", surname);
                demographic.AddOrUpdate("Bidrag_fra_formular", doubleAmount.ToString());
                demographic.AddOrUpdate("Adresse", address);
                demographic.AddOrUpdate("Postnummer", zip);
                demographic.AddOrUpdate("Mobil", phone);
                new SendTransactionalEmail().Send(ConfigurationManager.AppSettings.Get("ApsisBecomeMemberTransactionalProjectId"),
                    new SendTransactionalEmailBody() { Email = email, DemDataFields = demographic, Name = name });
            }
            catch (Exception e)
            {
                LogHelper.Error(typeof(BlivMedlemController), "Error sending mail to user: ",e);
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return false;
            }
            try
            {
                cpr = CheckInput(cpr).Replace("-", "");
                if (cpr.Length >= 10)
                {
                  cpr = cpr.Substring(0, 6) + "-" + cpr.Substring(6, 4);  
                }
                
                MailMessage mailToAmnesty = new MailMessage(emailSender, emailMemberReciver)
                {
                    CC = { emailMemberReciver2 },
                    Subject = "Nyt medlem - " + formNode.Url,
                    Body = "Fornavn: " + name +"<br>"+
                    "Efternavn: "+ surname+ "<br>" +
                    "Email: " + email + "<br>" +
                    "Telefon: " + phone + "<br>" +
                    "CPR: " + cpr + "<br>" +
                    "Postnummer: " + zip + "<br>" +
                    "By: " + city + "<br>" +
                    "Adresse: " + address +"<br>"+
                    "Studerende, pensionist eller ledig: " + student + "<br>" +
                    "Beløb: " + doubleAmount + "<br>" +
                    "Reg: " + reg + "<br>"+
                    "konto: " + konto + "<br>"+
                    "Donationstype: Løbende",
                    IsBodyHtml = true,
                    SubjectEncoding = System.Text.Encoding.UTF8,
                    BodyEncoding = System.Text.Encoding.UTF8
                };
                SmtpClient client = new SmtpClient();
                client.Send(mailToAmnesty);
            }
            catch (Exception e)
            {
                LogHelper.Info(typeof(BlivMedlemController), "Error sending mail to amnesty: " + e.StackTrace);
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return false;
            }
            return true;
        }

        private string CheckInput(string input)
        {
            if (input == null)
            {
                return "";
            }
            return input;
        }
    }
}