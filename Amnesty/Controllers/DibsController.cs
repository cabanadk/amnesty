using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ApsisCSharpApi.Helpers;
using ApsisCSharpApi.Models.General;
using ApsisCSharpApi.Models.Transactional;
using ApsisCSharpApi.Service.Requests.Subscriber;
using ApsisCSharpApi.Service.Requests.Transactional;
using DisPlay.Amnesty.Helpers;
using DisPlay.Amnesty.Models;
using DisPlay.Amnesty.ViewModels;
using log4net;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace DisPlay.Amnesty.Controllers
{
    public class DibsController : UmbracoApiController
    {
        [System.Web.Http.HttpPost]
        public void HandleCallback()
        {
            var orderId = HttpContext.Current.Request.Form["orderId"];
            if (string.IsNullOrWhiteSpace(orderId))
            {
                LogHelper.Info(typeof(DibsController), "orderId is null or empty"); 
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return;
            }

            var db = DatabaseContext.Database;
            var result = db.SingleOrDefault<DibsOrderModel>("SELECT * FROM DibsOrders WHERE OrderId=@0", orderId);
            if (result == null)
            {
                LogHelper.Info(typeof(DibsController), "No result in database width id "+orderId); 
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return;
            }

            double amount = 0;
            double.TryParse(result.Amount, out amount);
            amount = amount/100;
            var formNode = Umbraco.TypedContent(result.NodeId);
            var siteSettings = ContentHelper.FindSiteSettings(formNode);

            var emailSender = siteSettings.GetPropertyValue<string>("emailSender");
            var emailDibsReciver = siteSettings.GetPropertyValue<string>("emailDibsReciver");
            var dibsEmail = siteSettings.GetPropertyValue<string>("dibsEmail");
            var dibsEmailTitle = siteSettings.GetPropertyValue<string>("dibsEmailTitle");

            try
            {
                var subscriber = new GetSubscriberIdByEmail().Get(result.Email);
                List<DemDataField> demographic = new List<DemDataField>();
                if (subscriber.Code == 1)
                    demographic = new GetSubscriberDetailsById().Get(subscriber.Result).Result.DemDataFields;
                else
                    demographic.AddOrUpdate("Ukendt", "Y");
                demographic.AddOrUpdate("Fornavn", result.Name);
                demographic.AddOrUpdate("Efternavn", result.SurName);
                demographic.AddOrUpdate("Bidrag_fra_formular", amount.ToString());
                demographic.AddOrUpdate("Adresse", result.Address);
                demographic.AddOrUpdate("Postnummer", result.Zip);
                demographic.AddOrUpdate("Mobil", result.Phone);
                new SendTransactionalEmail().Send(ConfigurationManager.AppSettings.Get("ApsisContributeTransactionalProjectId"),
                    new SendTransactionalEmailBody() {Email = result.Email, DemDataFields = demographic, Name = result.Name});
            }
            catch (Exception e)
            {
                LogHelper.Error(typeof(DibsController), "Error sending mail to user: ", e);
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return;
            }

            try
            {
                var cpr = result.Cpr.Replace("-","");

                cpr = cpr.Replace("-", "");
                if (cpr.Length >= 10)
                {
                    cpr = cpr.Substring(0, 6) + "-" + cpr.Substring(6, 4);
                }

                MailMessage mailToAmnesty = new MailMessage(emailSender, emailDibsReciver)
                {
                    Subject = "Ny støtte - "+formNode.Url,
                    Body = "Fornavn: "+result.Name +"<br>"+
                    "Efternavn: "+ result.SurName +"<br>"+
                    "Email: " + result.Email + "<br>" +
                    "Telefon: " + result.Phone + "<br>" +
                    "CPR: " + cpr + "<br>" +
                    "Postnummer: " + result.Zip + "<br>" +
                    "By: " + result.City + "<br>" +
                    "Adresse: "+ result.Address + "<br>"+
                    "Beløb: " + amount + "<br>" +
                    "Donationstype: one_time",
                    IsBodyHtml = true,
                    SubjectEncoding = System.Text.Encoding.UTF8,
                    BodyEncoding =  System.Text.Encoding.UTF8
                };

                SmtpClient client = new SmtpClient();
                client.Send(mailToAmnesty);
            }
            catch (Exception e)
            {
                LogHelper.Info(typeof(DibsController), "Error sending email to " + emailSender+", error:" + e.StackTrace); 
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return;
            }
        }

        [System.Web.Http.HttpGet]
        public int GetOrderId(string email, string name, string surname, string address, string city, string cpr, string phone, bool student, string amount, int nodeId, string zip="")
        {
            var db = DatabaseContext.Database;

            if (string.IsNullOrWhiteSpace(amount))
            {
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return 0;
            }
            var temp = new DibsOrderModel { Email = CheckInput(email), Name = CheckInput(name), SurName = CheckInput(surname), City = CheckInput(city), Cpr = CheckInput(cpr), Phone = CheckInput(phone), Student = false, Zip = zip, Amount = amount, NodeId = nodeId,Address = address, Date = DateTime.Now};
            db.Insert(temp);
            return temp.OrderId;
        }

        private string CheckInput(string input)
        {
            if (input == null)
            {
                return "";
            }
            return input;
        }
    }
}