﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using DisPlay.Amnesty.Helpers;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace DisPlay.Amnesty.Controllers
{
    public class CprController : UmbracoApiController
    {
        [System.Web.Http.HttpGet]
        public bool SendEmail(string email, string name, string surname, string cpr, string memberNumber, int nodeId)
        {
            if (string.IsNullOrWhiteSpace(cpr) || string.IsNullOrWhiteSpace(memberNumber))
            {
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return false;
            }

            var formNode = Umbraco.TypedContent(nodeId);
            var siteSettings = ContentHelper.FindSiteSettings(formNode);
            var emailSender = siteSettings.GetPropertyValue<string>("emailSender");
            var emailCprReciver = siteSettings.GetPropertyValue<string>("emailCprReciver");
            var cprEmail = siteSettings.GetPropertyValue<string>("cprEmail");
            var cprEmailTitle = siteSettings.GetPropertyValue<string>("cprEmailTitle");


            try
            {
                MailMessage mailToCustomer = new MailMessage(emailSender, email)
                {
                    Subject = cprEmailTitle,
                    Body = cprEmail.Replace("\n", "<br>").
                        Replace("{name}", name).Replace("{surname}", surname),
                    IsBodyHtml = true
                };
                SmtpClient client = new SmtpClient();
                client.Send(mailToCustomer);
            }
            catch (Exception e)
            {
                LogHelper.Info(typeof(BlivMedlemController), "Error sending mail to user: " + e.StackTrace);
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return false;
            }
            try
            {
                cpr = CheckInput(cpr).Replace("-", "");
                if (cpr.Length >= 10)
                {
                    cpr = cpr.Substring(0, 6) + "-" + cpr.Substring(6, 4);
                }

                MailMessage mailToAmnesty = new MailMessage(emailSender, emailCprReciver)
                {
                    Subject = "CPR forespørgsel - " + formNode.Url,
                    Body = "Fornavn: " + name + "<br>" +
                           "Efternavn: " + surname + "<br>" +
                           "Email: " + email + "<br>" +
                           "Medlemsnummer: " + memberNumber + "<br>" +
                           "CPR: " + cpr + "<br>",
                    IsBodyHtml = true,
                    SubjectEncoding = System.Text.Encoding.UTF8,
                    BodyEncoding = System.Text.Encoding.UTF8
                };
                SmtpClient client = new SmtpClient();
                client.Send(mailToAmnesty);
            }
            catch (Exception e)
            {
                LogHelper.Info(typeof(BlivMedlemController), "Error sending mail to amnesty: " + e.StackTrace);
                HttpContext.Current.Response.StatusCode = 400;
                HttpContext.Current.Response.End();
                return false;
            }
            return true;
        }

        private string CheckInput(string input)
        {
            if (input == null)
            {
                return "";
            }
            return input;
        }
    }
}