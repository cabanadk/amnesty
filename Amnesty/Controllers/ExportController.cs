﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Http;
using Ctl.Data;
using DisPlay.Amnesty.Helpers;
using DisPlay.Amnesty.Models;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Forms.Core;
using Umbraco.Forms.Data.Storage;
using Umbraco.Forms.Web.Providers.Export;
using Umbraco.Web.WebApi;

namespace DisPlay.Amnesty.Controllers
{
    public class ExportController : UmbracoApiController
    {
        [HttpGet]
        public int ExportForms()
        {
            return ExportHelper.ExportForms();
        }
    }
}