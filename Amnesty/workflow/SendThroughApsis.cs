﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using ApsisCSharpApi.Helpers;
using ApsisCSharpApi.Models.General;
using ApsisCSharpApi.Models.Subscriber;
using ApsisCSharpApi.Models.Transactional;
using ApsisCSharpApi.Service.Requests.Subscriber;
using ApsisCSharpApi.Service.Requests.Transactional;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Attributes;
using Umbraco.Forms.Core.Enums;

namespace DisPlay.Amnesty.workflow
{
    public class SendThroughApsis : WorkflowType
    {
        [Setting("Transactional project", description = "select a transactional project", view = "apsis")]
        public string ApsisConfig { get; set; }

        public SendThroughApsis()
        {
            this.Id = new Guid("D8A44E3F-2F3A-4748-B36C-6F39DAEA08B6");
            this.Name = "Send transactional email through apsis";
            this.Description = "Sends a message through apsis";
            this.Icon = "icon-documents";
            this.Group = "Legacy";
        }

        public override WorkflowExecutionStatus Execute(Record record, RecordEventArgs e)
        {
            var apsisMapper = JsonConvert.DeserializeObject<ApsisMapper>(this.ApsisConfig);
            var transactproject = apsisMapper.Project;
            var mail = record.RecordFields[new Guid(apsisMapper.Email)].ValuesAsString(false);

            var result = new GetSubscriberIdByEmail().Get(mail);
            List<DemDataField> demographic = new List<DemDataField>();
            SubscriberDetails knownUserDetails = null;
            if (result.Code == 1)
            {
                knownUserDetails = new GetSubscriberDetailsById().Get(result.Result).Result;
                demographic = knownUserDetails.DemDataFields;
            }
            else
                demographic.AddOrUpdate("Ukendt", "Y");

            foreach (var dataMapping in apsisMapper.Properties)
            {
                if (dataMapping.HasValue())
                {
                    var str = dataMapping.StaticValue;

                    if (!string.IsNullOrEmpty(dataMapping.Field))
                       str = record.RecordFields[new Guid(dataMapping.Field)].ValuesAsString(false);

                    if (!string.IsNullOrEmpty(str))
                        demographic.AddOrUpdate(dataMapping.Alias,str);
                }
            }

            //Fire and forget call to update function, this is redundant to nightly sync, but will send in updates so they are available earlier.
            try
            {
                if (knownUserDetails != null)
                {
                    //Update the user before sending the transactional mail
                    var model = new List<UpdateQueuedSubscriberModel> {new UpdateQueuedSubscriberModel
                {
                    Id = knownUserDetails.Id,
                    Name = knownUserDetails.Name,
                    Password = knownUserDetails.Password,
                    CountryCode = knownUserDetails.CountryCode,
                    PhoneNumber = knownUserDetails.PhoneNumber,
                    Email = knownUserDetails.Email,
                    DemDataFields = demographic
                }};
                    new UpdateSubscribers().Post(model);
                }
            }
            catch
            {
                // ignored
            }


            return new SendTransactionalEmail().Send(transactproject, new SendTransactionalEmailBody() { Email = mail, DemDataFields = demographic}).Code ==1 ? WorkflowExecutionStatus.Completed : WorkflowExecutionStatus.Failed;
        }


        public override List<Exception> ValidateSettings()
        {
            List<Exception> list = new List<Exception>();
         
            return list;
        }
    }
}