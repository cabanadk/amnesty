﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime;
using System.Runtime.Serialization;
using System.Web.Http;
using ApsisCSharpApi.Service.Requests.Accounts;
using ApsisCSharpApi.Service.Requests.Transactional;
using Umbraco.Core.IO;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Enums;
using Umbraco.Forms.Core.Providers;
using Umbraco.Forms.Core.Providers.Models;
using Umbraco.Forms.Data.Storage;
using Umbraco.Forms.Web.Models.Backoffice;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;


namespace DisPlay.Amnesty.workflow
{
    [PluginController("UmbracoForms")]
    public class ApsisController : UmbracoAuthorizedJsonController
    {
        public IEnumerable<PickerItem> GetAllProjects()
        {
            var result = new GetTransactionalProjects().Get();
           return result.Result.Select(x => new PickerItem() {Id = x.ProjectId.ToString(), Value = x.NewsletterName}).ToList();
           
        }
        public IEnumerable<PickerItem> GetAllDemographicFields()
        {
            return new GetDemograpicData().Get().Result.Demographics.Where(x=>!string.IsNullOrEmpty(x.Key)).Select(x=>new PickerItem() {Id = x.Key,Value = x.Key});
        }
    }

 
}
