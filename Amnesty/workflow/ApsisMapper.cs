using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DisPlay.Amnesty.workflow
{
    [DataContract(Name = "ApsisMapper")]
    public class ApsisMapper
    {
        [DataMember(Name = "project")]
        public string Project { get; set; }
        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "properties")]
        public IEnumerable<DemographicDataMapping> Properties { get; set; }
    }
}