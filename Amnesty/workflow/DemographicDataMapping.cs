﻿using System.Runtime.Serialization;

namespace DisPlay.Amnesty.workflow
{
    [DataContract(Name = "DemographicDataMapping")]
    public class DemographicDataMapping
    {
        [DataMember(Name = "id")]
        public string Alias { get; set; }

        [DataMember(Name = "field")]
        public string Field { get; set; }

        [DataMember(Name = "staticValue")]
        public string StaticValue { get; set; }

        public bool HasValue()
        {
            return !string.IsNullOrEmpty(this.Field) || !string.IsNullOrEmpty(this.StaticValue);
        }
    }
}