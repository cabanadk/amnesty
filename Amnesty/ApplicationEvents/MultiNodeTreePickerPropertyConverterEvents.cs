﻿using Our.Umbraco.PropertyConverters;
using Umbraco.Core;
using Umbraco.Core.PropertyEditors;

namespace DisPlay.Amnesty.ApplicationEvents
{
    public class MultiNodeTreePickerPropertyConverterEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            PropertyValueConvertersResolver.Current.RemoveType<MultiNodeTreePickerPropertyConverter>();
            PropertyValueConvertersResolver.Current.RemoveType<DisPlay.Umbraco.ValueConverters.MultiNodeTreePickerValueConverter>();
        }
    }
}