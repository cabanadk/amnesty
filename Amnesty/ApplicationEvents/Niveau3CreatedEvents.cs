﻿using Archetype.Models;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace DisPlay.Amnesty.ApplicationEvents
{
    public class Niveau3CreateEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Created += ContentService_Created;
        }

        void ContentService_Created(IContentService sender, NewEventArgs<IContent> e)
        {
            if (e.Entity.ContentType.Alias == "niveau3")
            {
                e.Entity.Properties["sections"].Value =
                    JsonConvert.SerializeObject(new ArchetypeModel
                    {
                        Fieldsets = new[]
                        {
                            new ArchetypeFieldsetModel()
                            {
                                Alias = "section",
                                Properties = new[]
                                {
                                    new ArchetypePropertyModel() {Alias = "sectionName", Value = "Kort sagt"},
                                }
                            },
                            new ArchetypeFieldsetModel()
                            {
                                Alias = "section",
                                Properties = new[]
                                {
                                    new ArchetypePropertyModel() {Alias = "sectionName", Value = "Eksempler"},
                                }
                            },
                            new ArchetypeFieldsetModel()
                            {
                                Alias = "section",
                                Properties = new[]
                                {
                                    new ArchetypePropertyModel() {Alias = "sectionName", Value = "Vil du vide mere"},
                                }
                            },
                        }
                    });
            }
        }
    }
}