using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace DisPlay.Amnesty.ApplicationEvents
{
    public class HttpsRequestPreparedEvent : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
            PublishedContentRequest.Prepared += PublishedContentRequest_Prepared;
        }
        
            

        void PublishedContentRequest_Prepared(object sender, EventArgs e)
        {
            //check if we should redirect to https
            if (HttpContext.Current.Request.Url.Host == "localhost")
            {
                return;
            }
            if (HttpContext.Current.Request.IsSecureConnection)
            {
                //if its https just return
                return;
            }
            var publishedContentRequest = (PublishedContentRequest)sender;
            publishedContentRequest.SetRedirectPermanent(HttpContext.Current.Request.Url.AbsoluteUri.Replace("http", "https"));
        }
    }

}