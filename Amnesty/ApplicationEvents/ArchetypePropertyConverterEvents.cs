﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.PropertyEditors;

namespace DisPlay.Amnesty.ApplicationEvents
{
    public class ArchetypePropertyConverterEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            PropertyValueConvertersResolver.Current.RemoveType<DisPlay.Umbraco.ValueConverters.ArchetypeModuleBaseValueConverter>();
        }
    }
}