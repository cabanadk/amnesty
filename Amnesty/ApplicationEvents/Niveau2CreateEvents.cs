﻿using Archetype.Models;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace DisPlay.Amnesty.ApplicationEvents
{
    public class Niveau2CreateEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Created += ContentService_Created;
        }

        void ContentService_Created(IContentService sender, NewEventArgs<IContent> e)
        {
            if (e.Entity.ContentType.Alias == "niveau2")
            {
                e.Entity.Properties["sections"].Value =
                    JsonConvert.SerializeObject(new ArchetypeModel
                    {
                        Fieldsets = new[]
                        {
                            new ArchetypeFieldsetModel()
                            {
                                Alias = "section",
                                Properties = new[]
                                {
                                    new ArchetypePropertyModel() {Alias = "sectionName", Value = "Hvad er problemet"},
                                }
                            },
                            new ArchetypeFieldsetModel()
                            {
                                Alias = "section",
                                Properties = new[]
                                {
                                    new ArchetypePropertyModel() {Alias = "sectionName", Value = "Hvor foregår det"},
                                }
                            },
                            new ArchetypeFieldsetModel()
                            {
                                Alias = "section",
                                Properties = new[]
                                {
                                    new ArchetypePropertyModel() {Alias = "sectionName", Value = "Hvad gør Amnesty"},
                                }
                            },
                            new ArchetypeFieldsetModel()
                            {
                                Alias = "section",
                                Properties = new[]
                                {
                                    new ArchetypePropertyModel() {Alias = "sectionName", Value = "Hvad kan du gøre"},
                                }
                            },
                        }
                    });
            }
        }
    }
}