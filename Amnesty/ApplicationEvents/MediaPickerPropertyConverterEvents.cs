﻿using Our.Umbraco.PropertyConverters;
using Umbraco.Core;
using Umbraco.Core.PropertyEditors;

namespace DisPlay.Amnesty.ApplicationEvents
{
    public class MediaPickerPropertyConverterEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            PropertyValueConvertersResolver.Current.RemoveType<MediaPickerPropertyConverter>();
            PropertyValueConvertersResolver.Current.RemoveType<MultipleMediaPickerPropertyConverter>();
            PropertyValueConvertersResolver.Current.RemoveType<DisPlay.Umbraco.ValueConverters.MediaPickerPropertyConverter>();
            PropertyValueConvertersResolver.Current.RemoveType<DisPlay.Umbraco.ValueConverters.MultipleMediaPickerPropertyConverter>();
        }
    }
}