﻿/* DIS/PLAY Script
    Author's name: Nicolaj Lund Nielsen
    Modified by: ...
    Client name: Amnesty
    Date of creation: 08-12-2014
*/

DIS = {
    resetTimer: {},
    winWidth: 0,
    winHeight: 0,
    screenSm: 768,
    screenMd: 1024,
    screenLg: 1280,
    $billBoard: {},
    $body: {},
    $logo: {},
    $preview: {}
};

(function($, DIS, window) {
    "use strict";

    DIS.init = function ($preview) {
        DIS.$preview = $preview;
        DIS.winWidth = (DIS.$preview.length) ? window.parent.$("#resultFrame").width() : window.innerWidth; // Umbraco preview iframes site fix
        DIS.winHeight = (DIS.$preview.length) ? window.parent.$("#resultFrame").height() : window.innerHeight;  // Umbraco preview iframes site fix
        DIS.$billBoard = $("#billboard");
        DIS.$body = $("body:first");
        DIS.$logo = $("#logo");

        // Activate javascript modules
        // custom/DIS.scroll.js
        DIS.scroll.init();
        // custom/DIS.menu.js
        DIS.menu.init();
        // custom/DIS.ui.js
        DIS.ui.init();
        // custom/DIS.share.js
        //DIS.forms.init();
        // custom/DIS.fixes.js
        DIS.fixes.init();
        // custom/DIS.infogrph.js
        DIS.infograph.init();
        // custon/DIS.progressbar.js
        DIS.progressbar.init();
        //slick slider
        DIS.slider.init();
        //Init forms
        DIS.forms.init();
    };

}(jQuery, DIS, window));
