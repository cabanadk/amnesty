/*DIS/PLAY Script
    Author's name: Nicolaj Lund Nielsen
    Modified by: ...
    Client name: Amnesty
    Date of creation: 02-03-2015
*/

(function($, DIS, window) {
    "use strict";
    var $w = $w || $(window);

    DIS.menu = {
        height: 80,
        isOpen: false,
        $wrap: $(".menu-wrap"),
        $top: $("#top-menu"),
        $sub: $("#sub-menu"),
        $secMenu: $(".section-menu"),
        $body: {},

        init: function () {

            var t = this,
                resizeTimeout;

            this.stickyTopMenu();

            // Activate menu buttons
            this.burger();

            // Activate page section-menu
            this.sectionMenu();

            // Activate extend menu buttons
            this.$sub.find(".expand").on("click", DIS.menu.extend);
            this.$top.find(".expand").on("click", function (e) {
                if (DIS.winWidth < DIS.screenMd) {
                    DIS.menu.extend.call(this, e);
                }
            });
        }, // DIS.menu.init()
        burger: function () {
            var $menuOverlay = this.$wrap.find(".overlay"),
                $burger = $("#burger"),
                $bunFirst = $burger.find(".bun:first"),
                $bunLast = $burger.find(".bun:last"),
                $subMenu = this.$sub,
                $topMenu = this.$top;

            $burger.on("click", function (e) {
                e.preventDefault();
                if (DIS.menu.isOpen) {
                    DIS.menu.isOpen = false;
                    TweenLite.set(DIS.$body, { className: "-=menu-open" });
                    TweenLite.to($menuOverlay, 0.5, { className: "-=open", display: "none", ease: Power2.easeIn });
                    TweenLite.to($subMenu, 0.5, { xPercent: 100, zIndex: -1, ease: Power2.easeIn, display: "none" });
                    TweenLite.to($burger, 0.5, { className: "-=open", ease: Power3.easeIn });

                    if (DIS.winWidth < DIS.screenMd) {
                        TweenLite.to($topMenu, 0.5, { xPercent: 100, display: "none", ease: Power2.easeIn });
                        TweenLite.to($bunFirst, 0.5, { rotation: 0, top: "0", transformOrigin: "50% 50%", ease: Power3.easeInOut });
                        TweenLite.to($bunLast, 0.5, { rotation: 0, top: "24px", transformOrigin: "50% 50%", ease: Power3.easeInOut });
                    } else {
                        TweenLite.to($(".bg", DIS.$billBoard), 0.5, { x: 0, ease: Power3.easeInOut });
                        TweenLite.to($bunFirst, 0.5, { rotation: 0, backgroundColor: "#fff", top: "0", transformOrigin: "50% 50%", ease: Power3.easeInOut });
                        TweenLite.to($bunLast, 0.5, { rotation: 0, backgroundColor: "#fff", top: "24px", transformOrigin: "50% 50%", ease: Power3.easeInOut });
                    }

                } else {

                    var scrollTop = $w.scrollTop(),
                        menuTop = DIS.winHeight - DIS.menu.height;

                    if (DIS.$billBoard.length && scrollTop < menuTop) {
                        TweenLite.to(window, 0.2, { scrollTo: { y: 0 }, ease: Power4.easeOut });
                    } else if (DIS.$billBoard.length && scrollTop >= menuTop && scrollTop < DIS.winHeight) {
                        TweenLite.to(window, 0.2, {
                            scrollTo: { y: menuTop + 2 }, ease: Power4.easeOut
                        });
                    } else {
                        TweenLite.set(DIS.menu.$wrap.addClass("affix"));
                    }
                    DIS.menu.isOpen = true;
                    TweenLite.set(DIS.$body, { className: "+=menu-open" });
                    TweenLite.to($menuOverlay, 0.6, { className: "+=open", display: "block", ease: Power2.easeInOut });
                    TweenLite.to($burger, 0.6, { className: "+=open", ease: Power3.easeInOut });
                    TweenLite.fromTo($subMenu, 0.6, { xPercent: 100 }, { xPercent: 0, zIndex: 40, display: "block", ease: Power2.easeInOut });

                    if (DIS.winWidth < DIS.screenMd) {
                        TweenLite.to($topMenu, 0.6, { xPercent: 0, display: "block", ease: Power2.easeInOut });

                        TweenLite.to($bunFirst, 0.6, { rotation: -45, top: "12px", transformOrigin: "50% 50%", ease: Power3.easeInOut });
                        TweenLite.to($bunLast, 0.6, { rotation: 45, top: "12px", transformOrigin: "50% 50%", ease: Power3.easeInOut });
                    } else {
                        TweenLite.to($(".bg", DIS.$billBoard), 0.5, { x: -100, ease: Power3.easeInOut });
                        TweenLite.to($bunFirst, 0.6, { rotation: -45, backgroundColor: "#000", top: "12px", transformOrigin: "50% 50%", ease: Power3.easeInOut });
                        TweenLite.to($bunLast, 0.6, { rotation: 45, backgroundColor: "#000", top: "12px", transformOrigin: "50% 50%", ease: Power3.easeInOut });
                    }
                }
            });

            // Make clicking on the overlay close the submenu
            $menuOverlay.on("click", function () {
                $burger.click();
            });

            //Uncommented - See Incident 101 - https://adextranet.dis-play.dk/Amnesty/Lists/Incidentliste/DispForm.aspx?ID=101
            /*if (DIS.$body.hasClass("mediekonkurrence")) {
                // If site is the subsite mediekonkurrence
                // check if it is the first visit and open menu.
                DIS.menu.firstVisitCheck($burger);
            }*/

        },  // DIS.btns.burger()
        extend: function (e) {
            e.preventDefault();

            var $btn = $(this);
            var navLvl = $btn.parent();
            if (navLvl.hasClass("inpath")) {
                TweenLite.to(navLvl, 0.5, { className: "-=inpath", ease: Cubic.easeOut });
                TweenLite.to($btn.find("span"), 0.8, { rotationZ: "0", ease: Cubic.easeOut });
            } else {
                TweenLite.to(navLvl, 0.5, { className: "+=inpath", ease: Cubic.easeOut });
                TweenLite.to($btn.find("span"), 0.8, { rotationZ: "-315deg", transformOrigin: "50% 69%", ease: Cubic.easeOut });
            }
        },
        sectionMenu: function () {
            var menuOff = this.$secMenu.innerHeight() + this.$top.innerHeight();

            this.$secMenu.find("li a").on("click", function (e) {
                e.preventDefault();
                var href = $(this).attr("href"),
                    //$section = $(href),
                    elemOffset = (DIS.winWidth > DIS.screenSm) ? $(href).offset().top - menuOff : $(href).offset().top - 146;

                if (!Modernizr.touch) DIS.$body.removeClass("alpha-go");
                TweenLite.to(window, 1.4, {
                    scrollTo: { y: elemOffset }, ease: Back.easeOut, onComplete: function () {
                        if (!Modernizr.touch) DIS.$body.addClass("alpha-go");
                    }
                });
            });
            // back-button function
            if(!DIS.$body.hasClass("mediekonkurrence")){
                this.$secMenu.find("a.back-btn").on("click", function (e) {
                    if (document.referrer.indexOf(window.location.host) !== -1) {
                        window.history.go(-1);
                        return false;
                    }
                });
            }
        },
        stickyTopMenu: function () {
            // Set menu position out of sight
            // Activate special actions for mobile menues
            TweenLite.set(DIS.menu.$sub, { xPercent: 100, display: "none" });
            if (DIS.winWidth < DIS.screenMd) {
                TweenLite.set(DIS.menu.$top, { xPercent: 100, display: "none" });
            } else {
                DIS.menu.$top.removeAttr("style");
            }
            DIS.menu.height = (DIS.winWidth >= DIS.screenMd) ? 90 : 80;

            // Stick menu to fit screen
            if (DIS.winWidth >= DIS.screenMd && DIS.$billBoard.length) {
                this.$wrap.affix({
                    offset: {
                        top: DIS.winHeight - DIS.menu.height - 2
                    }
                });
                if (DIS.$billBoard.hasClass("breaking-news")) {
                    DIS.$logo.affix({
                        offset: {
                            top: DIS.winHeight - DIS.menu.height - 2
                        }
                    });
                }
                this.$secMenu.affix({
                    offset: {
                        top: DIS.winHeight + DIS.menu.height - 20
                    }
                });
                if (DIS.$preview.length) {
                    DIS.menu.$top.removeAttr("style");
                }
            } else {
                DIS.menu.$wrap.removeAttr("style");
                /*
                if (DIS.$billBoard.length && DIS.$preview.length) {
                    DIS.menu.$wrap.removeAttr("style");
                }
                */
            }
        },
        showHideMenu: function () {
            if (DIS.scroll.controller.info("scrollDirection") === "FORWARD" && DIS.scroll.controller.info("scrollPos") !== 0) {
                DIS.$body.addClass("hide-menu");
            } else {
                DIS.$body.removeClass("hide-menu");
            }
            if (DIS.$billBoard.hasClass("breaking-news")) {
                if (DIS.scroll.controller.info("scrollPos") !== 0) {
                    DIS.$logo.addClass("yellow-bg");
                } else {
                    DIS.$logo.removeClass("yellow-bg");
                }
            }
        },
        firstVisitCheck: function($menuBtn) {
            var firstVisit = window.localStorage.getItem("firstVisit") || true;
            if(firstVisit === true) {
                $menuBtn.click();
                setTimeout(function(){
                    $menuBtn.click();
                },3000);
                window.localStorage.setItem("firstVisit", false);
            }
        }
    }; // DIS.menu

}(jQuery, DIS, window));
