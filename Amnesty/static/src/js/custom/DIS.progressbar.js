(function ($, DIS, window) {
    "use strict";

    DIS.progressbar = {
        $bars: $(".signature-progress"),

        init: function () {
            this.$bars.each(function () {
                DIS.progressbar.setupProgressBar($(this));
            });
        },
        setupProgressBar: function ($progressFrame) {
            var signatures = $progressFrame.data("signatures"),
                goal = $progressFrame.data("goal"),
                signatureCount = { count: 0 },
                //missingCount = { count: 0},
                //missing = goal - signatures,
                $signatureCon = $progressFrame.find(".signature-count b"),
                $goalCon = $progressFrame.find(".signature-goal"),
                $missingCon = $goalCon.find(".missing"),
                $progressBar = $progressFrame.find(".progress-bar"),
                $totalbar = $progressFrame.find(".graphic-frame"),
                totalBarPx = $totalbar.width(),
                signetBarPx = (signatures/goal) * totalBarPx,
                tl = new TimelineLite({delay:2});

            tl.to(signatureCount, 2, {
                count: signatures,
                onUpdate: function () {
                    $signatureCon[0].innerHTML = Math.ceil(signatureCount.count);
                },
                ease: Sine.easeOut
            })
            .to($progressBar, 2.5, {
                width: signetBarPx < totalBarPx ? signetBarPx : totalBarPx,
                ease: Circ.easeOut
            }, "-=2")
            .to($goalCon, 0.6, {className: "+=shown", ease: Circ.easeOut}, "-=0.3")
            .to([$totalbar, $missingCon], 2, {className: "+=dark", ease: Expo.easeIn})
            .fromTo(signatureCount, 2, {count: 0}, {
                count: signatures,
                onUpdate: function () {
                        $missingCon[0].innerHTML = goal - Math.ceil(signatureCount.count);
                },
                ease: Sine.easeOut
            }, "-=2");
            tl.play();
        }
    }
}(jQuery, DIS, window));