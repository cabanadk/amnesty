﻿/* DIS/PLAY Script
    Author"s name: Nicolaj Lund Nielsen
    Modified by: ...
    Client name: Amnesty
    Date of creation: 02-03-2015
*/

(function ($, DIS, window) {
    "use strict";
    var $w = $w || $(window);

    DIS.ui = {
        init: function () {
            // Activate Search
            this.search.init();
            // Googlemaps setup
            this.maps.init();

            var $countEl = $(".counter span"),
                counterEndpont = "/umbraco/api/SamletAntalUnderskrifter/get"; //Call with parameter: ?formguid=91da5518-463a-4bb6-93cd-4a1883984357

            $($countEl).each(function () {
                DIS.ui.countNumber($countEl, counterEndpont);
            });

            if (DIS.winWidth >= DIS.screenMd) {
                //this.react();
                if (Modernizr.svg) {
                    this.logoHover();
                }
            }

            var $ctaBtns = $(".btn-cta");

            if ($ctaBtns.length) {
                $ctaBtns.each(function() {
                    DIS.ui.ctaHover($(this));
                });
            }

            var $pressNewsModule = $(".press-and-news:not(.news-only)");
            if ($pressNewsModule.length) {
                DIS.ui.tabsSetup($pressNewsModule);
            }
            if (DIS.$billBoard.length) {
                DIS.ui.loadBgImage($(".bg", DIS.$billboard));
            }

            if (Modernizr.touch) {
                FastClick.attach(document.body);
            }

            if (DIS.winWidth >= 480) {
                $(".topic-wrap .category").on("click", function () {
                    window.location.href = $(this).next().attr("href");
                });
            }

            if (DIS.winWidth <= 768) {
                var $credits = $('.photo-credits .description');

                if ($credits.length && Modernizr.touch) {
                    DIS.ui.creditsShowHide($credits);
                }
            }

            this.accordion();

            if (!DIS.$preview.length) {
                $w.load(function () {
                    $(".image-src-wrap").each(function () {
                        DIS.ui.loadImage($(this));
                    });
                    $(".ribbon .bg").each(function () {
                        DIS.ui.loadBgImage($(this));
                    });
                    DIS.ui.loadModules();
                });
            } else {
                $(".image-src-wrap").each(function () {
                    DIS.ui.loadImage($(this));
                });
                $(".ribbon .bg").each(function () {
                    DIS.ui.loadBgImage($(this));
                });
                DIS.ui.loadModules();
            }
        },
        loadModules: function () {

            // custom/DIS.share.js
            DIS.share.init();
        },
        maps: {
            mapArray: [],
            $lazyMapsInstance: {},
            init: function () {

                DIS.ui.maps.$lazyMapsInstance = $(".google-maps").lazyLoadGoogleMaps({
                    /*callback: function (container, map) {
                        var $container = $(container),
                            coords = $container.attr("data-coords").split(","),
                            center = new google.maps.LatLng(coords[0], coords[1]);

                        $.data(map, "center", center);
                        DIS.ui.maps.mapArray.push(map);
                        // Amnesty Googlemaps style:
                        // ------------------------- //
                        // yellow: #fbf038 / brand grey: #565e65
                        var style = [{ "stylers": [{ "visibility": "on" }, { "saturation": -100 }, { "gamma": 1 }] }, { "featureType": "road", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "stylers": [{ "color": "#565e65" }] }, { "featureType": "poi", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "labels.text", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "road.local", "elementType": "labels.text", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "gamma": 0.48 }] }, { "featureType": "transit.station", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "geometry.stroke", "stylers": [{ "gamma": 7.18 }] }];


                        // ------------------------- //
                        // Amnesty Googlmaps style end
                        var zoomLvl = parseInt($container.data("zoomlevel"));
                        if (zoomLvl > 20 || zoomLvl < 3) {
                            var zoomLvl = 7;
                        }
                        map.setOptions({ zoom: zoomLvl, center: center, draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true, disableDefaultUI: true, styles: style });
                        // icon: iconBase + "amnesty-marker.png"
                        var markerpath = "/static/dist/img/svg_amnesty-marker.png";
                        new google.maps.Marker({
                            position: center,
                            map: map,
                            icon: markerpath,
                            title: $container.data("pintext")
                        });
                    }*/

                    callback: function (container, map) {
                        var $container = $(container),
                            markerpath = "/static/dist/img/svg_amnesty-marker.png",
                            style = [{ "stylers": [{ "visibility": "on" }, { "saturation": -100 }, { "gamma": 1 }] }, { "featureType": "road", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "stylers": [{ "color": "#565e65" }] }, { "featureType": "poi", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "labels.text", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "road.local", "elementType": "labels.text", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "gamma": 0.48 }] }, { "featureType": "transit.station", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "geometry.stroke", "stylers": [{ "gamma": 7.18 }] }];

                        DIS.ui.maps.mapArray.push(map);

                        var zoomLvl = parseInt($container.data("zoomlevel"));
                        if(zoomLvl === "") {
                            zoomLvl = 7;
                        }
                        /*if (zoomLvl > 20 || zoomLvl < 3) {
                            zoomLvl = 7;
                        }*/

                        /*
                        * There are two ways to insert map via umbraco.
                        * Map with single coordinate and maps with multiple coordinates
                        */

                        if ($container.data('json')){
                            var bounds = new google.maps.LatLngBounds(),
                                infoWindow = new google.maps.InfoWindow(),
                                json = $container.data('json');


                            map.setOptions({
                                draggable: true,
                                zoomControl: true,
                                scrollwheel: false,
                                disableDoubleClickZoom: true,
                                disableDefaultUI: true,
                                styles: style,
                                zoom: zoomLvl
                            });
                            google.maps.event.addListener(map, "click", function(event){
                                map.setOptions({scrollwheel:true});
                            });


                            $.each(json, function(key, data) {
                                var data = json[key];
                                var myLatlng = new google.maps.LatLng(data.latitude, data.longitude);
                                bounds.extend(myLatlng);
                                var marker = new google.maps.Marker({
                                    position: myLatlng,
                                    map: map,
                                    icon: markerpath,
                                    title: data.title
                                });
                                (function (marker, data) {
                                    google.maps.event.addListener(marker, "click", function (e) {
                                        var infoWindowContent = "";
                                        if(data.title){
                                            infoWindowContent = "<h3>" + data.title + "</h3>";
                                        }
                                        if(data.description){
                                            infoWindowContent += "<p>" + data.description + "</p>";
                                        }
                                        if(data.linkUrl && data.linkText){
                                            infoWindowContent += "<a href='" + data.linkUrl + "' title='" + data.linkText + "'>" + data.linkText + "</a>";
                                        }

                                        infoWindow.setContent("<div class='info_content'>" + infoWindowContent + "</div>");
                                        infoWindow.open(map, marker);
                                    });
                                })(marker, data);

                                //make map fit all markers
                                map.fitBounds(bounds);
                            });

                            $.data(map, "center", bounds.getCenter());

                            //(optional) restore the zoom level after the map is done scaling
                            var listener = google.maps.event.addListener(map, "idle", function () {
                                map.setZoom(zoomLvl);
                                google.maps.event.removeListener(listener);
                            });

                        } else {
                            var coords = $container.attr("data-coords").split(","),
                                center = new google.maps.LatLng(coords[0], coords[1]);

                            $.data(map, "center", center);

                            map.setOptions({ zoom: zoomLvl, center: center, draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true, disableDefaultUI: true, styles: style });

                            new google.maps.Marker({
                                position: center,
                                map: map,
                                icon: markerpath,
                                title: $container.data("pintext")
                            });
                        }
                    }
                });

                $w.on("resize", DIS.ui.maps.$lazyMapsInstance.debounce(200, function () {
                    $.each(DIS.ui.maps.mapArray, function () {
                        this.setCenter($.data(this, "center"));
                    });
                }));
            }
        },
        search: {
            $global: $("#search-global"),
            init: function () {
                var $btn = this.$global.find(".btn-search"),
                $navWrap = this.$global.parent(),
                $field = $navWrap.find(".global-search"),
                $form = $navWrap.find("form");

                $btn.on("click", function (e) {
                    if ($navWrap.hasClass("focused") || DIS.winWidth >= DIS.screenLg ) {
                        if (!$field.val()) {
                            $field.blur();
                            TweenLite.to($navWrap, 0.6, { className: "-=focused", ease: Power3.easeOut });
                            return false;
                        }
                    } else {
                        TweenLite.to($navWrap, 0.6, { className: "+=focused", ease: Power3.easeOut });
                        $field.focus();
                        return false;
                    }
                });

                if (DIS.winWidth >= DIS.screenLg) {
                    $field.on("focus", function (e) {
                        TweenLite.to($navWrap, 0.6, { className: "+=focused", ease: Power3.easeOut });
                    });

                    $field.on("blur", function (e) {
                        TweenLite.to($navWrap, 0.6, { className: "-=focused", ease: Power3.easeOut });
                    });
                }
            }
        },
        countNumber: function(el, endpoint) {

            var counter = {
                var: 0, //Starting number
                formId: $(".contour").find("input[name=FormId]").val() //Find formid
            };

            $.ajax({
                url: endpoint+"?formguid="+counter.formId,
                type: 'get'
            })
            .done(function(data) {
                //When data is fetched, start counting
                TweenLite.to(counter, 2, {
                    var: data,
                    onUpdate: function() {
                        $(el).html(Math.ceil(counter.var));
                    },
                    ease: Power3.easeInOut
                });
            })
            .fail(function(error) {
                console.log("error:" + error);
            });


        },
        accordion: function() {
            $(".accordion__item h2").on("click", function() {

                var self = $(this);
                self.toggleClass("open");
                self.parent().find(".accordion__item--text").toggleClass("open");

            });
        },
        logoHover: function () {
            var $logoParts = $("svg > g", DIS.$logo);
            //logoBg = DIS.$logo.css("backgroundColor"),

            DIS.$logo.hover(function () {
                //TweenLite.to(DIS.$logo, 0.4, { background: "#000", ease: Power3.easeInOut });
                TweenLite.to($logoParts, 0.4, { fill: "#fff", ease: Power3.easeInOut });
            }, function () {
                //TweenLite.to(DIS.$logo, 0.4, { background: logoBg, ease: Power4.easeIn });
                TweenLite.to($logoParts, 0.3, { fill: "#000", ease: Power4.easeIn });
            });
        },
        tabsSetup: function ($module) {
            var $tabBtns = $module.find(".tab-controls .btn"),
                $tabWrap = $module.find(".tabs-wrapper")

            if (DIS.ui.getUrlParams("type") === "Consulation") {
                TweenLite.set($tabWrap, { xPercent: -48.5, marginLeft: "-35px", ease: Power2.easeInOut });
                TweenLite.set($tabBtns[0], { borderBottom: "2px solid #565e65", ease: Power3.easeInOut });
                TweenLite.set($tabBtns[1], { borderBottom: "4px solid #000", ease: Power2.easeInOut });
            } else {
                TweenLite.to($tabBtns[1], 0.4, { borderBottom: "2px solid #565e65", ease: Power2.easeInOut });
                TweenLite.to($tabBtns[0], 0.4, { borderBottom: "4px solid #000", ease: Power3.easeInOut });
            }

            $tabBtns.on("click", function () {
                var $btn = $(this),
                    parentIndex = $btn.parent().index();

                TweenLite.to($tabBtns, 0.4, { borderBottom: "2px solid #565e65", ease: Power2.easeInOut });
                TweenLite.to($btn, 0.4, { borderBottom: "4px solid #000", ease: Power2.easeInOut });

                if (parentIndex) {
                    TweenLite.to($tabWrap, 0.4, { xPercent: -48.5, marginLeft: "-35px", ease: Power2.easeInOut });
                } else {
                    TweenLite.to($tabWrap, 0.4, { xPercent: 0, marginLeft: "0", ease: Power2.easeInOut });
                }
            })
        },
        tweenHover: function ($elem, timeline) {
            $elem.hover(function () {
                timeline.play();
            }, function () {
                timeline.reverse();
            });
        },
        ctaHover: function ($btn) {
            var $leftLine = $btn.find(".line-left"),
                $rightLine = $btn.find(".line-right"),
                $topLeft = $btn.find(".line-topleft"),
                $topRight = $btn.find(".line-topright"),
                tl = new TimelineLite();

            tl.fromTo($leftLine, 0.3, { scaleY: "0" }, { scaleY: "1", transformOrigin: "bottom center", ease: Power2.easeOut });
            tl.fromTo($rightLine, 0.3, { scaleY: "0" }, { scaleY: "1", transformOrigin: "bottom center", ease: Power2.easeOut }, "0");
            tl.fromTo($topLeft, 0.3, { scaleX: "0", transformOrigin: "left center" }, { scaleX: "1", ease: Expo.easeOut }, "0.25");
            tl.fromTo($topRight, 0.3, { scaleX: "0", transformOrigin: "right center" }, { scaleX: "1", ease: Expo.easeOut }, "0.25");
            tl.stop();

            DIS.ui.tweenHover($btn, tl);
        },
        creditsShowHide: function($credits){

            $credits.each(function(){
                var $credit = $(this);
                console.log($credit);
                $credit.parent().parent().on('click', function (e) {
                    if ($credit.hasClass('show-credits')) {
                        TweenLite.to($credit, .3, { className: "-=show-credits", ease: Circ.easeOut });
                    } else {
                        TweenLite.to($credit, .3, { className: "+=show-credits", ease: Circ.easeOut });
                    }
                });
            });
        },
        getImgSrc: function ($el) {
            var src = "";
            if (DIS.winWidth >= DIS.screenSm && DIS.winWidth < DIS.screenLg) {
                src = $el.data("src-sm");
            } else if (DIS.winWidth >= DIS.screenLg) {
                src = $el.data("src-lg");
            } else {
                src = $el.data("src-xs");
            }
            return src;
        },
        loadImage: function ($el) {
            var img = $("<img />"),
            src = DIS.ui.getImgSrc($el);

            img.load(function () {
                $el.append(img);
                $el.addClass("loaded");
            });
            img.attr("alt", $el.attr("data-alt"));
            img.attr("src", src);
        },
        loadBgImage: function ($el) {
            var img = $("<img />"),
                src = DIS.ui.getImgSrc($el);

            img.load(function () {
                $el.attr("style", "background-image: url(" + src + ");");
                $el.addClass("loaded");
            });

            img.attr("src", src);
        },
        getUrlParams: function (name) {
            var results = new RegExp("[\\?&]" + name + "=([^&#]*)").exec(window.location.href);
            if (results) {
                return results[1];
            } else { return 0 };
        },
        // This function returns a url stripped from parameters
        stripUrl: function (url) {
            return url.split("?")[0].split("#")[0];
        },
        // This function returns the root url
        rootUrl: function (url) {
            return url.split("/")[2];
        },
        exGetUrlParams: function (name) {
            var results = new RegExp("[\\?&]" + name + "=([^&#]*)").exec(fakeUrl);
            if (results) {
                return results[1];
            } else { return 0 };
        }
    }; // DIS.ui

}(jQuery, DIS, window));
