﻿/* DIS/PLAY Script
 Author's name: Nicolaj Lund Nielsen
 Modified by: ...
 Client name: Amnesty
 Date of creation: 08-12-2014
 */
(function($, DIS, window) {
    "use strict";
    var $w = $w || $(window);

    DIS.forms = {
        offsetLeft: 0,
        $currForm: {},
        $currFieldSet: {},
        $stickyFormContainer: {},
        init: function() {

            // Using labels as placeholder text, hide label when text-field is filled
            $("footer .textfield input").each(DIS.forms.showHideField).on("focusout", DIS.forms.showHideField);

            $(".otherAmount .btn").on("click", this.showCustomField);

            $("#donationForm input[type='radio'], #dibsForm input[type='radio'], #memberForm input[type='radio']").on("change", this.showCustomField);

            var $forms = $(".contour form[action]"),
                $jumptoarticle,
                $offsetTopArticle;


            if ($("a#jumptoarticle").length) {
                $jumptoarticle = $("a#jumptoarticle").data("target");
                $offsetTopArticle = $($jumptoarticle).offset().top;

                $("a#jumptoarticle").click(function(e) {
                    e.preventDefault();
                    TweenLite.to(window, 0.4, { scrollTo: { y: $offsetTopArticle - 100 }, ease: Circ.easeOut });
                });
            }

            if ($forms.length) {

                $("#dibsForm, #memberForm, #contour_form_Sendetbrev, #landsstaevneForm").each(function() {
                    var $form = $(this);
                    // Setup DIBS
                    if ($form.is("#dibsForm") || $form.is("#memberForm")) {
                        DIS.forms.dibs.setup($form);
                    }
                    if ($form.is("#landsstaevneForm")) {
                        DIS.forms.landsstaevne.setup($form);
                        return;
                    }
                    // Setup multi-step form
                    DIS.forms.setupFormSteps($form);
                });

                $("#donationForm").each(function() {
                    var $form = $(this);

                    DIS.forms.dibs.setup($form);

                    $form.find(".btn-next").click(function() {
                        var fieldset = $form.find(".contourFieldSet");
                        var step = fieldset.find(".span4.active-step");
                        var nextStep = step.next();
                        var fields = step.find("input[data-val='true'], textarea[data-val='true']");

                        fieldset.css("max-height", step.height()).hide().show(0);

                        if (DIS.forms.validateFields(fields) || fields.length === 0) {
                            if (!step.is(":last-child")) {
                                step.removeClass("active-step").prop("disabled", true);
                                nextStep.addClass("active-step").prop("disabled", false);
                            }

                            $form.attr("data-active-step", nextStep.index() + 1);
                            fieldset.addClass("animating").css("max-height", nextStep.height()).one("transitionend", function() {
                                fieldset.removeClass("animating");
                                nextStep.find("input:visible:first").focus();
                            });

                            DIS.forms.scrollTo(fieldset);
                        }
                    });

                    $form.find(".btn-prev").click(function() {
                        var fieldset = $form.find(".contourFieldSet");
                        var step = fieldset.find(".span4.active-step");
                        var prevStep = step.prev();

                        fieldset.css("max-height", step.height()).hide().show(0);

                        if (!step.is(":first-child")) {
                            step.removeClass("active-step").prop("disabled", true);
                            prevStep.addClass("active-step").prop("disabled", false);
                        }

                        fieldset.css("max-height", prevStep.height()).one("transitionend", function() {
                            prevStep.find("input:visible:first").focus();
                        });
                        $form.attr("data-active-step", prevStep.index() + 1);

                        DIS.forms.scrollTo(fieldset);
                    });

                    $form.find(".btn-submit-member").click(function() {
                        var fieldset = $form.find(".contourFieldSet");
                        var step = fieldset.find(".span4.active-step");
                        var fields = step.find("input[data-val='true'], textarea[data-val='true']");

                        if (DIS.forms.validateFields(fields) || fields.length === 0) {
                            DIS.forms.dibs.saveUserInfo($form);
                            DIS.forms.blivMedlem.sendMail($form);
                            $form.find(".btn-submit-member").addClass("sending");
                        }
                    });

                    $(window).resize(function() {
                        var fieldset = $form.find(".contourFieldSet");
                        fieldset.css("max-height", "auto").hide().show(0);
                        fieldset.css("max-height", fieldset.find(".span4.active-step").height());
                    });
                });

                $("#donationForm #repeatedPayment").change(function() {
                    var repeatedPayment = $(this),
                        form = repeatedPayment.closest("form"),
                        radiobuttonlist = $(".radiobuttonlist");

                    form.toggleClass("repeated-payment", repeatedPayment.prop("checked"));
                    form.find(".cpr input").attr("data-val", repeatedPayment.prop("checked"));

                    if (form.hasClass("repeated-payment")) {
                        radiobuttonlist.find("input#donate100").val("12500");
                        radiobuttonlist.find("input#donate250").val("17500");
                        radiobuttonlist.find("input#donate750").val("35000");
                        radiobuttonlist.find("label.first").text("125 kr");
                        radiobuttonlist.find("label.second").text("175 kr"); //Trigger click for reset amount
                        $("#dibsAmount").val("17500");
                        radiobuttonlist.find("label.third").text("350 kr");
                    } else {
                        radiobuttonlist.find("input#donate100").val("12500");
                        radiobuttonlist.find("input#donate250").val("25000");
                        radiobuttonlist.find("input#donate750").val("75000");

                        radiobuttonlist.find("label.first").text("125 kr");
                        radiobuttonlist.find("label.second").text("250 kr"); //Trigger click for reset amount
                        $("#dibsAmount").val("25000");
                        radiobuttonlist.find("label.third").text("750 kr");
                    }
                }).change();

                $("#donationForm .cpr input").blur(function() {
                    var input = $(this),
                        form = input.closest("form");

                    if (!form.hasClass("repeated-payment")) {
                        // Validate this input if it has a value
                        if (input.val().trim()) {
                            input.attr("data-val", true);
                        } else {
                            input.attr("data-val", false);
                        }
                    }
                });

                $("#donationForm .mobiltelefon input").blur(function() {
                    var input = $(this);

                    // Validate this input if it has a value
                    if (input.val().trim()) {
                        input.attr("data-val", true);
                    } else {
                        input.attr("data-val", false);
                    }
                });

                var progressBar = $("#donationForm .formfields-progress__bar");
                if (progressBar.length) {
                    var form = progressBar.closest("form"),
                        customAmount = form.find("#customAmount");

                    function setProgress() {
                        var dibsInputsCount = 4,
                            isRepeatedPayment = form.hasClass("repeated-payment"),
                            inputsForValidation = (isRepeatedPayment ? form.find("input[data-val='true']") : form.find(".span4:first-child input[data-val='true']")),
                            inputsForValidationCount = inputsForValidation.length + (isRepeatedPayment ? 0 : dibsInputsCount),
                            inputsForValidationWithValue = inputsForValidation.filter(function() {
                                return $(this).val();
                            }),
                            inputsForValidationWithValueCount = inputsForValidationWithValue.length;

                        if (customAmount.attr("data-val") !== "true") {
                            inputsForValidationCount += 1;
                            inputsForValidationWithValueCount += form.find(".radiobuttonlist input:checked").length;
                        }

                        progressBar.width((inputsForValidationWithValueCount / inputsForValidationCount * 100) + "%");
                    }

                    form.find("input").on("change", function() {
                        setTimeout(setProgress, 100);
                    });

                    form.find(".radiobuttonlist label").on("click", function() {
                        setTimeout(setProgress, 100);
                    });

                    form.find(".otherAmount .btn").click(function() {
                        setTimeout(setProgress, 100);
                    });

                    setTimeout(setProgress, 100);
                }

                // Handle toggler (stylized checkbox - on/off
                var togglerHandles = $(".toggler__switch-handle");
                if (togglerHandles.length) {
                    var togglerTriggers = $(".toggler__text span"),
                        togglerActiveRail,
                        togglerActiveHandle,
                        togglerMouseDownTime;

                    // Click on triggers (text labels on each side)
                    togglerTriggers.click(function() {
                        var clicked = $(this).parent(),
                            input = clicked.closest(".toggler").find("input");
                        if (clicked.is(":first-child")) {
                            input.prop("checked", false).change();
                        } else {
                            input.prop("checked", true).change();
                        }
                    });

                    // Start moving handle with mouse or touch
                    togglerHandles.on("mousedown touchstart", function() {
                        var body = $("body");

                        // Set listeners
                        body.on("mousemove touchmove", togglerMouseMove);
                        body.on("mouseup touchend", togglerMouseUp);

                        // Remember that this is the active toggler at the moment
                        togglerActiveHandle = $(this);
                        togglerActiveRail = togglerActiveHandle.parent();

                        // Deactivate transition animation
                        togglerActiveHandle.addClass("no-transition");

                        // Remember time to check later (in togglerMouseUp())
                        // if touch is just a click and not touchmove/mousemove
                        togglerMouseDownTime = new Date().getTime();
                    });

                    // Move function
                    function togglerMouseMove(e) {
                        var xPos = e.clientX || e.originalEvent.touches[0].pageX,
                            railWidth = Math.floor(togglerActiveRail.outerWidth()),
                            newHandlePosition = Math.min(railWidth, Math.max(0, Math.floor(xPos - (togglerActiveRail.offset().left + (togglerActiveHandle.width() / 2)))));

                        // Set new position for handle
                        togglerActiveHandle.css("left", newHandlePosition);
                    }

                    // Stop move function
                    function togglerMouseUp() {
                        var body = $("body"),
                            wrapper = togglerActiveHandle.closest(".toggler"),
                            input = wrapper.find("input"),
                            railWidth = togglerActiveRail.width(),
                            railMiddle = railWidth / 2,
                            time = new Date().getTime();

                        // Remove listeners to save memory
                        body.off("mousemove touchmove", togglerMouseMove);
                        body.off("mouseup touchend", togglerMouseUp);

                        // Reactivate transition animation
                        togglerActiveHandle.removeClass("no-transition");

                        // Detect whether the touch was a click (trigger toggle)
                        // or if there was movement, then pick the closes option
                        if (time - togglerMouseDownTime < 200) {
                            wrapper.find("label").click();
                        } else {
                            if (parseInt(togglerActiveHandle.css("left")) < railMiddle) {
                                input.prop("checked", false).change();
                            } else {
                                input.prop("checked", true).change();
                            }
                        }

                        // Remove injected styling for positioning of handle
                        togglerActiveHandle.css("left", "");

                        // Deactivate stuff
                        togglerActiveRail = null;
                        togglerActiveHandle = null;
                    }
                }

                //If there's a validation error, focus on input
                if ($(".field-validation-error").length) {
                    var $offsetTopError = $(".field-validation-error").offset().top;
                    $(".field-validation-error").parent().find("input").focus();
                    TweenLite.to(window, 0.4, { scrollTo: { y: $offsetTopError - 100 }, ease: Circ.easeOut });
                }

                //on this specific page check if query contains params to prefill form fields
                if ($('.contour').length && window.location.hash) {
                    DIS.forms.prefillInputsfromQueryString();
                }

                //The labels should act as "placeholders" in field - So when se have focus in the input fields, we need to hide the labels.
                var contour = $(".contour.donateSinglePage, .contour.skrivunder"),
                    contourinput = contour.find("input[type=text], input[type=email], input[type=tel], input[type=number], textarea");

                contourinput.focus(function() {
                    $(this).one("keydown", function() {
                        $(this).removeClass("error");
                        $(this).next().empty();
                        $(this).parent().parent().find("label").hide();
                    });
                }).blur(function() {
                    var field = $(this),
                        label = field.parent().parent().find("label");

                    //only show label again if field has no value
                    if (field.val() !== "") {
                        label.hide();
                    } else {
                        label.show();
                    }
                });

                contourinput.trigger("blur");

                /*  If Skriv Under Form, we need to do this - See incident #217 in backlog - Could use a small refactor for optimization ;)  */
                $(".contour.skrivunder").each(function() {
                    var inputs = $(this).find('input[type=text], input[type=email]');
                    inputs.each(function() { //Should be able to do: $skrivunderinput.each(), but somehow that wont work - Look at this later on....
                        if ($(this).val() !== "") {
                            $(this).parent().parent().find("label").hide();
                        }
                    });

                });

                //Lifeline can be a skriv under formular as above, if this is the case, active this functionality
                var $lifelineForm = $(".contour.lifeline");
                if ($lifelineForm.length) {
                    DIS.forms.activateLifeLine($lifelineForm);
                }

                // Fix to make email-fields to type="email" (Umbraco makes all textfields = type text)
                var possibleEmailInputWannabees = $forms.find("input[type='text']:not([data-regex])");
                if (possibleEmailInputWannabees.length) {
                    DIS.forms.setTypeEmail($forms, possibleEmailInputWannabees);
                }

                $forms.on("submit", DIS.forms.validateForm);

                $(".btn-submit, .btn-mobilepay").on("click", function() {
                    $(this).addClass("sending").prop('disabled', true);
                    DIS.forms.$currForm = $(this).closest("form[action]");

                    if ($(this).hasClass("btn-submit") && DIS.forms.$currForm.data("default-action")) {
                        // Remove paytype for Mobile Pay (MPO_Nets)
                        DIS.forms.$currForm.find('input[name=paytype][value=MPO_Nets]').remove();

                        // Set default action on form
                        DIS.forms.$currForm.attr("action", DIS.forms.$currForm.data("default-action"));
                    } else if ($(this).hasClass("btn-mobilepay") && DIS.forms.$currForm.data("mobilepay-action")) {
                        // Add paytype set to Mobile Pay (MPO_Nets)
                        DIS.forms.$currForm.prepend('<input type="hidden" name="paytype" value="MPO_Nets">');

                        // Set MobilePay action on form
                        DIS.forms.$currForm.attr("action", DIS.forms.$currForm.data("mobilepay-action"));
                    }

                    var $fields;
                    if (DIS.forms.$currForm.is("#donationForm") && !DIS.forms.$currForm.hasClass("repeated-payment")) {
                        $fields = DIS.forms.$currForm.find(".span4:first-child").find("input[data-val='true'], textarea[data-val='true'], select[data-val='true']");
                    } else {
                        $fields = DIS.forms.$currForm.find("input[data-val='true'], textarea[data-val='true'], select[data-val='true']");
                    }

                    if (DIS.forms.validateFields($fields)) {
                        if (DIS.forms.$currForm.attr("id") == "dibsForm" || DIS.forms.$currForm.attr("id") == "donationForm") {
                            DIS.forms.dibs.saveData(DIS.forms.$currForm, (DIS.forms.$currForm.attr("id") == "donationForm"));
                        } else if (DIS.forms.$currForm.attr("id") == "memberForm") {
                            DIS.forms.blivMedlem.sendMail(DIS.forms.$currForm);
                        } else if (DIS.forms.$currForm.attr("id") == "cprForm") {
                            DIS.forms.cprFrom.sendMail(DIS.forms.$currForm);
                        } else if (DIS.forms.$currForm.attr("id") == "landsstaevneForm") {
                            DIS.forms.landsstaevne.saveData(DIS.forms.$currForm);
                        }
                    } else {
                        $(this).removeClass("sending").prop('disabled', false); //
                    }
                });
                if (Modernizr.touch) {
                    $(".help-block").on("click", function() {
                        $(this).toggleClass("mobile-click");
                    });
                }
            }
            DIS.forms.$stickyFormContainer = $(".sticky-form-section .category");
            if (DIS.forms.$stickyFormContainer.length) {
                DIS.forms.activateStickyForm();
            }


        },
        activateLifeLine: function($form) {
            var $mobileField = $form.find(".mobilnummer"),
                $mobileInput = $mobileField.find("input"),
                $lifelineCheck = $form.find(".checkbox input").eq(1);

            $mobileField.find("> label").append("<span class='contourIndicator'>*</span>")
            $mobileInput.attr("data-val-required", "Indtast mobilnummer");

            DIS.forms.changeCheckedState($lifelineCheck, $mobileInput, $mobileField);
            $lifelineCheck.on("change", function() {
                DIS.forms.changeCheckedState($lifelineCheck, $mobileInput, $mobileField);
            });
        },
        activateStickyForm: function() {
            if (DIS.winWidth > DIS.screenSm) {

                $(window).scroll(function() {
                    setTimeout(function() {
                        if ($(window).scrollTop() > 200) {
                            DIS.forms.$stickyFormContainer.find(".index-wrap").addClass("fixed-form addmargintop");
                        } else {
                            DIS.forms.$stickyFormContainer.find(".index-wrap").removeClass("fixed-form addmargintop");
                        }
                    }, 100);

                });

                /*var formHeight = DIS.forms.$stickyFormContainer.height();
                 DIS.forms.$stickyFormContainer.next().find(".module-wrap").css("min-height", formHeight);

                 DIS.forms.$stickyFormContainer.find(".index-wrap").affix({
                 offset: {
                 top: DIS.menu.height + 57
                 }
                 });
                 if(DIS.winWidth < DIS.screenLg) {
                 var $fields = DIS.forms.$stickyFormContainer.find("input"),
                 $formWrap = DIS.forms.$stickyFormContainer.find(".index-wrap"),
                 $closeBtn = $formWrap.find(".open-close");

                 $fields.on("focus", function(){
                 TweenLite.to($formWrap, 0.3, {className:"+=shown"});
                 });
                 $formWrap.on("click", "h4", function(){
                 TweenLite.to($formWrap, 0.3, {className:"+=shown"});
                 });
                 $formWrap.on("affix.bs.affix", function(){
                 TweenLite.to($formWrap, 0.3, {className:"-=shown"});
                 });
                 $closeBtn.on("click", function(){
                 TweenLite.to($formWrap, 0.3, {className:"-=shown"});
                 });

                 }*/
            } else {
                var $formCon = DIS.forms.$stickyFormContainer,
                    $scrollToBtn = $("button.open-close");
                //$scrollToBtn = $formCon.find(".open-close");

                $scrollToBtn.affix({
                    offset: {
                        top: 200
                    }
                }).on("click", function() {
                    TweenLite.to(window, 0.4, { scrollTo: { y: 0 }, ease: Circ.easeOut });
                    TweenLite.to($formCon, 0.3, { delay: 0.4, className: "+=shown" });
                });

                $formCon.find("h4:first").on("click", function() {
                    TweenLite.to($formCon, 0.3, { delay: 0.4, className: "+=shown" });
                });

                TweenLite.to($formCon, 0.6, { className: "+=activated", ease: Circ.easeOut });
            }
        },
        changeCheckedState: function($checkbox, $input, $field) {
            if (!$checkbox.prop("checked")) {
                $field.addClass("no-val");
                $input.attr("data-val", false);
            } else {
                $field.removeClass("no-val");
                $input.attr("data-val", true);
            }
        },
        showHideField: function() {
            var $field = $(this);
            if ($field.val() !== "" && !$field.hasClass("value")) {
                $field.addClass("value");
            } else {
                $field.removeClass("value");
            }
        },
        showCustomField: function(e) {
            e.preventDefault();
            var $fieldParent = $(this).parent(),
                $radioBtns = $fieldParent.prev().find("input[type='radio']"),
                $contourFieldSet = $fieldParent.parents('.contourFieldSet');

            //we need to remove the max-height property to prevent "videre"-button to be overlapping other form content
            $contourFieldSet.css('max-height','');

            if ($fieldParent.hasClass("otherAmount")) {
                var $customField = $fieldParent.next().find("input");
                $radioBtns.prop("checked", false);
                $fieldParent.removeClass("hide-next");
                $customField.attr("data-val", "true");
                $customField.focus();
            } else {
                var $otherAmount = $(".otherAmount"),
                    $customField = $otherAmount.next().find("input");
                $otherAmount.addClass("hide-next");
                $customField.attr("data-val", "false");
            }
        },
        setTypeEmail: function($forms, possibleEmailInputWannabees) {
            if ($forms) {

                var dataValRequired = "email",
                    re = RegExp(dataValRequired, "i");

                possibleEmailInputWannabees.filter(function() {
                    return re.test($(this).attr('data-val-required'));
                }).each(function() {
                    $(this).attr("type", "email");
                });

            }
        },
        setupFormSteps: function($form) {
            var $formNav = $form.find(".contourNavigation .col-md-12"),
                // Create next- and prevbutton, and cache elements that change in the form
                $btns = $("<input type='button' class='btn btn-grey prev hidden' value='Tilbage' /> <input type='button' class='btn next' value='Videre' />"),
                formType = $form.parent().attr("class").replace("contour", "").trim();

            if ($form.hasClass("sendetbrev")) {
                $form.find(".contourPage").prepend("<ol class='form-progress'><li class='active'><span>1. Skriv dit brev</span></li><li><span>2. Kontaktinfo</span></li></ol>")
            }
            var $fieldset = $form.find(".contourPage").find("fieldset:first"),
                $formTabs = $form.find(".form-progress"),
                $formSteps = $fieldset.find("> div").children();

            // Save formType as Button Classes for GA Tag manager
            $fieldset.data("formtype", formType);
            $btns.eq(1).addClass(formType + "-next-step1");

            $btns.on("click", function(e) {
                var $btn = $(this);

                if ($btn.hasClass("next")) {
                    var tab = $formTabs.children(".active").index(),
                        $fields = $formSteps.eq(tab).find("input[data-val='true'], textarea[data-val='true']");
                    if (DIS.forms.validateFields($fields) || $fields.length === 0) {
                        DIS.forms.changeStep("next", $fieldset, $btn, $formSteps, $formTabs);
                    }
                } else {
                    DIS.forms.changeStep("prev", $fieldset, $btn, $formSteps, $formTabs);
                }
            });
            // Insert buttons before submit button in form
            $formNav.prepend($btns);
            // Disable tab-index on hidden fields
            var $laterSteps = $fieldset.find("> div > div:not(:first-child)");
            $laterSteps.find("input").attr("tabIndex", "-1");
        },
        changeStep: function(direction, $fieldset, $btn, $formSteps, $formTabs) {
            var $btnParent = $btn.parent(),
                totalSteps = $formSteps.length,
                formType = $fieldset.data("formtype"),
                step1margin = (DIS.winWidth > DIS.screenSm) ? "-18px" : "-9px",
                step2margin = (DIS.winWidth > DIS.screenSm) ? "-36px" : "-15px",
                step3margin = (DIS.winWidth > DIS.screenSm) ? "-42px" : "-20px";

            $formTabs.children(".active").removeClass("active");
            if (direction === "next") {
                if (totalSteps === 2) {
                    TweenLite.to($fieldset, 0.4, { xPercent: -50, marginLeft: step2margin, ease: Back.easeInOut });
                    $btn.attr("class", "btn btn-grey prev hidden");
                    $btn.prev().attr("class", "btn btn-grey prev").addClass(formType + "-back-step2");
                    DIS.forms.setTabindex($formSteps.eq(1));
                    $formTabs.children().eq(1).addClass("active");
                } else {
                    if ($btnParent.hasClass("middle")) {
                        TweenLite.to($fieldset, 0.4, { xPercent: -66.665, marginLeft: step3margin, ease: Back.easeInOut });
                        $btnParent.removeClass("middle");
                        $btn.prev().attr("class", "btn btn-grey prev").addClass(formType + "-back-step3");
                        $btn.attr("class", "btn next hidden");
                        DIS.forms.setTabindex($formSteps.eq(2));
                        $formTabs.children().eq(2).addClass("active");
                    } else {
                        TweenLite.to($fieldset, 0.4, { xPercent: -33.334, marginLeft: step1margin, ease: Back.easeInOut });
                        $btn.prev().attr("class", "btn btn-grey prev").addClass(formType + "-back-step2");
                        $btn.attr("class", "btn next").addClass(formType + "-next-step2");
                        $btnParent.addClass("middle");
                        DIS.forms.setTabindex($formSteps.eq(1));
                        $formTabs.children().eq(1).addClass("active");
                    }
                }
            } else {
                if (totalSteps === 2) {
                    TweenLite.to($fieldset, 0.4, { xPercent: 0, marginLeft: "0", ease: Back.easeInOut });
                    $btn.attr("class", "btn btn-grey prev hidden");
                    $btn.next().attr("class", "btn next").addClass(formType + "-next-step1");
                    DIS.forms.setTabindex($formSteps.eq(0));
                    $formTabs.children().eq(0).addClass("active");
                } else {
                    if ($btnParent.hasClass("middle")) {
                        TweenLite.to($fieldset, 0.4, { xPercent: 0, marginLeft: "0", ease: Back.easeInOut });
                        $btn.attr("class", "btn btn-grey prev hidden");
                        $btn.next().attr("class", "btn next").addClass(formType + "-next-step2");
                        $btnParent.removeClass("middle");
                        DIS.forms.setTabindex($formSteps.eq(0));
                        $formTabs.children().eq(0).addClass("active");
                    } else {
                        TweenLite.to($fieldset, 0.4, { xPercent: -33.333, marginLeft: step1margin, ease: Back.easeInOut });
                        $btn.attr("class", "btn btn-grey prev").addClass(formType + "-back-step3");
                        $btnParent.addClass("middle");
                        $btn.next().attr("class", "btn next").addClass(formType + "-next-step1");
                        DIS.forms.setTabindex($formSteps.eq(1));
                        $formTabs.children().eq(1).addClass("active");
                    }
                }
                $btnParent.find(".error-msg").remove();
            }
        },
        setTabindex: function($formSteps) {
            $formSteps.siblings().find("input").attr("tabIndex", "-1");
            $formSteps.find("input").removeAttr("tabIndex");
        },
        scrollTo: function(element) {
            if (element[0].getBoundingClientRect().top < 150) {
                $("html, body").animate({
                    scrollTop: element.offset().top - 150
                });
            }
        },
        validateForm: function(e) {
            DIS.forms.$currForm = ($(this).is("form")) ? $(this) : $(this).closest("form[action]");

            if (DIS.forms.$currForm.is("#donationForm") && !DIS.forms.$currForm.hasClass("repeated-payment")) {
                DIS.forms.$currFieldSet = DIS.forms.$currForm.find(".span4:first-child").find(".contourFieldSet");
            } else {
                DIS.forms.$currFieldSet = DIS.forms.$currForm.find(".contourFieldSet");
            }

            //var mobilefield = DIS.forms.$currFieldSet.find(".contourField.mobil");

            $("input[type=submit], .btn-submit", DIS.forms.$currForm).addClass("sending");
            //$("input[type=submit], .btn.primary", DIS.forms.$currForm).addClass("sending").prop("disabled", true);

            /* 15/09-2016: Added a nasty hack in selector, as skriv under forms with a field mobilenumber should have the validation as the rest */
            var $valFields = DIS.forms.$currFieldSet.find(".contourField input[data-val='true']:not(.contourField.mobil input), textarea[data-val='true'], select[data-val='true']");

            if ($valFields.length) {
                //Check for values in input fields, such as text/textarea
                return DIS.forms.validateFields($valFields);
            } else {
                return true;
            }


        },
        validateFields: function($valFields) {
            var validCount = 0,
                totalFields = $valFields.length,
                isValid = false;

            $valFields.each(function() {
                var valTrue = DIS.forms.validateInput($(this));
                valTrue ? validCount++ : validCount = -1;

                if (validCount === totalFields) {
                    isValid = true;
                } else if (validCount === -1) {
                    event.preventDefault();
                    $("input[type=submit], .btn-submit", DIS.forms.$currForm).removeClass("sending");
                    return false;
                }
            });
            return isValid;
        },
        validateInput: function($field) {

            var $input = $field,
                $errorField = $input.next();
            // if error has been shown reset it as hidden
            if ($input.hasClass("error")) {
                $input.removeClass("error");
                $errorField.html("");
            }
            // get input value
            var val = $input.val();

            var message = "";

            // determen the switchs validation check according to the input type
            var inputType = $input.attr("type");
            var inputId = $input.attr("id");

            if ($input.data("type")) {
                inputType = $input.data("type");
            }

            if (inputType === "cpr") {
                if (val.match(/^[0-9]{6}[\s|-]*[0-9]{4}$/) === null) {
                    message = $input.data("val-required");
                    DIS.forms.sendError($input, $errorField, message);
                    DIS.forms.scrollTo($input);
                    return false;
                }
            } else if (inputType === "tel") {
                if (val.match(/^((\+|00)?45\s*)?[0-9]{2}\s*[0-9]{2}\s*[0-9]{2}\s*[0-9]{2}$/) === null) {
                    message = $input.data("val-required");
                    DIS.forms.sendError($input, $errorField, message);
                    DIS.forms.scrollTo($input);
                    return false;
                }
            } else if (inputType === "email") {
                var mail = val.toLowerCase();
                message = $input.data("val-required");
                // Use the function validate_email to check if email is valid
                if (!DIS.forms.validateEmail(mail, $input, $errorField, message)) {
                    DIS.forms.scrollTo($input);
                    return false;
                }
            } else if (inputType === "number") {
                // Check numbers according to input val-min/val-max data-attribues
                var minVal = ($input.data("val-min")) ? $input.data("val-min") : 1,
                    maxVal = ($input.data("val-max")) ? $input.data("val-max") : 15;
                if (!$.isNumeric(val) || val.length < minVal || val.length > maxVal) {
                    message = $input.data("val-required");
                    DIS.forms.sendError($input, $errorField, message);
                    DIS.forms.scrollTo($input);
                    return false;
                }
            } else if (inputId === "customAmount") {
                if ($('form.repeated-payment').length > 0 && val < 25) {
                    // check for value above 1 and below 25
                    if (val >= 1 && !isNaN(val)) {
                        DIS.forms.sendError($input, $errorField, "Beløbet er for lille, angiv korrekt beløb");
                        $input.val("");
                        return false;
                        // check if field has a value - if not present error
                    } else if (val === '') {
                        var message = $input.data("val-required");
                        DIS.forms.sendError($input, $errorField, message);
                        return false;
                    } else {

                    }
                } else {
                    if (val && val > 0 && !isNaN(val)) {} else {
                        var message = $input.data("val-required");
                        DIS.forms.sendError($input, $errorField, message);
                        return false;
                    }
                }

            } else if (inputId === "LandsstaevneSolution") {
                if (val.length === 0) {
                    $(".solution").find(".error-msg").css("display", "block");
                    DIS.forms.sendError($input, $errorField, message);
                    DIS.forms.scrollTo($input);
                    return false;
                }
            } else {
                //Check for val-min / val-max requirements
                var required = ($input.data("val-required")) ? true : false;

                var minVal = ($input.data("val-min")) ? $input.data("val-min") : 1;
                if (required && val.length < minVal) {
                    // get the error message stored as the data-attribute below
                    message = $input.data("val-required");

                    //Use the function sendError to setup error-message
                    DIS.forms.sendError($input, $errorField, message);
                    DIS.forms.scrollTo($input);
                    return false;
                }
            }
            return true;
        },
        // EMAIL VALIDATION
        validateEmail: function(mail, $input, $errorField, msg) {

            var atpos = mail.indexOf("@");
            var dotpos = mail.lastIndexOf(".");

            if (mail == "") {
                DIS.forms.sendError($input, $errorField, msg);
                return false;
            } else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= mail.length || mail.length < 7) {
                DIS.forms.sendError($input, $errorField, msg);
                return false;
            } else {
                return true;
            }
        },
        sendError: function($input, $errorField, msg) {
            $errorField.html(msg);
            $input.addClass("error");
            $input.focus();
        },
        hasNumbers: function(str) {
            var regex = /\d/g;
            return regex.test(str);
        },
        blivMedlem: {
            active: 0,
            sendMail: function($form) {
                if (DIS.forms.blivMedlem.active == 1) {
                    return;
                }
                DIS.forms.blivMedlem.active = 1;
                var dibsAmount = $("input[name=amount]", $form),
                    radioButtons = $("input[name=radioAmount]", $form),
                    customAmount = $("input[name=customAmount]", $form),
                    submitButton = $("input[type=submit]", $form),
                    acceptUrl = $form.data("memberAction"),
                    inputName = $("input[name='delivery1.Name']", $form),
                    inputSurname = $("input[name='delivery2.Surname']", $form),
                    inputEmail = $("input[name='delivery3.Email']", $form),
                    inputMobile = $("input[name='delivery4.Mobile']", $form),
                    inputZip = $("input[name='delivery5.Zip']", $form),
                    inputCity = $("input[name='delivery6.City']", $form),
                    inputCpr = $("input[name='delivery7.Cpr']", $form),
                    inputStudent = $("input[name='delivery8.Stud']", $form),
                    inputRegnr = $("input[name='delivery9.Reg']", $form),
                    inputKonto = $("input[name='delivery10.Konto']", $form),
                    inputAddress = $("input[name='delivery5.Address']", $form);

                var queryString = "Email=" + inputEmail.val() + "&Name=" + inputName.val() + "&SurName=" + inputSurname.val() + "&Cpr=" + inputCpr.val() + "&Zip=" + inputZip.val() + "&City=" + inputCity.val() + "&Address=" + inputAddress.val() + "&Phone=" + inputMobile.val() + "&Student=" + inputStudent.prop("checked") + "&Amount=" + dibsAmount.val() + "&NodeId=" + $form.data("nodeid") + "&Reg=" + inputRegnr.val() + "&Konto=" + inputKonto.val();

                $.get("/umbraco/api/BlivMedlem/SendEmail?" + queryString, function(data) {
                    DIS.forms.blivMedlem.active = 0;
                    window.location.href = acceptUrl;
                }).fail(function(data) {
                    DIS.forms.blivMedlem.active = 0;
                    DIS.forms.sendFail(data);
                });
            }
        },
        cprFrom: {
            active: 0,
            sendMail: function($form) {
                if (DIS.forms.cprFrom.active == 1) {
                    return;
                }
                DIS.forms.cprFrom.active = 1;

                var acceptUrl = $("input[name=accepturl]", $form),
                    inputName = $("#cprName", $form),
                    inputSurname = $("#cprSurname", $form),
                    inputEmail = $("#cprEmail", $form),
                    inputMember = $("#cprMember", $form),
                    inputCpr = $("#cprCpr", $form);

                var queryString = "Email=" + inputEmail.val() + "&Name=" + inputName.val() + "&SurName=" + inputSurname.val() + "&Cpr=" + inputCpr.val() + "&MemberNumber=" + inputMember.val() + "&NodeId=" + $form.data("nodeid");

                $.get("/umbraco/api/cpr/SendEmail?" + queryString, function(data) {
                    DIS.forms.cprFrom.active = 0;
                    window.location.href = acceptUrl.val();
                }).fail(function(data) {
                    DIS.forms.cprFrom.active = 0;
                    DIS.forms.sendFail(data);
                });
            }
        },
        dibs: {
            setup: function($form) {

                $form.find("input[name=radioAmount]").click(function(e) {
                    $form.find("input[name=amount]").val($(this).val());
                });

                $form.find("input[name=customAmount]").change(function(e) {
                    var amount = $(this).val().replace(",", ".").replace(/[^0-9\.]/g, '');
                    $(this).val(amount.replace(".", ","));
                    $form.find("input[name=amount]").val(amount * 100);
                });
            },

            saveData: function($form, storeForAccept) {

                var dibsAmount = $("input[name=amount]", $form),
                    radioButtons = $("input[name=radioAmount]", $form),
                    customAmount = $("input[name=customAmount]", $form),
                    submitButton = $("input[type=submit]", $form),
                    orderidInput = $("input[name=orderid]", $form),
                    inputName = $("input[name='delivery1.Name']", $form),
                    inputSurname = $("input[name='delivery2.Surname']", $form),
                    inputEmail = $("input[name='delivery3.Email']", $form),
                    inputMobile = $("input[name='delivery4.Mobile']", $form),
                    inputZip = $("input[name='delivery5.Zip']", $form),
                    inputCity = $("input[name='delivery6.City']", $form),
                    inputCpr = $("input[name='delivery7.Cpr']", $form),
                    inputAddress = $("input[name='delivery5.Address']", $form);

                var queryString = "Email=" + inputEmail.val() + "&Name=" + inputName.val() + "&SurName=" + inputSurname.val() + "&Cpr=" + inputCpr.val() + (inputZip.val() ? "&Zip=" + inputZip.val() : "") + "&City=" + inputCity.val() + "&Address=" + inputAddress.val() + "&Phone=" + inputMobile.val() + "&Student=" + "false" + "&Amount=" + dibsAmount.val() + "&NodeId=" + $form.data("nodeid");

                $.get("/umbraco/api/dibs/GetOrderId?" + queryString, function(data) {
                    orderidInput.val(data);

                    if (storeForAccept) {
                        var json = {
                            donateName: inputName.val(),
                            donateSurname: inputSurname.val(),
                            amount: dibsAmount.val()
                        };
                        $.post("/umbraco/surface/UserInformation/Store", json)
                            .success(function() {
                                submitButton.click();
                                submitButton.addClass("sending");
                            });
                    } else {
                        submitButton.click();
                        submitButton.addClass("sending");
                    }
                }).fail(function(data) {
                    DIS.forms.sendFail(data);
                    submitButton.removeClass("sending");
                });
            },

            saveUserInfo: function($form) {
                var dibsAmount = $("input[name=amount]", $form),
                    submitButton = $("input[type=submit]", $form),
                    inputName = $("input[name='delivery1.Name']", $form),
                    inputSurname = $("input[name='delivery2.Surname']", $form);

                var json = {
                    donateName: inputName.val(),
                    donateSurname: inputSurname.val(),
                    amount: dibsAmount.val()
                };

                $.post("/umbraco/surface/UserInformation/Store", json);
            }
        },
        landsstaevne: {
            setup: function($form) {

                $form.find("select[name='delivery9.Solution']").change(function(e) {
                    $form.find("input[name=amount]").val($(this).val());
                });
            },

            saveData: function($form) {
                var dibsAmount = $("input[name=amount]", $form),
                    submitButton = $("input[type=submit]", $form),
                    orderidInput = $("input[name=orderid]", $form),
                    inputName = $("#LandsstaevneName", $form),
                    inputSurname = $("#LandsstaevneSurname", $form),
                    inputEmail = $("#LandsstaevneEmail", $form),
                    inputMobile = $("#LandsstaevneMobile", $form),
                    inputZip = $("#LandsstaevneZip", $form),
                    inputCity = $("#LandsstaevneCity", $form),
                    inputLandsstaevneMemberOrCPR = $("#LandsstaevneMemberOrCPR", $form),
                    inputAddress = $("#LandsstaevneAddress", $form),
                    inputAge = $("#LandsstaevneAge", $form),
                    inputVegetar = $("input[name='delivery12.Vegetar']:checked", $form),
                    inputSolution = $("#LandsstaevneSolution option:selected", $form),
                    inputComment = $("#LandsstaevneComment", $form),
                    inputRelevantInfo = $("input[name='delivery13.RelevantInfo']:checked", $form),
                    inputPaymentOptions = $("#LandsstaevnePaymentOptions", $form),
                    inputRelevantInfoVal;

                if (inputRelevantInfo.prop("checked") === undefined) {
                    inputRelevantInfoVal = false;
                } else {
                    inputRelevantInfoVal = true;
                }

                var queryString =
                    "Email=" + inputEmail.val() +
                    "&Name=" + inputName.val() +
                    "&SurName=" + inputSurname.val() +
                    "&MemberOrCPR=" + inputLandsstaevneMemberOrCPR.val() +
                    "&Zip=" + inputZip.val() +
                    "&City=" + inputCity.val() +
                    "&Address=" + inputAddress.val() +
                    "&Phone=" + inputMobile.val() +
                    "&Student=" + "false" +
                    "&Age=" + inputAge.val() +
                    "&Vegetar=" + inputVegetar.val() +
                    "&Solution=" + inputSolution.text() +
                    "&Comment=" + inputComment.val() +
                    "&RelevantInfo=" + inputRelevantInfoVal +
                    "&PaymentOptions=" + inputPaymentOptions.val() +
                    "&Amount=" + dibsAmount.val() +
                    "&NodeId=" + $form.data("nodeid");

                $.get("/umbraco/api/Landsstaevne/GetOrderId?" + queryString, function(data) {
                    orderidInput.val(data);
                    //send only "betalingskort" to DIBS, handle rest
                    if (inputPaymentOptions.val() === "Betalingskort") {
                        submitButton.click();
                    } else {
                        window.location = $form.find("input[name=accepturl]").val();
                    }

                }).fail(function() {
                    DIS.forms.sendFail(data);
                });
            }
        },
        sendFail: function(faildata) {
            var $btn = $("input[type=submit], .btn-submit", DIS.forms.$currForm);

            $btn.removeClass("sending");
            console.log(faildata);

            if (!$btn.hasClass('error')) {
                $btn.addClass("error");
                var $btnParent = $btn.parent();

                if ($btnParent.hasClass("submit")) {
                    $btnParent = $btnParent.parent();
                }

                if (!$btnParent.find(".error-msg").length) {
                    $btnParent.prepend("<span class='error-msg'>Der opstod en fejl. Er din email skrevet korrekt?</span>");
                    $btn.attr('disabled', false);
                }
                setTimeout(function() {
                    $btn.removeClass("error");
                }, 3500);
            }
        },
        /**
         * This function makes it possible to prefill inputs via attributes in location string
         * Location string should contain a # and if multiple be split by &
         * Attributes names should correspond with a class on the parent .contourField. the first child input will be the target of the prefill
         * Example Querystring: http://localhost:56231/hvorfor-amnesty#fornavn=Marlene&efternavn=Thorlund&email=mthorlund@gmail.com
         */
        prefillInputsfromQueryString: function() {
            var hashParams = window.location.hash.substr(1).split('&'); // substr(1) to remove the `#`
            for (var i = 0; i < hashParams.length; i++) {
                var p = hashParams[i].split('='),
                    contourField = '.contourField.' + p[0],
                    targetInput = $(contourField).find('input');
                targetInput.val(decodeURIComponent(p[1]));
                //by focussing the input we hide 'in field labels' if value is prefilled and not empty
                if (decodeURIComponent(p[1]) !== "") {
                    targetInput.focus();
                }
            }
        }
    }; // DIS.forms

}(jQuery, DIS, window));
