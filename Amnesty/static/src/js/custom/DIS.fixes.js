﻿/* DIS/PLAY Script 
    Author's name: Nicolaj Lund Nielsen
    Modified by: ...
    Client name: Amnesty
    Date of creation: 08-12-2014
*/

(function($, DIS, window) {
    "use strict";

    DIS.fixes = {
        init: function() {
            // Activate resize actions, delayed to avoid gitty resize-experience
            if (DIS.$billBoard.length) {
                $(window).on("resize", function () {
                    clearTimeout(DIS.resetTimer);
                    
                    DIS.resetTimer = setTimeout(DIS.fixes.onResize, 200);
                });
                
                DIS.fixes.resizeBoard();
            }
        },
        resizeBoard: function () {
            var totalViewHeight = (DIS.winHeight - DIS.menu.height) + "px";

            DIS.$billBoard.css("height", totalViewHeight);
            if (DIS.winWidth >= DIS.screenMd) {
                TweenLite.set(DIS.menu.$wrap, { top: totalViewHeight, position: "absolute" });
                TweenLite.set(DIS.menu.$sub, { height: totalViewHeight });
            }
        },
        onResize: function () {
            DIS.winWidth = (DIS.$preview.length) ? window.parent.$("#resultFrame").width() : window.innerWidth; // Umbraco preview iframes site fix
            DIS.winHeight = (DIS.$preview.length) ? window.parent.$("#resultFrame").height() : window.innerHeight;  // Umbraco preview iframes site fix

            DIS.menu.stickyTopMenu();
            DIS.fixes.resizeBoard();
        }
    }; // DIS.fixes

}(jQuery, DIS, window));

$(function () {
    // UMBRACO PREVIEW FIX
    // Checks if page is loaded in iframe
    var $iframe = window.parent.$("#resultFrame");

    if ($iframe.length) {
        $(window).load(function () {
            setTimeout(function () {
                DIS.init($iframe);
            },500);            
        });
    } else {
        DIS.init($iframe);
    }    
});
