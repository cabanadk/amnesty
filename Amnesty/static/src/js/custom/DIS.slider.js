/**
 * Created by mtt on 30-11-2016.
 */

(function ($, DIS, window) {
    "use strict";

    DIS.slider = {
        init: function () {

            $.fn.randomize = function (selector) {
                var $elems = selector ? $(this).find(selector) : $(this).children(),
                    $parents = $elems.parent();

                $parents.each(function () {
                    $(this).children(selector).sort(function (childA, childB) {
                        // * Prevent last slide from being reordered
                        /*if($(childB).index() !== $(this).children(selector).length - 1) {

                        }*/
                        return Math.round(Math.random());
                    }.bind(this)).detach().appendTo(this);
                });

                return this;
            };
            var $slider = $(".img-slider"),
                random = $(".img-slider").data("random");

            if(random === "True") { //random returns a string, not bool, which is why the string check
                $slider.randomize().slick({
                    dots: false,
                    arrows: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                dots: true,
                                arrows: false,
                                infinite: true,
                                speed: 300,
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            } else{
                $slider.slick({
                    dots: false,
                    arrows: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                dots: true,
                                arrows: false,
                                infinite: true,
                                speed: 300,
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            }



        }
    }; // DIS.slider

}(jQuery, DIS, window));
