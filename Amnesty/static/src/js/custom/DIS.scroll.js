﻿/* DIS/PLAY Script 
    Author"s name: Nicolaj Lund Nielsen
    Modified by: ...
    Client name: Amnesty
    Date of creation: 08-12-2014
*/

(function($, DIS, window) {
    "use strict";
    var $w = $(window);

    DIS.scroll = {
        controller: {},
        $sections: $(".ribbon[data-section-no]"),
        scenes: [],
        menuOpen: false,

        init: function () {

            // Build ScrollMagic Controller
            this.controller = new ScrollMagic.Controller();
            if (this.$sections.length) {
                this.animateSections();
            }
            
            $w.load(function () {
                if(!DIS.$billBoard.length) {
                    if(DIS.$body.hasClass("mediekonkurrence")){
                        DIS.scroll.activateSectionArrowLinks();
                    } else {
                        DIS.scroll.activateSectionLinkLines();
                    }
                }
            });
            $w.on("scroll", function () {
                window.clearTimeout(DIS.resetTimer);
                DIS.resetTimer = window.setTimeout(DIS.menu.showHideMenu, 100);
            });
        },
        activateSectionLinkLines: function () {
            var animaTo1 = { width: "50%", ease: Power4.easeOut },
                animaFrom1 = { scale: "0"},
                animaTo2= { scale: "1", display:"block", transformOrigin:"50% 50%", ease: Back.easeOut };

            DIS.menu.$secMenu.find("a[data-section-no]").each(function (i) {
                var $secLink = $(this),                    
                    $section = $($secLink.attr("href")),
                    tl = new TimelineLite(),
                    $linkLine = $secLink.siblings("hr.first"),
                    $linkDot = $linkLine.siblings("i");

                if (i !== 0) {
                    var $prevLinkLine = $secLink.parent().prev("li").find("hr.last");
                    tl.to($prevLinkLine[0], 0.2, { width: "50%", ease: Power0.easeNone });
                }

                tl.to($linkLine[0], 0.3, animaTo1);
                tl.fromTo($linkDot[0], 0.25, animaFrom1, animaTo2);
                
                var scene = new ScrollMagic.Scene({ triggerElement: $section[0] })
                    .setClassToggle($secLink[0], "active") // add class toggle
                    .setTween(tl)
                    .addTo(DIS.scroll.controller);
            });
        },
        activateSectionArrowLinks: function () {

            DIS.menu.$secMenu.find("a[data-section-no]").each(function (i) {
                var $secLink = $(this),
                    $section = $secLink.attr("href"),
                    $sectionHeight = $($section).height(),
                    //tl = new TimelineLite(),
                    $linkDot = $secLink.siblings("i");

                //console.log($sectionHeight);
                /*if (i !== 0) {
                    var $prevLinkDot = $secLink.parent().prev("li").find("i");
                    tl.to($prevLinkDot[0], 0.2, { bottom: "-10px", ease: Expo.easeIn });
                }
                tl.fromTo($linkDot[0], 0.25, { bottom: "-10px"}, { bottom: "-18px", transformOrigin:"50% 50%", ease: Expo.easeOut });*/

                var scene = new ScrollMagic.Scene({ triggerElement: $section, duration:$sectionHeight})
                    .setClassToggle($secLink[0], "active") // add class toggle
                    .addTo(DIS.scroll.controller);
                    //.setTween(tl);
            });
        },
        scrollClass: function(trigger, elem, c){
            new ScrollMagic.Scene({ triggerElement: trigger })
					.setClassToggle(elem, c) // add class toggle
					.addTo(DIS.scroll.controller);
        },
        animateSections: function () {
            var total = this.$sections.length;
            for (var i = 0; i < total; i++) {
                this.buildZoomScene("#topics #" + this.$sections[i].id);
            }
        },
        buildZoomScene: function (elemId) {

            var tl = new TimelineLite(),
                $elem = $(elemId),
                //$index = $elem.find(".category .index"),
                //$header = $elem.find(".category header"),
                $bg = $elem.find(".topic .bg");

            if ($bg.length) {
                tl.add(TweenLite.to($bg, 15, { scaleX: 1.05, scaleY: 1.05, ease: Cubic.easeInOut }));
            }
            //if ($index.length) {
            //    tl.add(TweenLite.to($index, 12, { color: "#bdc2c6", ease: Power1.easeInOut }), "-=15");
            //}
            //tl.add(TweenLite.to($header, 12, { color: "#a5aaac", ease: Power1.easeInOut }), "-=10");

            var scene = new ScrollMagic.Scene({ triggerElement: elemId, duration: 600, offset: -200 })
                           .setTween(tl)
                           .addTo(this.controller);
        },
        buildAnimaScene: function (elemId, tween) {
            var scene = new ScrollMagic.Scene({ triggerElement: elemId, offset: -100 })
                           .setTween(tween) 
                           .addTo(this.controller);
        }
    }; // DIS.scroll

}(jQuery, DIS, window));
