﻿/* DIS/PLAY Script 
    Author's name: Nicolaj Lund Nielsen
    Modified by: ...
    Client name: Amnesty
    Date of creation: 02-03-2015
*/

(function($, DIS, window) {
    "use strict";
    var $w = $w || $(window);

    DIS.infograph = {
        $modules: $(".infographic"),
        mIndex: 0,
        tl: new TimelineLite(),
        init: function () {
            this.$modules.each(function () {
                var $circles = $(this).find(".circle-loader");                
                $circles.each(DIS.infograph.setLoader);
                DIS.infograph.mIndex++;
            });
        },
        setLoader: function () {

            var $circle = $(this),
                is2nd = $circle.hasClass("leftover"),
                counter = { var: 0 },
                total = parseInt($circle.data("total")),
                current = parseInt($circle.data("current")),
                startDate = $circle.data("start-date"),
                $result = $circle.find(".result"),
                fullCirc = $circle.find(".full")[0],
                prog = $circle.find(".prog")[0],
                progBg = $circle.find(".progBg")[0];

            var missing = (current < total) ? total - current: 0,
                currPercent = (current < total) ? (current / total) * 100 : 100,
                bgPercent = currPercent < 98 ? currPercent + 2 : currPercent;

            if (!is2nd) {
                DIS.infograph.tl.to(fullCirc, 1.2, { strokeWidth: "54", ease: Elastic.easeOut });
            } else {
                DIS.infograph.tl.to(prog, 1.2, { strokeWidth: "66", ease: Elastic.easeOut }, "0.6");
            }

            DIS.infograph.tl.to(counter, 2, {
                var: (is2nd ? missing : current),
                onUpdate: function () {
                    var number = Math.ceil(counter.var) | 0;

                    if (number > 999) {
                        $result.text(DIS.infograph.formatInt(number));
                    } else {
                        $result.text(number);
                    }
                },
                ease: Power2.easeOut
            }, (is2nd ? "1.8" : "0.7"));

            DIS.infograph.tl.fromTo(prog, 2,
                { drawSVG: (is2nd ? "100%" : "0%"), rotationZ: "-110", transformOrigin: "50% 50%" },
                { drawSVG: currPercent + "%", rotationZ: "-90", ease: Power2.easeInOut}, (is2nd ? "2.06" : "0.6"));
            DIS.infograph.tl.fromTo(progBg, 2,
                { drawSVG: (is2nd ? "100%" : "0%"), rotationZ: "-110", transformOrigin: "50% 50%" },
                { drawSVG: bgPercent + "%", rotationZ: "-94", ease: Power2.easeInOut }, (is2nd ? "2.06" : "0.6"));

            if (is2nd) {

                DIS.scroll.buildAnimaScene(DIS.infograph.$modules[0], DIS.infograph.tl);

                DIS.infograph.tl = new TimelineLite({});
            }
        },
        formatInt: function (number) {            
            var re = "\\d(?=(\\d{3})+$)";
            return number.toFixed(Math.max(0, ~~0)).replace(new RegExp(re, "g"), "$&.");
        }
    }; // DIS.infograph

}(jQuery, DIS, window));
