/* DIS/PLAY Script
 Author"s name: Nicolaj Lund Nielsen
 Modified by: ...
 Client name: Amnesty
 Date of creation: 05-03-2015
 */

(function ($, DIS, window) {
    "use strict";

    DIS.share = {
        // stripUrl() removes hash-tags og parametere from the URL
        localURL: DIS.ui.stripUrl(location.href),
        // rootUrl() returns the root of a sites-url
        rootURL: "",
        firstShare: true,
        init: function () {
            this.rootURL = DIS.ui.rootUrl(DIS.share.localURL);
            if (!DIS.$billBoard.length) {

                var $shareBtns = $(".share-btn");

                if ($shareBtns.length) {
                    $shareBtns.each(DIS.share.checkShareable);
                }
            }
        },
        trimDesc: function (string, length) {

            //trim the string to the maximum length
            var trimmedString = string.substr(0, length);

            //re-trim if we are in the middle of a word
            trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))

            // add elipsis
            trimmedString = trimmedString + '...';

            // return string
            return trimmedString;
        },
        checkShareable: function () {

            var $btn = $(this),
                sectionId = $btn.data("section-id");
            if (!sectionId) {
                DIS.share.hover($btn);
            } else {
                var $section = $(sectionId),
                    // attacth sectionId to shared URL
                    shareLink = DIS.share.localURL + sectionId,
                    // Look for first headline in section
                    $headingone = $section.find("h2:first").text(),
                    $headline = $headingone, //Before find(".content h2:first") // Altered as all sections should support sharing, and sections can have different heading (h1, h2, h3 etc...)
                    //Does a section contain a form?
                    $hasForm = $section.find("form");

                if ($headline.length) { //if ($headline.length && $headline.closest(".form-module").length < 1) {

                    DIS.share.hover($btn);

                    $btn.on("click", function (e) {
                        e.preventDefault();

                        //Save ID to global scope so we can access it later on
                        window.sectionId = sectionId;

                        var $thisBtn = $(this);
                        if (DIS.share.firstShare) {
                            // This is a first-take implementation
                            // Implementing the facebook SDK just before the body-tag might be nessesary
                            $thisBtn.addClass("load-spin");
                            $.ajaxSetup({ cache: true });

                            $.getScript("//connect.facebook.net/en_UK/all.js", function () {
                                FB.init({
                                    //appId: "580014375731960", //Marlenes
                                    appId: "1576823489231295", //Nikolajs (den officielle)
                                    xfbml: true,
                                    version: "v2.2"
                                });
                                $thisBtn.removeClass("load-spin");
                                //If there a form in the section, we want to share the page, and not the section
                                if ($hasForm.length > 0) {
                                    //console.log("Section has form");
                                    DIS.share.shareSection($thisBtn, "", "", shareLink);
                                } else {
                                    DIS.share.shareSection($thisBtn, $section, $headline, shareLink);
                                }

                                DIS.share.firstShare = false;
                            }).fail(function () {
                                console.log("Failed to load //connect.facebook.net/en_UK/all.js")
                            });
                        } else {
                            DIS.share.shareSection($thisBtn, $section, $headline, shareLink);
                        }
                    });
                }
            }
        },
        hover: function ($btn) {
            var $svgCircle = $btn.find("circle"),
                $svgPath = $btn.find("path"),
                tl = new TimelineLite();

            tl.to($btn, 0.3, { scale: "1.15", ease: Power2.easeOut });

            tl.to($svgCircle, 0.3, { fill: "#000", transformOrigin: "50% 50%", ease: Power2.easeOut }, "0");
            tl.to($svgPath, 0.3, { fill: "#fbf038", transformOrigin: "50% 50%", ease: Power2.easeOut }, "0");

            tl.stop();

            DIS.ui.tweenHover($btn, tl);
        },
        shareSection: function ($btn, $section, $headline, shareLink) {

            if ($btn.hasClass("facebook")) {
                DIS.share.onFacebook($section, $headline, shareLink);
            } else if ($btn.hasClass("twitter")) {
                DIS.share.onTwitter($btn, $headline, shareLink);

            } else if ($btn.hasClass("email")) {
                DIS.share.onEmail(shareLink);
            }
        },
        onFacebook: function ($section, $headline, shareLink) {
            // Look for first description in section
            var descrip,
                $shareModule,
                headline,
                imgSrc,
                shareImg;

            //If headline is empty, this means that there's a form in a section, and we want to share the page, instead of a section
            if ($headline === "") {
                descrip = "";
                imgSrc = "";
                shareImg = "";
                headline = $("title").text();

                var width = 600,
                    height = 400,
                    opts = "menubar=no,toolbar=no,resizable=no,scrollbars=no," +
                    ",width=" + width +
                    ",height=" + height;

                var shareURL = "http://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(shareLink) + "&t=" + encodeURIComponent(headline);

                //window.open(shareURL, "Facebook-sharing-Window", opts);
                //

            } else {
                descrip = $section.find(".manchet:first").text();
                if (!descrip) {
                    descrip = $section.find("p:not(.manchet)").text();
                }
                $shareModule = $section;

                if (!descrip) {
                    $shareModule = $section.closest(".ribbon");
                    descrip = $shareModule.find(".manchet:first").text();
                    if (!descrip) {
                        descrip = $shareModule.find("p:not(.manchet)").text();
                    }
                }

                //console.log(window.sectionId)
                //imgSrc = $(window.sectionId).find("img:first").attr("src")

                // Look for an image to share
                if ($shareModule.hasClass("video")) {
                    imgSrc = "http:" + $shareModule.find("iframe").attr("src");
                } else {
                    imgSrc = $shareModule.find("img:first").attr("src");
                    if (!imgSrc) {
                        imgSrc = $section.find(".text-content img:first").attr("src");
                    }
                    //If none of the above, choose this image
                    if (imgSrc === undefined) {
                        imgSrc = $('meta[property="og:image"]').attr('content');
                    }
                }

                // Replace multible spaces with just one
                //headline = $headline.text().replace(/\s+/g, " ");
                if ($headline === "") {
                    $headline = $("title").text();
                }

                if (imgSrc) {
                    //To strip params from img url...used in testing
                    //var imgSrc = imgSrc.substring(0, imgSrc.indexOf('?'));
                    if (imgSrc.substring(0, 7) !== "http://" && imgSrc.substring(0, 8) !== "https://") {
                        shareImg = "http://" + DIS.share.rootURL + imgSrc;
                    } else if (imgSrc.substring(0, 7) === "http://" || imgSrc.substring(0, 8) === "https://") {
                        shareImg = imgSrc;
                    }
                }

                //Facebook sharing API code
                var publish = {
                    method: 'share_open_graph',
                    action_type: 'og.shares',
                    action_properties: JSON.stringify({
                        object: {
                            'og:url': shareLink,
                            'og:title': $headline,
                            'og:description': DIS.share.trimDesc(descrip, 180),
                            'og:image': shareImg
                        }
                    }),
                    actions: [
                        { name: "Del og kommenter på facebook", link: shareLink }
                    ]
                };

                FB.ui(publish, function (response) {
                    //if (response && response.post_id) {
                    //    console.log("Post was published.");
                    //} else {
                    //    console.log("Post was not published.");
                    //}
                });

            }

        },
        onTwitter: function ($btn, $headline, shareLink) {
            // Replace multible spaces with just one

            //var headline = $headline.text();

            // Share-window parameters
            var width = 600,
                height = 400,
                left = $btn.offset().left - (width / 2),
                top = 200,
                opts = "menubar=no,toolbar=no,resizable=no,scrollbars=no," +
                ",width=" + width +
                ",height=" + height +
                ",top=" + top +
                ",left=" + left;

            var shareURL = "http://twitter.com/home?status=" + encodeURIComponent($headline.substring(0, 80) + "...") + " " + (encodeURIComponent(" - " + shareLink));

            window.open(shareURL, "Twitter-sharing-Window", opts);
        },
        onEmail: function (shareLink) {
            window.location.href = "mailto:modtager?subject=Delt fra Amnestys hjemmeside&body=" + shareLink;
        }
    }
}(jQuery, DIS, window));