﻿using DisPlay.Amnesty.Helpers;
using Umbraco.Core;

namespace DisPlay.Amnesty.Tasks
{
    public class ExportFormsTask
    {
        public void Execute()
        {
            ExportHelper.ExportForms();
        }
    }
}