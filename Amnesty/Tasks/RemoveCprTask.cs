﻿using Umbraco.Core;

namespace DisPlay.Amnesty.Tasks
{
    public class RemoveCprTask
    {
        public void Execute()
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            db.Execute("UPDATE DibsOrders SET Cpr = ''");
            db.Execute("UPDATE blivMedlemOrders SET Cpr = ''");
            db.Execute("UPDATE LandsstaevneOrders SET MemberOrCPR = ''");
        }
    }
}