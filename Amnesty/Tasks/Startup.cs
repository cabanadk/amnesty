﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using DisPlay.Amnesty.Models;
using Hangfire;
using Hangfire.Dashboard;
using Hangfire.MemoryStorage;
using Microsoft.Owin;
using Owin;
using Umbraco.Core;
using Umbraco.Core.Security;

[assembly: OwinStartup(typeof(DisPlay.Amnesty.Tasks.Startup))]
namespace DisPlay.Amnesty.Tasks
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .UseLog4NetLogProvider();

            if (HostingEnvironment.IsDevelopmentEnvironment)
            {
                GlobalConfiguration.Configuration
                    .UseMemoryStorage();
            }
            else
            {
                GlobalConfiguration.Configuration
                    .UseSqlServerStorage(ApplicationContext.Current.DatabaseContext.ConnectionString);
            }

            var options = new DashboardOptions { AuthorizationFilters = new[] { new UmbracoBackofficeAuthFilter() } };

            app.UseHangfireDashboard("/umbraco/backoffice/hangfire", options);
            app.UseHangfireServer();

            RecurringJob.AddOrUpdate<ExportFormsTask>(x => x.Execute(), Cron.Daily(1, 0));//Export forms before removing cpr
            RecurringJob.AddOrUpdate<RemoveCprTask>(x => x.Execute(), Cron.Daily(1, 30));
            
        }
    }

    public class UmbracoBackofficeAuthFilter : IAuthorizationFilter
    {
        public bool Authorize(IDictionary<string, object> owinEnvironment)
        {
            var context = new OwinContext(owinEnvironment);

            return (context.Authentication.User.Identity is UmbracoBackOfficeIdentity
                    && context.Authentication.User.Identity.IsAuthenticated) ||
                    context.Request.Path.StartsWithSegments(new PathString("/js")) ||
                    context.Request.Path.StartsWithSegments(new PathString("/css")) ||
                    context.Request.Path.StartsWithSegments(new PathString("/fonts"));
        }
    }
}