﻿using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("PressList")]
    public class PressListViewModel: ListBaseViewModel
    {
        public PressListViewModel(IPublishedContent content) : base(content)
        {
        }

    }
}