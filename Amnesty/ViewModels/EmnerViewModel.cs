﻿using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("topics")]
    public class EmnerViewModel : NiveauBaseViewModel
    {
        public EmnerViewModel(IPublishedContent content)
            : base(content)
        {
        }
    }
}