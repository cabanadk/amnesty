﻿using System.Collections.Generic;
using System.Linq;
using DisPlay.Amnesty.ViewModels.Models;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("frontpage")]
    public class FrontpageViewModel : BaseViewModel
    {
        public FrontpageViewModel(IPublishedContent content)
            : base(content)
        {
        }

        public BillBoard BillBoard
        {
            get { return new BillBoard(this); }
        }

        public EmnerViewModel Emner
        {
            get { return this.FirstChild<EmnerViewModel>() as EmnerViewModel; }
        }
    }
}