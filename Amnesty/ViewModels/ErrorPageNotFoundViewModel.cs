﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("ErrorPageNotFound")]
    public class ErrorPageNotFoundViewModel : BaseViewModel
    {
        public ErrorPageNotFoundViewModel(IPublishedContent content) : base(content)
        {
        }
    }
}