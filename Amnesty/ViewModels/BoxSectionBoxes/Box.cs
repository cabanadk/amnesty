﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Archetype.Models;
using DisPlay.Amnesty.Helpers;

namespace DisPlay.Amnesty.ViewModels.BoxSectionBoxes
{
    public class Box
    {
        private readonly ArchetypeFieldsetModel _model;

        public Box(ArchetypeFieldsetModel model)
        {
            _model = model;
        }

        public ArchetypeFieldsetModel Model
        {
            get { return _model; }
        }

        public virtual string ViewName
        {
            get { return _model.Alias; }
        }

    }
}