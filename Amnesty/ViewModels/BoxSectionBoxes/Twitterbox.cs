﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DisPlay.Amnesty.Helpers;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels.BoxSectionBoxes
{
    public class TwitterBox : Box
    {

        public TwitterBox(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string UserName
        {
            get
            {
                return Model.GetValue<string>("username");
            }
        }

        public string HashTag
        {
            get
            {
                return Model.GetValue<string>("hashtag");
            }
        }

        public IEnumerable<TweetModel> GetTweetFromUser()
        {
            var username = Model.GetValue<string>("username");
            return new TwitterHelper().GetTweetsFromUsername(username, 1);
        }

        public IEnumerable<TweetModel> GetTweetsFromHashTag()
        {
            var hashtag = Model.GetValue<string>("hashtag");
            return new TwitterHelper().GetTweetsFromHash(hashtag, 1);
        }


    }
}