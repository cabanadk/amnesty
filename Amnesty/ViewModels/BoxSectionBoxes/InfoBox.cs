﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Archetype.Models;
using RJP.MultiUrlPicker.Models;

namespace DisPlay.Amnesty.ViewModels.BoxSectionBoxes
{
    public class InfoBox :Box
    {
        public InfoBox(ArchetypeFieldsetModel model) : base(model)
        {
        }

        public string Headline
        {
            get
            {
                return Model.GetValue<string>("Headline");
            }
        }

        public string SubHeadline
        {
            get { return Model.GetValue<string>("subHeadline"); }
        }

        public Link Link
        {
            get
            {
                var links = Model.GetValue<MultiUrls>("link");
                if (links == null || !links.Any())
                {
                    return null;
                }
                return links.FirstOrDefault();
            }
        }

    }
}