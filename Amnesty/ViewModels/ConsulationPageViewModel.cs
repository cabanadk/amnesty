﻿using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("ConsulationPage")]
    public class ConsulationPageViewModel : NiveauBaseViewModel
    {
        public ConsulationPageViewModel(IPublishedContent content) : base(content)
        {
        }
    }
}