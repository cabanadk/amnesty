﻿using System;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("niveau2")]
    public class Niveau2ViewModel : NiveauBaseViewModel
    {
        public Niveau2ViewModel(IPublishedContent content) : base(content)
        {

        }

        public string RenderInHead
        {
            get { return this.GetPropertyValue<string>("insertInHead"); }
        }

    }
}