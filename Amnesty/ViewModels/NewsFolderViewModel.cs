﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("NewsFolder")]
    public class NewsFolderViewModel : BaseViewModel
    {
        public NewsFolderViewModel(IPublishedContent content)
            : base(content)
        {

        }
    }
}