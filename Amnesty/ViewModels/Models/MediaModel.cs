﻿using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels.Models
{
    public class MediaModel : PublishedContentModel
    {
        public MediaModel(IPublishedContent content)
            : base(content)
        {
        }

        public string FileTitle
        {
            get { return this.GetPropertyValue<string>("fileTitle"); }
        }
    }
}