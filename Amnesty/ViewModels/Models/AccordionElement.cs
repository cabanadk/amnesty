﻿using System.Web;
using Archetype.Models;
using DisPlay.Amnesty.ViewModels.Modules;

namespace DisPlay.Amnesty.ViewModels.Models
{
    public class AccordionElement : Module
    {
        public AccordionElement(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string Headline
        {
            get { return Model.GetValue<string>("headline"); }
        }


        public IHtmlString Text
        {
            get
            {
                return Model.GetValue<HtmlString>("text");
            }
        }


    }
}