﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DisPlay.Amnesty.Helpers;
using Umbraco.Core.Models;

namespace DisPlay.Amnesty.ViewModels.Models
{
    public class CookieModel
    {
        public bool ShowCookieBanner { get; set; }
        public string Headline { get; set; }
        public IHtmlString Description { get; set; }
        public string LinkText { get; set; }
        public BaseViewModel LinkPage {get; set; }
    }
}