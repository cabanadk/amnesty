﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace DisPlay.Amnesty.ViewModels.Models
{
    [PublishedContentModel("Folder")]
    public class FolderModel : MediaModel
    {
        public FolderModel(IPublishedContent content)
            : base(content)
        {
        }

        public new IEnumerable<MediaModel> Children
        {
            get { return base.Children.OfType<MediaModel>(); }
        }

    }
}