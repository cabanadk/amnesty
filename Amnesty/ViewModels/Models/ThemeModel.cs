﻿using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels.Models
{
    [PublishedContentModel("Theme")]
    public class ThemeModel : PublishedContentModel
    {
        public ThemeModel(IPublishedContent content) : base(content)
        {
        }

        public string BackgroundColor
        {
            get
            {
                return this.GetPropertyValue<string>("backgroundColor");
            }
        }

        public string TextColor
        {
            get { return this.GetPropertyValue<string>("textColor"); }
        }

        public string LinkColor
        {
            get
            {
                 return this.GetPropertyValue<string>("linkColor"); 
            }
        }

        public string LinkHover
        {
            get
            {
                return this.GetPropertyValue<string>("linkHover");
            }
        }

        public string HeaderColor
        {
            get
            {
                return this.GetPropertyValue<string>("headerColor");
            }
        }


        public string ButtonColor
        {
            get
            {
                return this.GetPropertyValue<string>("buttonColor");
            }
        }


        public string ButtonHover
        {
            get
            {
                return this.GetPropertyValue<string>("buttonHover");
            }
        }

        public string ClassName
        {
            get { return "Theme-"+Id; }
        }
    }
}