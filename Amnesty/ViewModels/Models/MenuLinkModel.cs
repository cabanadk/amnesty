﻿using RJP.MultiUrlPicker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels.Models
{
    public class MenuLinkModel
    {
        private readonly string _url;
        private readonly string _text;
        private readonly string _target;
        private readonly IEnumerable<BaseViewModel> _children = Enumerable.Empty<BaseViewModel>();
        private readonly IPublishedContent _page = null;

        public MenuLinkModel(Link link)
        {
            _url = link.Url;
            _text = link.Name;
            _target = link.Target;
            if (link.Type == LinkType.Content && link.Id.HasValue)
            {
                _page = UmbracoContext.Current.ContentCache.GetById(link.Id.Value);
                _children = _page.Children.OfType<BaseViewModel>();
            }
        }
        public MenuLinkModel(BaseViewModel viewModel)
        {
            _page = viewModel;
            _url = viewModel.Url;
            _text = viewModel.Name;
            //_target = viewModel.Url;
            _children = viewModel.Children.OfType<BaseViewModel>();
        }

        public string Url
        {
            get { return _url; }
        }
        public string Text
        {
            get { return _text; }
        }
        public string Target
        {
            get { return _target; }
        }

        public bool IsAncestor
        {
            get
            {
                if (_page == null || !UmbracoContext.Current.PublishedContentRequest.HasPublishedContent)
                {
                    return false;
                }
                return UmbracoContext.Current.PublishedContentRequest.PublishedContent.IsAncestor(_page);
            }
        }
        
        public bool IsActive
        {
            get
            {
                if (_page == null || !UmbracoContext.Current.PublishedContentRequest.HasPublishedContent)
                {
                    return false;
                }
                return UmbracoContext.Current.PublishedContentRequest.PublishedContent.Id == _page.Id;
            }
        }

        public IEnumerable<MenuLinkModel> Children
        {
            get { return _children.Where(_ => _.IsVisible()).Select(_ => new MenuLinkModel(_)); }
        }
    }
}