using System.Web;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace DisPlay.Amnesty.ViewModels.Models
{
    public class BillBoard
    {
        private readonly IPublishedContent _content;

        public BillBoard(IPublishedContent content)
        {
            _content = content.GetPropertyValue<IPublishedContent>("Billboard");
        }

        public string Layout
        {
            get { return _content.GetPropertyValue<string>("billboardLayout"); }
        }

        public ImageModel BackgroundImage
        {
            get
            {
                return _content.GetPropertyValue<ImageModel>("backgroundImage");
            }
        }

        public string PhotoName
        {
            get { return _content.GetPropertyValue<string>("photoName"); }
        }

        public string PhotoDescription
        {
            get
            {
                var description = _content.GetPropertyValue<string>("photoDescription");
                if (string.IsNullOrWhiteSpace(description))
                {
                    return "";
                }
                return description.Replace("\n", "<br>");
            }
        }

        public bool PhotoShowBackground
        {
            get { return _content.GetPropertyValue<bool>("photoShowBackground"); }
        }

        public IHtmlString Headline
        {
            get
            {
                var headline = _content.GetPropertyValue<string>("headline");

                return new HtmlString((headline ?? "").Replace("\n", "<br>"));
            }
        }

        public string ButtonText
        {
            get { return _content.GetPropertyValue<string>("buttonText"); }
        }

        public string ButtonLink
        {
            get
            {
                var link = _content.GetPropertyValue<IPublishedContent>("buttonLink");
                if (link == null)
                {
                    return string.Empty;
                }
                return link.Url;
            }
        }
    }
}