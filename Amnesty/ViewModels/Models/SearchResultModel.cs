﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace DisPlay.Amnesty.ViewModels.Models
{
    public class SearchResultModel
    {
        public BaseViewModel Content { get; set; }
        public string Title { get; set; }
        public IHtmlString Snippet { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
    }
}
