﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace DisPlay.Amnesty.ViewModels.Models
{
    [PublishedContentModel("File")]
    public class FileModel : MediaModel
    {
        public FileModel(IPublishedContent content)
            : base(content)
        {
        }

    }
}