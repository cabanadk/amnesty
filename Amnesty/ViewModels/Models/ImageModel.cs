﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace DisPlay.Amnesty.ViewModels.Models
{
    [PublishedContentModel("Image")]
    public class ImageModel : MediaModel
    {
        public ImageModel(IPublishedContent content)
            : base(content)
        {
        }

        public string AlternateText
        {
            get { return this.GetPropertyValue<string>("alternateText", Name); }
        }

        public string Photographer
        {
            get { return this.GetPropertyValue<string>("photographer", string.Empty); }
        }

        public string Description
        {
            get { return this.GetPropertyValue<string>("description", string.Empty); }
        }

        public bool ShowBG
        {
            get { return this.GetPropertyValue<bool>("showbg", false); }
        }

        public string GetCropUrl(int width, int? height = null, int? quality = null, ImageCropMode? imageCropMode = null, ImageCropAnchor? imageCropAnchor = null, bool preferFocalPoint = false, bool useCropDimensions = false, bool cacheBuster = true, ImageCropRatioMode? ratioMode = null, bool upScale = true, string format = "jpg", string furtherOptions = null)
        {
            return new DisPlay.Umbraco.Media.Image(this).GetCropUrl(width, height, quality, imageCropMode, imageCropAnchor, preferFocalPoint, useCropDimensions, cacheBuster, ratioMode, upScale, format, furtherOptions);
        }
    }
}
