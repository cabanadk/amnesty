﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DisPlay.Amnesty.ViewModels.Models
{
    public class MenuModel
    {
        public IEnumerable<MenuLinkModel> MainMenu { get; set; }
        public IEnumerable<MenuLinkModel> BurgerNavigation { get; set; }
        public IEnumerable<MenuLinkModel> FooterNavigation { get; set; }
    }
}