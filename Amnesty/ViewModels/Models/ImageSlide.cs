﻿using System.Linq;
using System.Web;
using Archetype.Models;
using DisPlay.Amnesty.ViewModels.Modules;
using RJP.MultiUrlPicker.Models;

namespace DisPlay.Amnesty.ViewModels.Models
{
    public class ImageSlide : Module
    {
        public ImageSlide(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string Headline
        {
            get { return Model.GetValue<string>("headline"); }
        }

        public IHtmlString Description
        {
            get
            {
                var description = Model.GetValue<string>("description");
                return new HtmlString((description ?? "").Replace("\r\n", "<br>"));
            }
        }

        public Link Link
        {
            get
            {
                var links = Model.GetValue<MultiUrls>("link");
                if (links == null || !links.Any())
                {
                    return null;
                }
                return links.FirstOrDefault();
            }
        }

        public ImageModel Image
        {
            get { return Model.GetValue<ImageModel>("image"); }
        }
    }
}