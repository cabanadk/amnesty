﻿using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("NewsPage")]
    public class NewsPageViewModel : NiveauBaseViewModel
    {
        public NewsPageViewModel(IPublishedContent content) : base(content)
        {
        }
    }
}