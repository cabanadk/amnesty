﻿using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("PressPage")]
    public class PressPageViewModel : NiveauBaseViewModel
    {
        public PressPageViewModel(IPublishedContent content) : base(content)
        {
        }
    }
}