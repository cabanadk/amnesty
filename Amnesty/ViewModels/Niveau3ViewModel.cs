﻿using DisPlay.Amnesty.ViewModels.Modules;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("niveau3")]
    public class Niveau3ViewModel : NiveauBaseViewModel
    {
        public Niveau3ViewModel(IPublishedContent content) : base(content)
        {

        }
        public virtual string Headline
        {
            get { return this.GetPropertyValue<string>("headline"); }
        }


        public string RenderInHead
        {
            get { return this.GetPropertyValue<string>("insertInHead"); }
        }

        public bool ShowTopFormular
        {
            get
            {
                foreach (Module module in this.TopSection.Modules)
                {
                    if (module.Model.Alias.Equals("skrivunderFormular"))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}