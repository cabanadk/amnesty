﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DisPlay.Amnesty.Helpers;
using RJP.MultiUrlPicker.Models;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using DisPlay.Amnesty.ViewModels.Models;
using SEOChecker.Core.DataTypeValues;
using SEOChecker.MVC;
using Umbraco.Core;

namespace DisPlay.Amnesty.ViewModels
{
    public abstract class BaseViewModel : PublishedContentModel
    {

        protected BaseViewModel(IPublishedContent content) : base(content)
        {

        }

        public string SearchPageUrl
        {
            get
            {
                var frontpage= ContentHelper.FindFrontpage(this);
                if (frontpage == null)
                {
                    return "";
                }
                var searchPage = frontpage.Children.FirstOrDefault(child => child.DocumentTypeAlias == "Search");
                if (searchPage == null)
                {
                    return "";
                }

                return searchPage.Url;
            }
        }

        public virtual string Title
        {
            get { return this.GetPropertyValue<string>("title", Name); }
        }

        public SEOCheckerValues SeoMetaData
        {
            get
            {
                if (GetProperty("seoChecker") == null || GetProperty("seoChecker").DataValue==null) 
                {
                    return new SEOCheckerValues();
                }
                return new SEOCheckerValues(GetProperty("seoChecker").DataValue.ToString());
            }
        }

        public IEnumerable<BaseViewModel> Breadcrumb
        {
            get { return this.AncestorsOrSelf<BaseViewModel>().Reverse().Where(_ => _.IsVisible()); }

        }

        public MenuModel MainMenu
        {
            get
            {
                var siteSettings = ContentHelper.FindSiteSettings(this);
                return new MenuModel
                {
                    MainMenu = ContentHelper.GetLinkModelsFromLinkField(siteSettings, "mainMenu")
                };
            }
        }

        public MenuModel Menu {
            get
            {
                //Skal på en eller anden måde have udskrevet valgte items børn med ud - Se evt NavigationBurger.cshtml
                //var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var siteSettings = ContentHelper.FindSiteSettings(this);

                return new MenuModel
                {
                    BurgerNavigation = ContentHelper.GetLinkModelsFromLinkField(siteSettings, "navigationBurgerItems")
                };
            } 
        }

        public List<ThemeModel> Themes
        {
            get
            {
                return (List<ThemeModel>)ApplicationContext.Current.ApplicationCache.RequestCache.GetCacheItem("Themes", () => new List<ThemeModel>());
            }
        } 

        public CookieModel Cookie
        {
            get
            {
                var showCookie = false;
                var httpCookie = HttpContext.Current.Request.Cookies["AmnestyCookie"];
                if (httpCookie == null || string.IsNullOrWhiteSpace(httpCookie.ToString()))
                {
                    var amnestyCookie = new HttpCookie("AmnestyCookie");
                    amnestyCookie.Expires = DateTime.Now.AddMonths(3);
                    amnestyCookie.Value = "Allowed";
                    HttpContext.Current.Response.Cookies.Add(amnestyCookie);
                    showCookie = true;
                }
                var siteSettings = ContentHelper.FindSiteSettings(this);
                var cookieLinkPage = siteSettings.CookieLinkPage;
                if (cookieLinkPage == null)
                {
                    cookieLinkPage = this;
                }
                return new CookieModel
                {
                    ShowCookieBanner = showCookie,
                    Description = siteSettings.CookieDescription,
                    Headline = siteSettings.CookieHeadline,
                    LinkPage = cookieLinkPage,
                    LinkText = siteSettings.CookieLinkText
                };
            }
        }

        public MenuModel FooterNavigation
        {
            get
            {
                var siteSettings = ContentHelper.FindSiteSettings(this);

                return new MenuModel
                {
                    FooterNavigation = ContentHelper.GetLinkModelsFromLinkField(siteSettings, "FooterNavigation")
                };
            }
        }

        public IEnumerable<string> SearchTags
        {
            get
            {
                var pageTags = this.GetPropertyValue<IEnumerable<IPublishedContent>>("tags");
                if (pageTags == null)
                {
                    return Enumerable.Empty<string>();
                }
                return pageTags.Select(_=>_.Name);
            }
        }

        public string VisualAppearance
        {
            get {
                var siteSettings = ContentHelper.FindSiteSettings(this);
                return siteSettings.VisualAppearance;
            }
        }

        public bool HidePageHeadingAndTags
        {
            get
            {
                var siteSettings = ContentHelper.FindSiteSettings(this);
                return siteSettings.HidePageHeadingAndTags;
            }
        }

    }
}