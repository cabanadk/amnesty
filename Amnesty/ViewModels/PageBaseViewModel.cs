﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DisPlay.Amnesty.ViewModels.Models;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels
{
    public class PageBaseViewModel : BaseViewModel
    {
        public PageBaseViewModel(IPublishedContent content) : base(content)
        {

        }
        public string Headline { get { return this.GetPropertyValue<string>("headline"); } }

        public DateTime Timestamp
        {
            get
            {
                var date = this.GetPropertyValue<DateTime>("date");
                if (date.Year == 1)
                {
                    return this.CreateDate;
                }
                return date;
            }
        }

        public ImageModel Image
        {
            get
            {
                return this.GetPropertyValue<ImageModel>("image");
            }
        }

        public string Text { get { return this.GetPropertyValue<string>("text"); } }
    }
}