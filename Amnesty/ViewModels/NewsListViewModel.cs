﻿using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("NewsList")]
    public class NewsListViewModel : ListBaseViewModel
    {
        public NewsListViewModel(IPublishedContent content) : base(content)
        {
        }

        public override int NumberOfItemsPerPage
        {
            get { return 10; }
        }
    }
}