﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Archetype.Models;
using DisPlay.Amnesty.Helpers;
using DisPlay.Amnesty.ViewModels.Modules;
using DisPlay.Amnesty.ViewModels.Models;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Content = System.Web.UI.WebControls.Content;

namespace DisPlay.Amnesty.ViewModels
{
    public abstract class NiveauBaseViewModel : BaseViewModel
    {
        protected NiveauBaseViewModel(IPublishedContent content) : base(content)
        {
        }

        public DateTime Timestamp
        {
            get
            {
                var date = this.GetPropertyValue<DateTime>("timestamp");
                if (date != default(DateTime))
                {
                    return date;
                }
                return this.GetPropertyValue<DateTime>("date");
            }
        }

        public string hideBreadCrumb
        {
            get
            {
                var hideBreadCrumb = this.GetPropertyValue<string>("hideBreadcrumb");
                return hideBreadCrumb;
            }
        }

        public TopSection TopSection
        {
            get
            {
                var model = this.GetPropertyValue<ArchetypeModel>("topSection").SingleOrDefault(x => !x.Disabled);
                if (model != null)
                {
                    return new TopSection(model);
                }
                return null;
            }
        }

        public BottomSection BottomSection
        {
            get
            {
                var model = this.GetPropertyValue<ArchetypeModel>("bottomSection").SingleOrDefault(x => !x.Disabled);
                if (model != null)
                {
                    return new BottomSection(model);
                }
                return null;
            }
            
        }

        public SectionWrap Sections
        {
            get
            {
                if (this.HasValue("sections"))
                {
                    return
                        new SectionWrap(
                            this.GetPropertyValue<ArchetypeModel>("sections")
                                .Where(x => !x.Disabled)
                                .Select(section => new Section(section)), this);
                }
                return new SectionWrap(Enumerable.Empty<Section>(),this);
            }
        }


    }

    public class SectionWrap
    {
        public IEnumerable<Section> Sections { get; set; }
        public IPublishedContent CurrentNode { get; set; }

        public SectionWrap(IEnumerable<Section> sections, IPublishedContent currentNode)
        {
            Sections = sections;
            CurrentNode = currentNode;
        }
    }
    

    public class Section
    {
        private readonly ArchetypeFieldsetModel _model;

        public Section(ArchetypeFieldsetModel model)
        {
            _model = model;
        }

        public string Name
        {
            get { return _model.GetValue<string>("sectionName"); }
        }

        public IEnumerable<Module> Modules
        {
            get { return _model.GetModules(); }
        }
    }

    public class TopSection
    {
        private readonly ArchetypeFieldsetModel _model;

        public TopSection(ArchetypeFieldsetModel model)
        {
            _model = model;
        }

        public IEnumerable<Module> Modules
        {
            get { return _model.GetModules(); }
        }
    }

    public class BottomSection
    {
        private readonly ArchetypeFieldsetModel _model;

        public BottomSection(ArchetypeFieldsetModel model)
        {
            _model = model;
        }

        public IEnumerable<Module> Modules
        {
            get { return _model.GetModules(); }
        }
    }

}