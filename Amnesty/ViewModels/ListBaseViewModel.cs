﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels
{

    public class ListBaseViewModel : BaseViewModel
    {
        public ListBaseViewModel(IPublishedContent content) : base(content)
        {
        }

        virtual public int NumberOfItemsPerPage
        {
            get { return 3; }
        }

        public string Headline { get { return this.GetPropertyValue<string>("headline"); } }

        public int NumberOfPages
        {
            get
            {
                var pageCount = AllChildren.Count() / NumberOfItemsPerPage;
                pageCount += AllChildren.Count() % NumberOfItemsPerPage == 0 ? 0 : 1;
                return pageCount;
            }
        }

        public int CurrentPage
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(HttpContext.Current.Request.QueryString["page"]))
                {
                    int page = 0;
                    if (int.TryParse(HttpContext.Current.Request.QueryString["page"], out page))
                    {
                        return page;
                    }
                }
                return 0;
            }
        }

        public string NextPage
        {
            get
            {
                var nameValues = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
                nameValues.Set("page", (CurrentPage+1).ToString());
                nameValues.Set("type", Type);
                string url = HttpContext.Current.Request.Url.AbsolutePath;
                string updatedQueryString = "?" + nameValues.ToString();
                return url + updatedQueryString + "#pressnews";
            }
        }

        public string PreviousPage
        {
            get
            {
                var nameValues = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
                nameValues.Set("page", (CurrentPage - 1).ToString());
                nameValues.Set("type", Type);
                string url = HttpContext.Current.Request.Url.AbsolutePath;
                string updatedQueryString = "?" + nameValues.ToString();
                return url + updatedQueryString + "#pressnews";
            }
        }

        public IEnumerable<PageBaseViewModel> AllChildren
        {
            get
            {
               var children = this.Children.Where(_ => _.DocumentTypeAlias == "NewsFolder").SelectMany(RecursiveChildren);
               var temp = children.Concat(this.Children.Select(_ => new PageBaseViewModel(_))).Where(_ => _.DocumentTypeAlias != "NewsFolder").OrderByDescending(_ => _.Timestamp);
                return temp;
            }
        }

        private IEnumerable<PageBaseViewModel> RecursiveChildren(IPublishedContent content)
        {
            var children = content.Children.Where(_ => _.DocumentTypeAlias == "NewsFolder").SelectMany(RecursiveChildren);

            return children.Concat(content.Children.Select(_ => new PageBaseViewModel(_)));
        }

        public IEnumerable<PageBaseViewModel> RelevantChildren
        {
            get
            {
                var listOfChildren = AllChildren;

                return listOfChildren.Skip(CurrentPage * NumberOfItemsPerPage).Take(NumberOfItemsPerPage);
            }
        }

        public string Type
        {
            get
            {
                switch (this.GetType().Name)
                {
                    case "PressListViewModel":
                        return "Press";
                    case "NewsListViewModel":
                        return "News";
                    case "ConsulationListViewModel":
                        return "Consulation";
                    default:
                        return this.GetType().Name;
                }
            }
        }
    }
}