﻿using System;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("subsiteForside")]
    public class SubsiteForsideViewModel : NiveauBaseViewModel
    {
        public SubsiteForsideViewModel(IPublishedContent content) : base(content)
        {

        }

        public string RenderInHead
        {
            get { return this.GetPropertyValue<string>("insertInHead"); }
        }

    }
}