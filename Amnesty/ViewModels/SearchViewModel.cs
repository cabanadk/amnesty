﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;
using DisPlay.Amnesty.ViewModels.Models;
using Examine;
using LinqToTwitter;
using Umbraco.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("search")]
    public class SearchViewModel :BaseViewModel
    {
        public SearchViewModel(IPublishedContent content) : base(content)
        {
        }

        public IEnumerable<string> SearchTags { get; set; }

        public IEnumerable<SearchResultModel> SearchResults { get; set; }

        public string SearchQuery { get; set; }
    }
}