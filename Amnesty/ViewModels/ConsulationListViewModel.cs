﻿using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace DisPlay.Amnesty.ViewModels
{
    [PublishedContentModel("ConsulationList")]
    public class ConsulationListViewModel : ListBaseViewModel
    {
        public ConsulationListViewModel(IPublishedContent content) : base(content)
        {
        }
    }
}