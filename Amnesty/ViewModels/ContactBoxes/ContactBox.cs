﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Archetype.Models;
using DisPlay.Amnesty.Helpers;

namespace DisPlay.Amnesty.ViewModels.ContactBoxes
{
    public class ContactBox
    {
        private readonly ArchetypeFieldsetModel _model;

        public ContactBox(ArchetypeFieldsetModel model)
        {
            _model = model;
        }

        public ArchetypeFieldsetModel Model
        {
            get { return _model; }
        }

        public virtual string ViewName
        {
            get { return _model.Alias; }
        }

    }
}