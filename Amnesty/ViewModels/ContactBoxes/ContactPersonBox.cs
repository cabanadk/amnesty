﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;

namespace DisPlay.Amnesty.ViewModels.ContactBoxes
{
    public class ContactPersonBox : ContactBox
    {
        public ContactPersonBox(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string PersonName
        {
            get
            {
                return Model.GetValue<string>("personname");
            }
        }

        public ImageModel Image
        {
            get
            {
                return Model.GetValue<ImageModel>("Image");
            }
        }

        public string Title
        {
            get { return Model.GetValue<string>("title"); }
        }

        public string Email
        {
            get { return Model.GetValue<string>("email"); }
        }

        public string Phone
        {
            get { return Model.GetValue<string>("phone"); }
        }
            

    }
}