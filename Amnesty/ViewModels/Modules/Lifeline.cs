﻿using System.Web;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;


namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Lifeline : Module
    {
        public Lifeline(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string Headline
        {
            get { return Model.GetValue<string>("headline"); }
        }

        public IHtmlString BodyText
        {
            get { return Model.GetValue<IHtmlString>("bodyText"); }
        }

        public string Form
        {
            get { return Model.GetValue<string>("form"); }
        }

    }
}