﻿using System.Web;
using Umbraco.Core.Models;
using Archetype.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Video : Module
    {
        public Video(ArchetypeFieldsetModel model) : base(model)
        {
        }

        public string Headline
        {
            get { return Model.GetValue<string>("headline");  }
        }

        public string VideoEmbed
        {
            get {     
                return Model.GetValue<string>("videoEmbed");  
            }
        }
    }
}