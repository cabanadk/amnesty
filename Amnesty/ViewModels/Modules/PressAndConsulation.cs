﻿using System.Collections.Generic;
using Archetype.Models;
using DisPlay.Amnesty.Helpers;
using Umbraco.Core.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class PressAndConsulation : Module
    {
        public PressAndConsulation(ArchetypeFieldsetModel model) : base(model)
        {
        }

        public ListBaseViewModel PressList
        {
            get
            {
                return Model.GetValue<ListBaseViewModel>("pressList");
            }
        }

        public ListBaseViewModel ConsulationList
        {
            get
            {
                return Model.GetValue<ListBaseViewModel>("consulationList");
            }
        } 
    }
}