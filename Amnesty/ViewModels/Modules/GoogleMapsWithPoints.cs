﻿using System.Collections.Generic;
using System.Linq;
using Archetype.Models;
using DisPlay.Amnesty.ViewModels.Models;
namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class GoogleMapsWithPoints : Module
    {
        public GoogleMapsWithPoints(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public IEnumerable<Point> Points
        {
            get { return Model.GetValue<ArchetypeModel>("points").Select(x => new Point(x)).OrderBy(_=>_.Zip); }
        }

        public string Json
        {
            get
            {
                var points = Points;


                return "["+string.Join(",",points.Select(_=>_.Json))+"]";
            }
        }


        public string ZoomLevel
        {
            get { return Model.GetValue<string>("zoomLevel"); }
        }


    }
}