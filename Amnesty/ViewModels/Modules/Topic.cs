﻿using System.Web;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Topic : Module
    {
        public Topic(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        ////Ville normalt være string, men fordi de kan indeholde <br /> skal det være IHtmlString
        public string Headline
        {
            get
            {
                return Model.GetValue<string>("headline");
            }
        }

        //Ville normalt være string, men fordi de kan indeholde <br /> skal det være IHtmlString
        public string SubHeadline
        {
            get
            {
                return Model.GetValue<string>("subheadline");
            }
        }

        public ImageModel Image
        {
            get
            {
                return Model.GetValue<ImageModel>("Image");
            }
        }

        public IPublishedContent Link
        {
            get
            {
                return Model.GetValue<IPublishedContent>("link");
            }
        }

    }
}