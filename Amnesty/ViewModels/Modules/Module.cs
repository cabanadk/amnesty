﻿using System.Collections.Generic;
using System.Linq;
using Archetype.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Umbraco.Core;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public abstract class Module
    {
        private readonly ArchetypeFieldsetModel _model;

        protected Module(ArchetypeFieldsetModel model)
        {
            _model = model;
        }

        public ArchetypeFieldsetModel Model
        {
            get { return _model; }
        }

        public ThemeModel Theme
        {
            get
            {
                var theme = Model.GetValue<ThemeModel>("theme");
                if (theme == null)
                {
                    return null;
                }
                var requestCache = ApplicationContext.Current.ApplicationCache.RequestCache;

                var themes = (List<ThemeModel>)requestCache.GetCacheItem("Themes", () => new List<ThemeModel>());
                if (!themes.Any(_ => _.Id == theme.Id))
                {
                    themes.Add(theme);
                    requestCache.ClearCacheItem("Themes");
                    requestCache.GetCacheItem("Themes", () => themes);
                }

                return Model.GetValue<ThemeModel>("theme");
            }
        }

        public virtual string ViewName
        {
            get { return _model.Alias; }
        }
    }
}