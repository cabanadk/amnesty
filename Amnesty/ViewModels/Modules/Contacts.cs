﻿using System.Web;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;
using DisPlay.Amnesty.Helpers;
using DisPlay.Amnesty.ViewModels.ContactBoxes;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Contacts : Module
    {
        public Contacts(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public IEnumerable<ContactBox> Boxes
        {
            get
            {
                return Model.GetValue<IEnumerable<ContactBox>>("contactBoxes");
            }
        }

    }
}
