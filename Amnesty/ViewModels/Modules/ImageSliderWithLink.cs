﻿using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;
using RJP.MultiUrlPicker.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class ImageSliderWithLink : Module
    {
        public ImageSliderWithLink(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public IEnumerable<ImageSlide> ImageSlide
        {
            get { return Model.GetValue<ArchetypeModel>("imageSlide").Select(x => new ImageSlide(x)); }
        }

        public bool RandomStart
        {
            get { return Model.GetValue<bool>("randomStart"); }
        }



    }
}