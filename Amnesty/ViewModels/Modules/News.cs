﻿using System.Collections.Generic;
using Archetype.Models;
using DisPlay.Amnesty.Helpers;
using Umbraco.Core.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class News : Module
    {
        public News(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public ListBaseViewModel NewsList
        {
            get
            {
                return Model.GetValue<ListBaseViewModel>("newsList");
            }
        }
    }
}