﻿using System.Web;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;
using RJP.MultiUrlPicker.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Text2Cols : Module
    {
        public Text2Cols(ArchetypeFieldsetModel model) : base(model)
        {
        }
        
        public string Headline
        {
            get { return Model.GetValue<string>("Headline");  }
        }

        public IHtmlString SubHeadline
        {
            get
            {
                var subHeadline = Model.GetValue<string>("subHeadline");

                return new HtmlString((subHeadline ?? "").Replace("\n", "<br>"));
            }
        }

        public IHtmlString Manchet
        {
            get
            {
                var manchet = Model.GetValue<string>("Manchet");
                return new HtmlString((manchet ?? "").Replace("\r\n", "<br>"));
            }
        }

        public ImageModel Image
        {
            get
            {
                return Model.GetValue<ImageModel>("Image");
            }
        }

        public IHtmlString BodyTextCol1 {
            get { return Model.GetValue<IHtmlString>("textCol1");  }
        }

        public IHtmlString BodyTextCol2
        {
            get { return Model.GetValue<IHtmlString>("textCol2");  }
        }

        public bool NoShare
        {
            get { return Model.GetValue<bool>("noShare"); }
        }

        public Link CallToAction
        {
            get
            {
                var cta = Model.GetValue<MultiUrls>("callToAction");
                if (cta == null || !cta.Any())
                {
                    return null;
                }
                return cta.FirstOrDefault();
            }
        }

        public ThemeModel Theme
        {
            get { return Model.GetValue<ThemeModel>("theme"); }
        }

    }
}