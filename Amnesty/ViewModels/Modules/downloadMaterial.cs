﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Archetype.Models;
using DisPlay.Amnesty.ViewModels.Models;
namespace DisPlay.Amnesty.ViewModels.Modules
{


    public class DownloadMaterial : Module
    {

        public IEnumerable<MediaModel> Files
        {
            get
            {
                var files = Model.GetValue<IEnumerable<MediaModel>>("files");
                if (files != null)
                {
                    return GetMediaItemsRecursive(files);
                }
                else
                {
                    return new List<MediaModel>();
                }
            }
        }

        private IEnumerable<MediaModel> GetMediaItemsRecursive(IEnumerable<MediaModel> files)
        {
            var result = new List<MediaModel>();

            foreach (MediaModel model in files)
            {
                FolderModel folderModel = model as FolderModel;
                if (folderModel != null)
                {
                    result.AddRange(GetMediaItemsRecursive(folderModel.Children));
                }
                else
                {
                    result.Add(model);
                }
            }
            return result;
        }

        public DownloadMaterial(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string Headline
        {
            get
            {
                return Model.GetValue<string>("Headline");
            }
        }
        public ThemeModel Theme
        {
            get { return Model.GetValue<ThemeModel>("theme"); }
        }




    }
}