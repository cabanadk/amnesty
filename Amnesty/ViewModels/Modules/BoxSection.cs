﻿using System.Web;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;
using DisPlay.Amnesty.Helpers;
using DisPlay.Amnesty.ViewModels.BoxSectionBoxes;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class BoxSection : Module
    {
        public BoxSection(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public IEnumerable<Box> Boxes
        {
            get
            {
                return Model.GetValue<IEnumerable<Box>>("SectionBoxes");
            }
        }

    }
}
