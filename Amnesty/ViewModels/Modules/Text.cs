﻿using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;
using DisPlay.Umbraco.Caching;
using RJP.MultiUrlPicker.Models;
using Umbraco.Core;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Text : Module
    {
        public Text(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string Headline
        {
            get { return Model.GetValue<string>("headline"); }
        }


        public ImageModel Image {
            get {
                return Model.GetValue<ImageModel>("Image");
            }
        }

        public IHtmlString SubHeadline
        {
            get {
                var subHeadline = Model.GetValue<string>("subHeadline");
                
                return new HtmlString((subHeadline ?? "").Replace("\n", "<br>"));
            }
        }

        public IHtmlString BodyText
        {
            get { return Model.GetValue<IHtmlString>("bodyText"); }
        }

        public bool NoShare
        {
            get { return Model.GetValue<bool>("noShare"); }
        }

        public Link CallToAction
        {
            get
            {
                var cta = Model.GetValue<MultiUrls>("callToAction");
                if (cta == null || !cta.Any())
                {
                    return null;
                }
                return cta.FirstOrDefault();
            }
        }

    }
}