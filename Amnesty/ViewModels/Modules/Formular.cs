﻿using System.Web;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Formular : Module
    {
        public Formular(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string Headline
        {
            get { return Model.GetValue<string>("headline"); }
        }

        public IHtmlString BodyText
        {
            get { return Model.GetValue<IHtmlString>("bodyText"); }
        }

        public ImageModel Image
        {
            get
            {
                return Model.GetValue<ImageModel>("Image");
            }
        }

        public string Form
        {
            get { return Model.GetValue<string>("form"); }
        }

        public bool BlackText
        {
            get { return Model.GetValue<bool>("blackText"); }
        }

        public bool FlipForm
        {
            get
            {
                return Model.GetValue<bool>("flipForm");
            }
        }

        public ThemeModel Theme
        {
            get { return Model.GetValue<ThemeModel>("theme"); }
        }
    }
}