﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Archetype.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class HrSkyen : Module
    {
        public HrSkyen(ArchetypeFieldsetModel model) : base(model)
        {
        }

        public string Headline { get { return Model.GetValue<string>("headline"); } }
    }
}