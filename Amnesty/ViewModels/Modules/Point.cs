﻿using System.Linq;
using Archetype.Models;
using Newtonsoft.Json;
using RJP.MultiUrlPicker.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Point : Module
    {
        public Point(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string Description
        {
            get { return Model.GetValue<string>("description"); }
        }

        public Link Link
        {
            get
            {
                var links = Model.GetValue<MultiUrls>("link");
                if (links == null || !links.Any())
                {
                    return null;
                }
                return links.FirstOrDefault();
            }
        }

        public string Longitude
        {
            get { return Model.GetValue<string>("longitude"); }
        }

        public string Latitude
        {
            get { return Model.GetValue<string>("latitude"); }
        }

        public string Title
        {
            get { return Model.GetValue<string>("title"); }
        }

        public string Zip
        {
            get { return Model.GetValue<string>("zip"); }
        }

        public string Json
        {
            get
            {
                return "{"+string.Format(
                        "\"title\":\"{0}\",\"description\":\"{1}\", \"linkUrl\":\"{2}\", \"linkText\":\"{3}\", \"longitude\":\"{4}\",\"latitude\":\"{5}\",\"zip\":\"{6}\"", Title,Description,Link?.Url,Link?.Name,Longitude,Latitude,Zip)+"}";
            }
        }
    }
}