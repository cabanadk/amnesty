﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Archetype.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class WriteLetter : Module
    {
        public WriteLetter(ArchetypeFieldsetModel _model)
            : base(_model)
        {
            
        }

        public string Headline
        {
            get { return Model.GetValue<string>("headline"); }
        }

        public IHtmlString BodyText
        {
            get { return Model.GetValue<IHtmlString>("bodyText"); }
        }

        public string Form
        {
            get { return Model.GetValue<string>("form"); }
        }

    }
}