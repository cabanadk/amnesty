﻿using Archetype.Models;
using DisPlay.Amnesty.ViewModels.Models;
namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class GoogleMaps : Module
    {
        public GoogleMaps(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public string Coordinate
        {
            get { return Model.GetValue<string>("coordinate"); }
        }

        public string Pintext
        {
            get { return Model.GetValue<string>("pintext"); }
        }

        public string zoomLevel
        {
            get { return Model.GetValue<string>("zoomlevel"); }
        }
        //public ImageModel pinImage {
        //    get
        //    {
        //        return Model.GetValue<ImageModel>("pinImage"); 
        //    }
        //}

    }
}