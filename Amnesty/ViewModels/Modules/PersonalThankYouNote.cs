﻿using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;
using DisPlay.Amnesty.Models;
using RJP.MultiUrlPicker.Models;
using SEOChecker.Core.Extensions;
using Umbraco.Core;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class PersonalThankYouNote : Module
    {
        public PersonalThankYouNote(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public IHtmlString BodyText
        {
            get
            {
                IHtmlString value = Model.GetValue<IHtmlString>("bodyText");
                string returnValue = value.ToHtmlString();

                var userInformation = HttpContext.Current.Session["UserInformation"] as UserInformation;
                if (userInformation != null)
                {
                    int amountInKr = userInformation.Amount/100; 

                    returnValue = returnValue.Replace("{dibsAmount}",
                        amountInKr.ToString());
                    returnValue = returnValue.Replace("{donateName}",
                        userInformation.DonateName);
                    returnValue = returnValue.Replace("{donateSurname}",
                        userInformation.DonateSurname);
                }

                return new HtmlString(returnValue);
            }
        }

    }
}