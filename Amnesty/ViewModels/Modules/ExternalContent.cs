﻿using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class ExternalContent : Module
    {

        public ExternalContent(ArchetypeFieldsetModel model)
                : base(model)
        {
        }

        public string ScriptContent
        {
            get { return Model.GetValue<string>("scriptContent"); }
        }

        public IHtmlString TextContent
        {
            get
            {
                var manchet = Model.GetValue<string>("textContent");
                return new HtmlString((manchet ?? "").Replace("\r\n", "<br>"));
            }
        }

    }

}