﻿using System.Collections.Generic;
using System.Linq;
using Archetype.Models;
using DisPlay.Amnesty.ViewModels.Models;
namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Accordion : Module
    {
        public Accordion(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public IEnumerable<AccordionElement> Accordions
        {
            get { return Model.GetValue<ArchetypeModel>("accordions").Select(x => new AccordionElement(x)); }
        }


    }
}