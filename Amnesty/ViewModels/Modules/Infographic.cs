﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using DisPlay.Amnesty.ViewModels.Models;
using Archetype.Models;

namespace DisPlay.Amnesty.ViewModels.Modules
{
    public class Infographic : Module
    {
        public Infographic(ArchetypeFieldsetModel model)
            : base(model)
        {
        }

        public DateTime startDate
        {
            get
            {
                return Model.GetValue<DateTime>("startDate");
            }
        }

        public int actualNumber
        {
            get
            {
                return Model.GetValue<int>("actualNumber");
            }
        }

        public int fullNumber
        {
            get
            {
                return Model.GetValue<int>("fullNumber");
            }
        }
        public string buttonText
        {
            get
            {
                return Model.GetValue<string>("buttonText");
            }
        }
        public IPublishedContent buttonLink
        {
            get
            {
                return Model.GetValue<IPublishedContent>("buttonLink");
            }
        }

        public string LeftUnit
        {
            get { return Model.GetValue<string>("leftUnit"); }
        }

        public string RightUnit
        {
            get { return Model.GetValue<string>("rightUnit"); }
        }

        public ThemeModel Theme
        {
            get { return Model.GetValue<ThemeModel>("theme"); }
        }



    }
}